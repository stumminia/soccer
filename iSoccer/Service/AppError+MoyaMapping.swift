//
//  AppError+MoyaMapping.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/11/21.
//

import Foundation
import Moya

public extension AppError {
    static func mapAppError(error: Error) -> AppError {
        
        // Codable mapping issues
        if let decErr: DecodingError = error as? DecodingError {
            var err = "DecodingError"
            switch decErr {
            case let .dataCorrupted(context):
                err = "Data Corrupted \(context)"
            case let .keyNotFound(key, context):
                err = "Key '\(key)' not found: \(context.debugDescription) - codingPath: \(context.codingPath)"
            case let .valueNotFound(value, context):
                err = "Value '\(value)' not found: \(context.debugDescription) - codingPath: \(context.codingPath)"
            case let .typeMismatch(type, context):
                err = "Type '\(type)' mismatch: \(context.debugDescription) - codingPath: \(context.codingPath)"
            }
            return AppError(errorCode: .mapping,
                            message: "Mapping error",
                            originalPayload: err)
        }
        
        // Network error
        if let moyaErr: MoyaError = error as? MoyaError {
            switch moyaErr {
            case let .underlying(_, .some(response)), let .statusCode(response):

                // TODO: we are assuming we have a standard Luxottica Error Payload... not confirmed yet.
                var errorString: String?
                
                if response.statusCode == 401 {
                    let errorPayload = try? JSONDecoder().decode(AuthenticationErrorPayload.self, from: response.data)
                    errorString = errorPayload?.toString()
                } else {
                    let errorPayload = try? JSONDecoder().decode(SoccerErrorPayload.self, from: response.data)
                    errorString = errorPayload?.toString()
                }
                //let str = String(decoding: response.data, as: UTF8.self)

                switch (response.statusCode, errorString) {
                case let (statusCode, .some(httpError)) where statusCode == 401:
                    return AppError(errorCode: .authentication,
                                    message: "Authentication error",
                                    originalPayload: httpError)

                case let (_, .some(httpError)):
                    return AppError(errorCode: .network,
                                    message: "Network Error",
                                    originalPayload: httpError)

                case let (statusCode, _):
                    return AppError(errorCode: .network,
                                    message: "Network Error",
                                    statusCode: statusCode,
                                    originalPayload: errorString)
                }
            case let .objectMapping(error, _):
                let reason = "Mapping Error\n" + error.localizedDescription
                return AppError(errorCode: .mapping, message: reason)
            default:
                return AppError(errorCode: .network, message: "Network Error")
            }
        }

        return AppError(errorCode: .generic,
                        message: "Cannot determine error kind",
                        originalPayload: error.localizedDescription)
    }
}

