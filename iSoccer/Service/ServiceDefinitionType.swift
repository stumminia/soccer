//
//  ServiceDefinitionType.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/11/21.
//

import Foundation
import Moya

public protocol ServiceDefinitionType: Moya.TargetType {
    
    /// The base URL of the service
    var baseURL: URL { get }
    
}
