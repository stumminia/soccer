//
//  APIHandler.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 04/11/21.
//

import Alamofire

public class APIHandler {
            
    public func handleResponse<T: Decodable>(_ response: DataResponse<T, AFError>) -> Any? {
        switch response.result {
        case .success:
            return response.value
        case .failure:
            return response.error
        }
    }
}
