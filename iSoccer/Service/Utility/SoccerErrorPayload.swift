//
//  SoccerErrorPayload.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/11/21.
//

import Foundation

public struct SoccerErrorPayload : Codable {

    let message : String
    let typeError : String
    
    // MARK: Coding Keys

    enum CodingKeys: String, CodingKey {
        case message = "message"
        case typeError = "typeError"
    }

    public func toString() -> String? {
        do {
            let jsonData = try JSONEncoder().encode(self)
            return String(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
}
