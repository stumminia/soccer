//
//  AppError.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/11/21.
//

import Foundation

public struct AppError : Error {

    public let code: Int
    public let message: String
    public let statusCode: Int? // network service status code
    public let originalPayload: String?
    
    public init(code: Int, message: String, statusCode: Int? = nil, originalPayload: String? = nil) {
        self.code = code
        self.message = message
        self.statusCode = statusCode
        self.originalPayload = originalPayload
    }
    
    public init(errorCode: AppErrorCode, message: String, statusCode: Int? = nil, originalPayload: String? = nil) {
        self.code = errorCode.rawValue
        self.message = message
        self.statusCode = statusCode
        self.originalPayload = originalPayload
    }
}


public enum AppErrorCode : Int {
    // generic
    case unknown = 1000
    case configuration // internal error for code inconsistency, kind of weaksekf missing reference
    case generic
    case network
    case authentication
    case mapping
    case followError
    case firebaseError
    // login / auth
    case invalidLoginCredentials
    case firebaseInvalidToken
    case notLoggedIn
    // db
    case dbSaveError
}
