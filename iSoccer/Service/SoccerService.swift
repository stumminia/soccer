//
//  SoccerService.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/11/21.
//

import Foundation
import Moya

public enum SoccerServiceRequest {
    
    case allLeagues(auth: ApiAuth)
    case allCountries(auth: ApiAuth)
    case currentFixture(auth: ApiAuth, league: Int, season: Int, current: Bool)
    case getFixtureByRound(auth: ApiAuth, league: Int, season: Int, round: String?)
    case getStandings(auth: ApiAuth, league: Int, season: Int)
    case getEvent(auth: ApiAuth, fixture: Int)
    case getLineups(auth: ApiAuth, fixture: Int)
    case getTopScores(auth: ApiAuth, league: Int, season: Int)
    case getTopAssist(auth: ApiAuth, league: Int, season: Int)
    case getTopRedCards(auth: ApiAuth, league: Int, season: Int)
    case getTopYellowCards(auth: ApiAuth, league: Int, season: Int)
    case getStatistics(auth: ApiAuth, fixture: Int)
    case getPlayersTeam(auth: ApiAuth, teamID: Int)
    case getTeamInformation(auth: ApiAuth, teamID: Int)
    case getTeamStatistics(auth: ApiAuth, league: Int, season: Int, teamID: Int)
    case getTeamSeasons(auth: ApiAuth, teamID: Int)
    case getAllFixtureByTeam(auth: ApiAuth, season: Int, teamID: Int)
    case getSeasonPlayer(auth: ApiAuth, playerId: Int)
    case getPlayerStatistics(auth: ApiAuth, playerId: Int, season: Int)
    case getPlayerTransfers(auth: ApiAuth, playerId: Int)
    case getPlayerTrophies(auth: ApiAuth, playerId: Int)
}

public struct SoccerService {
    
    /**
     The service request to be used by this target
     */
    public let serviceRequest: SoccerServiceRequest

    /**
     The target's base URL
     */
    public let baseURL: URL

    /**
     Application API. Initialize this target with the service you want to use and the baseURL of Neosperience Application API.
     - parameters:
     - service: The service to be used by this target
     - baseURL: The target's base URL
     */
    init(serviceRequest: SoccerServiceRequest, baseURL: URL) {
        self.serviceRequest = serviceRequest
        self.baseURL = baseURL
    }
    
}

public struct ApiAuth {
    let apiHost: String
    let apiKey: String
    
    init(host: String = "api-football-v1.p.rapidapi.com", key: String = "04947e90a4msh59541d64080d56cp1ddba9jsn2cb473e7fc7f") {
        self.apiHost = host
        self.apiKey = key
    }
}

extension SoccerService: ServiceDefinitionType {
    
    public var path: String {
        switch serviceRequest {
        case .allLeagues: return "/v3/leagues"
        case .allCountries: return "/v3/countries"
        case .currentFixture: return "/v3/fixtures/rounds"
        case .getFixtureByRound: return "/v3/fixtures"
        case .getStandings: return "/v3/standings"
        case .getEvent: return "/v3/fixtures/events"
        case .getLineups: return "/v3/fixtures/lineups"
        case .getTopScores: return "/v3/players/topscorers"
        case .getTopAssist: return "/v3/players/topassists"
        case .getTopRedCards: return "/v3/players/topredcards"
        case .getTopYellowCards: return "/v3/players/topyellowcards"
        case .getStatistics: return "/v3/fixtures/statistics"
        case .getPlayersTeam: return "/v3/players/squads"
        case .getTeamInformation: return "/v3/teams"
        case .getTeamStatistics: return "/v3/teams/statistics"
        case .getTeamSeasons: return "/v3/teams/seasons"
        case .getAllFixtureByTeam: return "/v3/fixtures"
        case .getSeasonPlayer: return "/v3/players/seasons"
        case .getPlayerStatistics: return "/v3/players"
        case .getPlayerTransfers: return "/v3/transfers"
        case .getPlayerTrophies: return "/v3/trophies"
        }
    }
    
    public var method: Moya.Method {
        switch serviceRequest {
        case .allLeagues: return .get
        case .allCountries: return .get
        case .currentFixture: return .get
        case .getFixtureByRound: return .get
        case .getStandings: return .get
        case .getEvent: return .get
        case .getLineups: return .get
        case .getTopScores: return .get
        case .getTopAssist: return .get
        case .getTopRedCards: return .get
        case .getTopYellowCards: return .get
        case .getStatistics: return .get
        case .getPlayersTeam: return .get
        case .getTeamInformation: return .get
        case .getTeamStatistics: return .get
        case .getTeamSeasons: return .get
        case .getAllFixtureByTeam: return .get
        case .getSeasonPlayer: return .get
        case .getPlayerStatistics: return .get
        case .getPlayerTransfers: return .get
        case .getPlayerTrophies: return .get
        }
    }
    
    public var headers: [String: String]?{
        switch serviceRequest {
        case .allLeagues(let auth): return self.headers(with: auth)
        case .allCountries(let auth): return self.headers(with: auth)
        case .currentFixture(let auth,_,_,_): return self.headers(with: auth)
        case .getFixtureByRound(let auth,_,_,_): return self.headers(with: auth)
        case .getStandings(let auth,_,_): return self.headers(with: auth)
        case .getEvent(let auth,_): return self.headers(with: auth)
        case .getLineups(let auth,_): return self.headers(with: auth)
        case .getTopScores(let auth,_,_): return self.headers(with: auth)
        case .getTopAssist(let auth,_,_): return self.headers(with: auth)
        case .getTopRedCards(let auth,_,_): return self.headers(with: auth)
        case .getTopYellowCards(let auth,_,_): return self.headers(with: auth)
        case .getStatistics(let auth,_): return self.headers(with: auth)
        case .getPlayersTeam(let auth,_): return self.headers(with: auth)
        case .getTeamInformation(let auth,_): return self.headers(with: auth)
        case .getTeamStatistics(let auth,_,_,_): return self.headers(with: auth)
        case .getTeamSeasons(let auth,_): return self.headers(with: auth)
        case .getAllFixtureByTeam(let auth,_,_): return self.headers(with: auth)
        case .getSeasonPlayer(let auth,_): return self.headers(with: auth)
        case .getPlayerStatistics(let auth,_,_): return self.headers(with: auth)
        case .getPlayerTransfers(let auth,_): return self.headers(with: auth)
        case .getPlayerTrophies(let auth,_): return self.headers(with: auth)
        }
    }
    
    public var sampleData: Data { return Data() }
    
    public var task: Task {
        switch serviceRequest {
        case .allLeagues(_): return .requestPlain
        case .allCountries(_): return .requestPlain
        case let .currentFixture(_,league,season,current):
            var params: [String: Any] = [:]
            params["league"] = league
            params["season"] = season
            params["current"] = String(current)
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case let .getFixtureByRound(_,league,season,round):
            var params: [String: Any] = [:]
            params["league"] = league
            params["season"] = season
            if let round = round {
                params["round"] = round
            }
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        case let .getStandings(_,league,season):
            var params: [String: Any] = [:]
            params["season"] = season
            params["league"] = league
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)

        case let .getEvent(_,fixtureID):
            var params: [String: Any] = [:]
            params["fixture"] = fixtureID
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getLineups(_,fixture):
            var params: [String: Any] = [:]
            params["fixture"] = fixture
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)

        case let .getTopScores(_,league,season):
            var params: [String: Any] = [:]
            params["league"] = league
            params["season"] = season
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getTopAssist(_,league,season):
            var params: [String: Any] = [:]
            params["league"] = league
            params["season"] = season
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getTopRedCards(_,league,season):
            var params: [String: Any] = [:]
            params["league"] = league
            params["season"] = season
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getTopYellowCards(_,league,season):
            var params: [String: Any] = [:]
            params["league"] = league
            params["season"] = season
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getStatistics(_,fixture):
            var params: [String: Any] = [:]
            params["fixture"] = fixture
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getPlayersTeam(_, teamID):
            var params: [String: Any] = [:]
            params["team"] = teamID
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getTeamInformation(_,teamID):
            var params: [String: Any] = [:]
            params["id"] = teamID
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getTeamStatistics(_, league,season,teamID):
            var params: [String: Any] = [:]
            params["league"] = league
            params["season"] = season
            params["team"] = teamID
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getTeamSeasons(_,teamID):
            var params: [String: Any] = [:]
            params["team"] = teamID
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getAllFixtureByTeam(_,season,teamID):
            var params: [String: Any] = [:]
            params["team"] = teamID
            params["season"] = season
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getSeasonPlayer(_, playerId):
            var params: [String: Any] = [:]
            params["player"] = playerId
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getPlayerStatistics(_, playerId, season):
            var params: [String: Any] = [:]
            params["id"] = playerId
            params["season"] = season
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getPlayerTransfers(_, playerId):
            var params: [String: Any] = [:]
            params["player"] = playerId
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
            
        case let .getPlayerTrophies(_, playerId):
            var params: [String: Any] = [:]
            params["player"] = playerId
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
        
        
    }
    
    public var validationType: ValidationType {
        switch serviceRequest {
        default: return .successAndRedirectCodes
        }
    }
    
    private func headers(with auth: ApiAuth) -> [String:String] {
        let dict: [String:String] = [
            "x-rapidapi-host" : auth.apiHost,
            "x-rapidapi-key" : auth.apiKey
        ]
         
        return dict
    }
    
}

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}
