//
//  ServiceClient.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/11/21.
//

import Foundation
import Alamofire
import Moya

open class ServiceClient<ServiceDefinition: ServiceDefinitionType> {
    
    /// Moya Provider
    public let provider: MoyaProvider<ServiceDefinition>

    /// The base URL of the service
    public let baseURL: URL
    
    public init(baseURL: URL,
                sessionManager: Alamofire.Session? = nil,
                shouldStub: Bool = false,
                moyaPlugins: [PluginType] = [],
                endpointClosure: @escaping MoyaProvider<ServiceDefinition>.EndpointClosure = MoyaProvider.defaultEndpointMapping,
                stubClosure: @escaping MoyaProvider<ServiceDefinition>.StubClosure = MoyaProvider<ServiceDefinition>.neverStub,
                requestClosure: @escaping MoyaProvider<ServiceDefinition>.RequestClosure = MoyaProvider<ServiceDefinition>.defaultRequestMapping) {
        
        self.baseURL = baseURL

        var customStub = stubClosure
        if shouldStub { customStub = MoyaProvider.immediatelyStub }

        let alamoSessionManager = sessionManager ?? MoyaProvider<ServiceDefinition>.defaultAlamofireSession()

        self.provider = MoyaProvider<ServiceDefinition>(
            endpointClosure: endpointClosure,
            requestClosure: requestClosure,
            stubClosure: customStub,
            manager: alamoSessionManager,
            plugins: moyaPlugins
        )
    }
}

// MARK: Session Manager with custom cache path

extension ServiceClient {
    /// Builds a sessionManager with custom path and size. It can be useful to isolate cached data from shared cache
    /// (e.g. we can safely flush shared cache without compromising application cache)
    ///
    /// - Parameters:
    ///   - diskPath: the folder under application caches directory where the cache data is stored. Defaults to "ServiceClientData"
    ///   - diskMB: the capacity, measured in megabytes, for the cache on disk. Defaults to 10
    ///   - memoryMB: the capacity, measured in megabytes, for the cache in memory. Defaults to 4
    /// - Returns: an initialized Alamofire.Session, with the given capacity, backed by disk.
    public static func cacheSessionManager(diskPath: String = "ServiceClientData",
                                           diskMB: Int = 10,
                                           memoryMB: Int = 4) -> Alamofire.Session {
        let cache = URLCache(memoryCapacity: memoryMB * 1024 * 1024, diskCapacity: diskMB * 1024 * 1024, diskPath: diskPath)
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.urlCache = cache
        let manager = Alamofire.Session(configuration: sessionConfig)
        return manager
    }
}


public extension MoyaProvider {
    /**
     Initializes the MoyaProvider instance with OAuth2 authentication required by Neosperience Cloud
     - parameters:
     - serviceAuthenticator: An object that will handle alamofire retry and adapt functionality
     */
    convenience init(endpointClosure: @escaping EndpointClosure = MoyaProvider.defaultEndpointMapping,
                     requestClosure: @escaping RequestClosure = MoyaProvider.defaultRequestMapping,
                     stubClosure: @escaping StubClosure = MoyaProvider.neverStub,
                     manager: Session = MoyaProvider<Target>.defaultAlamofireSession(),
                     plugins: [PluginType] = [],
                     trackInflights: Bool = false) {

        self.init(endpointClosure: endpointClosure,
                  requestClosure: requestClosure,
                  stubClosure: stubClosure,
                  session: manager,
                  plugins: plugins,
                  trackInflights: trackInflights)
    }
}

