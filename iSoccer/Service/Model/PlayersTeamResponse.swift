//
//  PlayersTeamResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 10/03/22.
//

import Foundation
import SwiftUI

public class PlayersTeamResponse: BaseResponse {
    
    var response: [PlayersTeam]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [PlayersTeam]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([PlayersTeam].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}

public struct PlayersTeam: Codable, Hashable {
    
    var team: Team?
    var players: [PlayerTeam]?
    
    public init(team: Team? = nil, players: [PlayerTeam]? = nil) {
        self.team = team
        self.players = players
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        team = (try? container.decodeIfPresent(Team.self, forKey: .team))
        players = (try? container.decodeIfPresent([PlayerTeam].self, forKey: .players))
    }
    
    enum CodingKeys: String, CodingKey {
        case team
        case players
    }
}


public struct PlayerTeam: Codable, Hashable, Identifiable {
    
    
    public var id: Int?
    var name: String?
    var age: Int?
    var number: Int?
    var position: String?
    var photo: String?
    
    public init(id: Int? = nil, name: String? = nil, age: Int? = nil, number: Int? = nil, position: String? = nil, photo: String? = nil) {
        self.id = id
        self.name = name
        self.age = age
        self.number = number
        self.position = position
        self.photo = photo
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        age = (try? container.decodeIfPresent(Int.self, forKey: .age))
        number = (try? container.decodeIfPresent(Int.self, forKey: .number))
        position = (try? container.decodeIfPresent(String.self, forKey: .position))
        photo = (try? container.decodeIfPresent(String.self, forKey: .photo))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case age
        case number
        case position
        case photo
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
}
