//
//  StandingsResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 27/12/21.
//

import Foundation

public class StandingsResponse: BaseResponse {
    
    public var response: [ResponseLeagueStandings]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [ResponseLeagueStandings]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([ResponseLeagueStandings].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}

public struct ResponseLeagueStandings: Codable {

    public var league: LeagueStandings?
    
    internal init(league: LeagueStandings) {
        self.league = league
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        league = (try? container.decodeIfPresent(LeagueStandings.self, forKey: .league))
    }
    
    enum CodingKeys: String, CodingKey {
        case league
    }
}

public struct LeagueStandings: Codable {
    
    var id: Int?
    var name: String?
    var country: String?
    var logo: String?
    var flag: String?
    var season: Int?
    var standings: [[Stands]]?
    
    public init(id: Int? = nil, name: String? = nil, country: String? = nil, logo: String? = nil, flag: String? = nil, season: Int? = nil, standings: [[Stands]]? = nil) {
        self.id = id
        self.name = name
        self.country = country
        self.logo = logo
        self.flag = flag
        self.season = season
        self.standings = standings
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        country = (try? container.decodeIfPresent(String.self, forKey: .country))
        logo = (try? container.decodeIfPresent(String.self, forKey: .logo))
        flag = (try? container.decodeIfPresent(String.self, forKey: .flag))
        season = (try? container.decodeIfPresent(Int.self, forKey: .season))
        standings = (try? container.decodeIfPresent([[Stands]].self, forKey: .standings))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case country
        case logo
        case flag
        case season
        case standings
    }
}

public struct Standing: Codable {
    
    var stands: [Stands]?
    
    public init(stands: [Stands]? = nil) {
        self.stands = stands
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        stands = (try? container.decodeIfPresent([Stands].self, forKey: .stands))
    }
    
    enum CodingKeys: String, CodingKey {
        case stands
    }
    
}

public struct Stands: Codable, Identifiable, Equatable, Hashable {

    public var id:UUID?
    var rank: Int?
    var team: Team?
    var points: Int?
    var goalsDiff: Int?
    var group: String?
    var form: String?
    var status: String?
    var description: String?
    var all: Goal?
    var home: Goal?
    var away: Goal?
    var update: String?
    
    
    public init(rank: Int? = nil, team: Team? = nil, points: Int? = nil, goalsDiff: Int? = nil, group: String? = nil, form: String? = nil, status: String? = nil, description: String? = nil, all: Goal? = nil, home: Goal? = nil, away: Goal? = nil, update: String? = nil) {
        self.id = UUID()
        self.rank = rank
        self.team = team
        self.points = points
        self.goalsDiff = goalsDiff
        self.group = group
        self.form = form
        self.status = status
        self.description = description
        self.all = all
        self.home = home
        self.away = away
        self.update = update
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        rank = (try? container.decodeIfPresent(Int.self, forKey: .rank))
        team = (try? container.decodeIfPresent(Team.self, forKey: .team))
        points = (try? container.decodeIfPresent(Int.self, forKey: .points))
        goalsDiff = (try? container.decodeIfPresent(Int.self, forKey: .goalsDiff))
        group = (try? container.decodeIfPresent(String.self, forKey: .group))
        form = (try? container.decodeIfPresent(String.self, forKey: .form))
        status = (try? container.decodeIfPresent(String.self, forKey: .status))
        description = (try? container.decodeIfPresent(String.self, forKey: .description))
        all = (try? container.decodeIfPresent(Goal.self, forKey: .all))
        home = (try? container.decodeIfPresent(Goal.self, forKey: .home))
        away = (try? container.decodeIfPresent(Goal.self, forKey: .away))
        update = (try? container.decodeIfPresent(String.self, forKey: .update))
    }
    
    enum CodingKeys: String, CodingKey {
        case rank
        case team
        case points
        case goalsDiff
        case group
        case form
        case status
        case description
        case all
        case home
        case away
        case update
    }
    
    public static func == (lhs: Stands, rhs: Stands) -> Bool {
        return lhs.id == rhs.id &&
        lhs.rank == rhs.rank &&
        lhs.team == rhs.team &&
        lhs.points == rhs.points &&
        lhs.goalsDiff == rhs.goalsDiff &&
        lhs.group == rhs.group &&
        lhs.form == rhs.form &&
        lhs.status == rhs.status &&
        lhs.description == rhs.description &&
        lhs.all == rhs.all &&
        lhs.home == rhs.home &&
        lhs.away == rhs.away &&
        lhs.update == rhs.update
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(rank)
        hasher.combine(team)
        hasher.combine(points)
        hasher.combine(goalsDiff)
        hasher.combine(group)
        hasher.combine(form)
        hasher.combine(status)
        hasher.combine(description)
        hasher.combine(all)
        hasher.combine(away)
        hasher.combine(home)
        hasher.combine(update)
    }
}

public struct Goal: Codable, Equatable, Hashable {
    
    var played: Int?
    var win: Int?
    var draw: Int?
    var lose: Int?
    var goals: GoalsStand?
    
    public init(played: Int? = nil, win: Int? = nil, draw: Int? = nil, lose: Int? = nil, goals: GoalsStand? = nil) {
        self.played = played
        self.win = win
        self.draw = draw
        self.lose = lose
        self.goals = goals
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        played = (try? container.decodeIfPresent(Int.self, forKey: .played))
        win = (try? container.decodeIfPresent(Int.self, forKey: .win))
        draw = (try? container.decodeIfPresent(Int.self, forKey: .draw))
        lose = (try? container.decodeIfPresent(Int.self, forKey: .lose))
        goals = (try? container.decodeIfPresent(GoalsStand.self, forKey: .goals))
    }
    
    enum CodingKeys: String, CodingKey {
        case played
        case win
        case draw
        case lose
        case goals
    }
    
    public static func == (lhs: Goal, rhs: Goal) -> Bool {
        return lhs.played == rhs.played &&
        lhs.win == rhs.win &&
        lhs.draw == rhs.draw &&
        lhs.lose == rhs.lose &&
        lhs.goals == rhs.goals
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(played)
        hasher.combine(win)
        hasher.combine(draw)
        hasher.combine(lose)
        hasher.combine(goals)
    }
}


public struct GoalsStand: Codable, Equatable, Hashable {
    
    var facts: Int?
    var against: Int?
    
    public init(facts: Int? = nil, against: Int? = nil) {
        self.facts = facts
        self.against = against
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        facts = (try? container.decodeIfPresent(Int.self, forKey: .facts))
        against = (try? container.decodeIfPresent(Int.self, forKey: .against))
    }
    
    enum CodingKeys: String, CodingKey {
        case facts = "for"
        case against = "against"
    }
    
    public static func == (lhs: GoalsStand, rhs: GoalsStand) -> Bool {
        return lhs.facts == rhs.facts &&
        lhs.against == rhs.against
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(facts)
        hasher.combine(against)
    }
}
