//
//  EventsResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 20/01/22.
//

import Foundation
import SwiftUI

public enum TypeEvent: String, Codable {
    case card = "Card"
    case subst = "subst"
    case goal = "Goal"
    case _var = "Var"
    
}

public class EventsResponse: BaseResponse {
    
    public var response: [Events]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [Events]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([Events].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}


public struct Events: Codable, Hashable {
    
    var time: Time?
    var team: Team?
    var player: Player?
    var assist: Player?
    var type: TypeEvent?
    var detail: String?
    var comments: String?
    
    public init(time: Time? = nil, team: Team? = nil, player: Player? = nil, assist: Player? = nil, type: TypeEvent? = nil, detail: String? = nil, comments: String? = nil) {
        self.time = time
        self.team = team
        self.player = player
        self.assist = assist
        self.type = type
        self.detail = detail
        self.comments = comments
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        time = (try? container.decodeIfPresent(Time.self, forKey: .time))
        team = (try? container.decodeIfPresent(Team.self, forKey: .team))
        player = (try? container.decodeIfPresent(Player.self, forKey: .player))
        assist = (try? container.decodeIfPresent(Player.self, forKey: .assist))
        if let typeString = (try? container.decodeIfPresent(String.self, forKey: .type)) {
            type = TypeEvent(rawValue: typeString)
        }
        detail = (try? container.decodeIfPresent(String.self, forKey: .detail))
        comments = (try? container.decodeIfPresent(String.self, forKey: .comments))
    }
    
    enum CodingKeys: String, CodingKey {
        case time
        case team
        case player
        case assist
        case type
        case detail
        case comments
    }
    
    public static func == (lhs: Events, rhs: Events) -> Bool {
        lhs.time?.elapsed == rhs.time?.elapsed &&
        lhs.team?.id == rhs.team?.id &&
        lhs.player?.id == rhs.player?.id &&
        lhs.type == rhs.type &&
        lhs.assist?.id == rhs.assist?.id &&
        lhs.detail == rhs.detail &&
        lhs.comments == rhs.comments
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(time)
        hasher.combine(team)
        hasher.combine(player)
        hasher.combine(type)
        hasher.combine(assist)
        hasher.combine(detail)
        hasher.combine(comments)
    }
}


public struct Time: Codable, Hashable {
    
    var elapsed: Int?
    var extra: String?
    
    public init(elapsed: Int? = nil, extra: String? = nil) {
        self.elapsed = elapsed
        self.extra = extra
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        elapsed = (try? container.decodeIfPresent(Int.self, forKey: .elapsed))
        extra = (try? container.decodeIfPresent(String.self, forKey: .extra))
    }
    
    enum CodingKeys: String, CodingKey {
        case elapsed
        case extra
    }
    
    public static func == (lhs: Time, rhs: Time) -> Bool {
        lhs.elapsed == rhs.elapsed &&
        lhs.extra == rhs.extra
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(elapsed)
        hasher.combine(extra)
    }
    
}

public struct Player: Codable, Hashable {
   
    var id: Int?
    var name: String?
    var number: Int?
    var pos: String?
    var grid: String?
    
    public init(id: Int? = nil, name: String? = nil) {
        self.id = id
        self.name = name
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        number = (try? container.decodeIfPresent(Int.self, forKey: .number))
        pos = (try? container.decodeIfPresent(String.self, forKey: .pos))
        grid = (try? container.decodeIfPresent(String.self, forKey: .grid))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case number
        case pos
        case grid
    }
    
    public static func == (lhs: Player, rhs: Player) -> Bool {
        lhs.id == rhs.id
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
}
