//
//  FixtureResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 03/08/22.
//

import Foundation
import SwiftUI

public class FixtureResponse: BaseResponse {
    
    var response: [MatchDay]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [MatchDay]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([MatchDay].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}
