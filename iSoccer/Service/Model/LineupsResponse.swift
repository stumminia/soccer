//
//  LineupsResponse.swift
//  iSoccer
//
//  Created by Giuseppe Lisanti on 20/01/22.
//

import Foundation
public class LineupsResponse: BaseResponse {
    
    public var response: [Lineup]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [Lineup]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([Lineup].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
}

public struct Lineup: Codable {

    public var startXI: [PlayerElement]?
    public var substitutes: [PlayerElement]?
    public var coach: Coach?
    public var team: Team?
    public var formation: String?
    

    internal init() {
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        startXI = (try? container.decodeIfPresent([PlayerElement].self, forKey: .startXI))
        substitutes = (try? container.decodeIfPresent([PlayerElement].self, forKey: .substitutes))
        coach = (try? container.decodeIfPresent(Coach.self, forKey: .coach))
        team = (try? container.decodeIfPresent(Team.self, forKey: .team))
        formation = (try? container.decodeIfPresent(String.self, forKey: .formation))

    }
    
    enum CodingKeys: String, CodingKey {
        case startXI
        case substitutes
        case coach
        case team
        case formation

    }
}

public struct PlayerElement: Codable, Hashable {

    public var player:  Player?
      
    internal init() {
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        player = (try? container.decodeIfPresent(Player.self, forKey: .player))
    }
    
    enum CodingKeys: String, CodingKey {
        case player

    }
}

public struct Coach: Codable {

    var id: Int?
    var name: String?
    var photo: String?
    
    internal init() {
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        photo = (try? container.decodeIfPresent(String.self, forKey: .photo))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case photo

    }
}
