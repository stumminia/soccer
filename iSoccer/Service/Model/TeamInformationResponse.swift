//
//  TeamInformationResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 11/03/22.
//

import Foundation
import SwiftUI

public class TeamInformationResponse: BaseResponse {
    
    var response: [TeamInformation]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [TeamInformation]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([TeamInformation].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
    
}

public struct TeamInformation: Codable, Hashable {
    
    var team: TeamInfo?
    var venue: VenueInfo?
    
    public init(team: TeamInfo? = nil, venue: VenueInfo? = nil) {
        self.team = team
        self.venue = venue
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        team = (try? container.decodeIfPresent(TeamInfo.self, forKey: .team))
        venue = (try? container.decodeIfPresent(VenueInfo.self, forKey: .venue))
    }
    
    enum CodingKeys: String, CodingKey {
        case team
        case venue
    }
    
}

public struct TeamInfo: Codable, Hashable {
    
    var id: Int?
    var name: String?
    var code: String?
    var country: String?
    var founded: Int?
    var national: Bool?
    var logo: String?
    
    public init(id: Int? = nil, name: String? = nil, code: String? = nil, country: String? = nil, founded: Int? = nil, national: Bool? = nil, logo: String? = nil) {
        self.id = id
        self.name = name
        self.code = code
        self.country = country
        self.founded = founded
        self.national = national
        self.logo = logo
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        code = (try? container.decodeIfPresent(String.self, forKey: .code))
        country = (try? container.decodeIfPresent(String.self, forKey: .country))
        founded = (try? container.decodeIfPresent(Int.self, forKey: .founded))
        national = (try? container.decodeIfPresent(Bool.self, forKey: .national))
        logo = (try? container.decodeIfPresent(String.self, forKey: .logo))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case code
        case country
        case founded
        case national
        case logo
    }
    
}


public struct VenueInfo: Codable, Hashable {
    
    var id: Int?
    var name: String?
    var address: String?
    var city: String?
    var capacity: Int?
    var surface: String?
    var image: String?
    
    public init(id: Int? = nil, name: String? = nil, address: String? = nil, city: String? = nil, capacity: Int? = nil, surface: String? = nil, image: String? = nil) {
        self.id = id
        self.name = name
        self.address = address
        self.city = city
        self.capacity = capacity
        self.surface = surface
        self.image = image
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        address = (try? container.decodeIfPresent(String.self, forKey: .address))
        city = (try? container.decodeIfPresent(String.self, forKey: .city))
        capacity = (try? container.decodeIfPresent(Int.self, forKey: .capacity))
        surface = (try? container.decodeIfPresent(String.self, forKey: .surface))
        image = (try? container.decodeIfPresent(String.self, forKey: .image))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case address
        case city
        case capacity
        case surface
        case image
    }
    
}
