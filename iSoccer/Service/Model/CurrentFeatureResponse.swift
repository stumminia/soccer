//
//  CurrentFeatureResponse.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 29/11/21.
//

import Foundation


public class CurrentFeatureResponse: BaseResponse {
    
    public var response: [String]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [String]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([String].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
}
