//
//  StatisticsResponse.swift
//  iSoccer
//
//  Created by Giuseppe Lisanti on 31/01/22.
//

import Foundation
public class StatisticsResponse: BaseResponse {

    public var response: [TeamStatisics]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [TeamStatisics]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([TeamStatisics].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
}

public struct TeamStatisics: Codable {

    public var team: Team?
    public var statistics: [Statistic]?
   

    internal init() {
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        team = (try? container.decodeIfPresent(Team.self, forKey: .team))
        statistics = (try? container.decodeIfPresent([Statistic].self, forKey: .statistics))

    }
    
    enum CodingKeys: String, CodingKey {
        case team
        case statistics

    }
}

public struct Statistic: Codable {

    public var statisticType: StatisticType?
    public var statisticValue: String?
   
    internal init() {
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let statisticString = (try? container.decodeIfPresent(String.self, forKey: .statisticType)) {
            statisticType = StatisticType(rawValue: statisticString)
        }
        if let stringValue = try? container.decodeIfPresent(String.self, forKey: .statisticValue) {
            statisticValue = stringValue
        } else if let intValue = try? container.decodeIfPresent(Int.self, forKey: .statisticValue) {
            statisticValue = intValue.description
        } else if let doubleValue = try? container.decodeIfPresent(Double.self, forKey: .statisticValue) {
            statisticValue = doubleValue.description
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case statisticType = "type"
        case statisticValue = "value"

    }
}

public enum StatisticType: String, Codable {
    case shotsOnGoal = "Shots on Goal"
    case shotsOffGoal = "Shots off Goal"
    case totalShots = "Total Shots"
    case blockedShots = "Blocked Shots"
    case shotsInsidebox = "Shots insidebox"
    case shotsOutsidebox = "Shots outsidebox"
    case fouls = "Fouls"
    case cornerKicks = "Corner Kicks"
    case offsides = "Offsides"
    case ballPossession = "Ball Possession"
    case yellowCards = "Yellow Cards"
    case redCards = "Red Cards"
    case goalkeeperSaves = "Goalkeeper Saves"
    case totalPasses = "Total passes"
    case passesAccurate = "Passes accurate"
    case passesPercentage = "Passes %"
    
    static func getLocalizable(type: StatisticType?) -> String {
        
        guard let statType = type else { return "" }
        
        switch statType {
        case .shotsOnGoal:
            return "match.statistics.shots.on.goal"
        case .shotsOffGoal:
            return "match.statistics.shots.off.goal"
        case .totalShots:
            return "match.statistics.shots.total"
        case .blockedShots:
            return "match.statistics.shots.blocked"
        case .shotsInsidebox:
            return "match.statistics.shots.inside.box"
        case .shotsOutsidebox:
            return "match.statistics.shots.outside.box"
        case .fouls:
            return "match.statistics.fouls"
        case .cornerKicks:
            return "match.statistics.corner.kicks"
        case .offsides:
            return "match.statistics.offside"
        case .ballPossession:
            return "match.statistics.ball.possession"
        case .yellowCards:
            return "match.statistics.yellow.cards"
        case .redCards:
            return "match.statistics.red.cards"
        case .goalkeeperSaves:
            return "match.statistics.goalkeeper.saves"
        case .totalPasses:
            return "match.statistics.total.passes"
        case .passesAccurate:
            return "match.statistics.passes.accurate"
        case .passesPercentage:
            return "match.statistics.passes.percentage"
        }
        
    }
    
}
