//
//  TeamStatisticsResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 11/03/22.
//

import Foundation
import SwiftUI

public class TeamStatisticsResponse: BaseResponse {
    
    var response: TeamStatistics?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: TeamStatistics? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent(TeamStatistics.self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
}

public struct TeamStatistics: Codable {
    
    var league: LeagueStandings?
    var team: Team?
    var form: String?
    var fixtures: FixturesTeam?
    var goals: GoalStatistics?
    var biggest: Biggest?
    var clean_sheet: FixtureItem?
    var failed_to_score: FixtureItem?
    var penalty: PenaltyStatisticItem?
    var lineups: [LineupsItem]?
    var cards: CardsStatistics?
    
    public init(league: LeagueStandings? = nil, team: Team? = nil, form: String? = nil, fixtures: FixturesTeam? = nil, goals: GoalStatistics? = nil, biggest: Biggest? = nil, clean_sheet: FixtureItem? = nil, failed_to_score: FixtureItem? = nil, penalty: PenaltyStatisticItem? = nil, lineups: [LineupsItem]? = nil, cards: CardsStatistics? = nil) {
        self.league = league
        self.team = team
        self.form = form
        self.fixtures = fixtures
        self.goals = goals
        self.biggest = biggest
        self.clean_sheet = clean_sheet
        self.failed_to_score = failed_to_score
        self.penalty = penalty
        self.lineups = lineups
        self.cards = cards
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        league = (try? container.decodeIfPresent(LeagueStandings.self, forKey: .league))
        team = (try? container.decodeIfPresent(Team.self, forKey: .team))
        form = (try? container.decodeIfPresent(String.self, forKey: .form))
        fixtures = (try? container.decodeIfPresent(FixturesTeam.self, forKey: .fixtures))
        goals = (try? container.decodeIfPresent(GoalStatistics.self, forKey: .goals))
        biggest = (try? container.decodeIfPresent(Biggest.self, forKey: .biggest))
        clean_sheet = (try? container.decodeIfPresent(FixtureItem.self, forKey: .clean_sheet))
        failed_to_score = (try? container.decodeIfPresent(FixtureItem.self, forKey: .failed_to_score))
        penalty = (try? container.decodeIfPresent(PenaltyStatisticItem.self, forKey: .penalty))
        lineups = (try? container.decodeIfPresent([LineupsItem].self, forKey: .lineups))
        cards = (try? container.decodeIfPresent(CardsStatistics.self, forKey: .cards))
    }
    
    enum CodingKeys: String, CodingKey {
        case league
        case team
        case form
        case fixtures
        case goals
        case biggest
        case clean_sheet
        case failed_to_score
        case penalty
        case lineups
        case cards
    }
}

public struct FixturesTeam: Codable {
    
    var played: FixtureItem?
    var wins: FixtureItem?
    var draws: FixtureItem?
    var loses: FixtureItem?
    
    public init(played: FixtureItem? = nil, wins: FixtureItem? = nil, draws: FixtureItem? = nil, loses: FixtureItem? = nil) {
        self.played = played
        self.wins = wins
        self.draws = draws
        self.loses = loses
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        played = (try? container.decodeIfPresent(FixtureItem.self, forKey: .played))
        wins = (try? container.decodeIfPresent(FixtureItem.self, forKey: .wins))
        draws = (try? container.decodeIfPresent(FixtureItem.self, forKey: .draws))
        loses = (try? container.decodeIfPresent(FixtureItem.self, forKey: .loses))
    }
    
    enum CodingKeys: String, CodingKey {
        case played
        case wins
        case draws
        case loses
    }
    
}


public struct FixtureItem: Codable {
    
    var home: Int?
    var away: Int?
    var total: Int?
    
    public init(home: Int? = nil, away: Int? = nil, total: Int? = nil) {
        self.home = home
        self.away = away
        self.total = total
    }
    
    public init(from decoder: Decoder) throws {
        let controller = try decoder.container(keyedBy: CodingKeys.self)
        home = (try? controller.decodeIfPresent(Int.self, forKey: .home))
        away = (try? controller.decodeIfPresent(Int.self, forKey: .away))
        total = (try? controller.decodeIfPresent(Int.self, forKey: .total))
    }
    
    enum CodingKeys: String, CodingKey {
        case home
        case away
        case total
    }
    
}

public struct GoalStatistics: Codable {
    
    var facts: GoalItem?
    var against: GoalItem?
    
    public init(facts: GoalItem? = nil, against: GoalItem? = nil) {
        self.facts = facts
        self.against = against
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        facts = (try? container.decodeIfPresent(GoalItem.self, forKey: .facts))
        against = (try? container.decodeIfPresent(GoalItem.self, forKey: .against))
    }
    
    enum CodingKeys: String, CodingKey {
        case facts = "for"
        case against = "against"
    }
    
}

public struct GoalItem: Codable {
    
    var total: FixtureItem?
    var average: FixtureItem?
    var minute: MinuteGoalStatistic?
    
    public init(total: FixtureItem? = nil, average: FixtureItem? = nil, minute: MinuteGoalStatistic? = nil) {
        self.total = total
        self.average = average
        self.minute = minute
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        total = (try? container.decodeIfPresent(FixtureItem.self, forKey: .total))
        average = (try? container.decodeIfPresent(FixtureItem.self, forKey: .average))
        minute = (try? container.decodeIfPresent(MinuteGoalStatistic.self, forKey: .minute))
    }
    
    enum CodingKeys: String, CodingKey {
        case total
        case average
        case minute
    }
    
}

public struct MinuteGoalStatistic: Codable {
    
    var _0To15: MinuteGoalItem?
    var _16To30: MinuteGoalItem?
    var _31To45: MinuteGoalItem?
    var _46To60: MinuteGoalItem?
    var _61To75: MinuteGoalItem?
    var _76To90: MinuteGoalItem?
    var _91To105: MinuteGoalItem?
    var _106To120: MinuteGoalItem?
    
    public init(_0To15: MinuteGoalItem? = nil, _16To30: MinuteGoalItem? = nil, _31To45: MinuteGoalItem? = nil, _46To60: MinuteGoalItem? = nil, _61To75: MinuteGoalItem? = nil, _76To90: MinuteGoalItem? = nil, _91To105: MinuteGoalItem? = nil, _106To120: MinuteGoalItem? = nil) {
        self._0To15 = _0To15
        self._16To30 = _16To30
        self._31To45 = _31To45
        self._46To60 = _46To60
        self._61To75 = _61To75
        self._76To90 = _76To90
        self._91To105 = _91To105
        self._106To120 = _106To120
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        _0To15 = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: ._0To15))
        _16To30 = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: ._16To30))
        _31To45 = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: ._31To45))
        _46To60 = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: ._46To60))
        _61To75 = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: ._61To75))
        _76To90 = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: ._76To90))
        _91To105 = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: ._91To105))
        _106To120 = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: ._106To120))
    }
    
    
    enum CodingKeys: String, CodingKey {
        
        case _0To15 = "0-15"
        case _16To30 = "16-30"
        case _31To45 = "31-45"
        case _46To60 = "46-60"
        case _61To75 = "61-75"
        case _76To90 = "76-90"
        case _91To105 = "91-105"
        case _106To120 = "106-120"
    }
    
}

public struct MinuteGoalItem: Codable {
    
    var total: Int?
    var percentage: String?
    
    public init(total: Int? = nil, percentage: String? = nil) {
        self.total = total
        self.percentage = percentage
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        total = (try? container.decodeIfPresent(Int.self, forKey: .total))
        percentage = (try? container.decodeIfPresent(String.self, forKey: .percentage))
    }
    
    enum CodingKeys: String, CodingKey {
        case total
        case percentage
    }
    
}


public struct Biggest: Codable {
    
    var streak: Streak?
    var wins: BiggestItemString?
    var loses: BiggestItemString?
    var goals: GoalsBiggest?
    
    public init(streak: Streak? = nil, wins: BiggestItemString? = nil, loses: BiggestItemString? = nil, goals: GoalsBiggest? = nil) {
        self.streak = streak
        self.wins = wins
        self.loses = loses
        self.goals = goals
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        streak = (try? container.decodeIfPresent(Streak.self, forKey: .streak))
        wins = (try? container.decodeIfPresent(BiggestItemString.self, forKey: .wins))
        loses = (try? container.decodeIfPresent(BiggestItemString.self, forKey: .loses))
        goals = (try? container.decodeIfPresent(GoalsBiggest.self, forKey: .goals))
    }
    
    enum CodingKeys: String, CodingKey {
        case streak
        case wins
        case loses
        case goals
    }
    
}

public struct Streak: Codable {
    
    var wins: Int?
    var draws: Int?
    var loses: Int?
    
    public init(wins: Int? = nil, draws: Int? = nil, loses: Int? = nil) {
        self.wins = wins
        self.draws = draws
        self.loses = loses
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        wins = (try? container.decodeIfPresent(Int.self, forKey: .wins))
        draws = (try? container.decodeIfPresent(Int.self, forKey: .draws))
        loses = (try? container.decodeIfPresent(Int.self, forKey: .loses))
    }
    
    enum CodingKeys: String, CodingKey {
        case wins
        case draws
        case loses
    }
}

public struct BiggestItemString: Codable {
    
    var home: String?
    var away: String?
    
    public init(home: String? = nil, away: String? = nil) {
        self.home = home
        self.away = away
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        home = (try? container.decodeIfPresent(String.self, forKey: .home))
        away = (try? container.decodeIfPresent(String.self, forKey: .away))
    }
    
    enum CodingKeys: String, CodingKey {
        case home
        case away
    }
}

public struct GoalsBiggest: Codable {
    
    var facts: FixtureItem?
    var against: FixtureItem?
    
    public init(facts: FixtureItem? = nil, against: FixtureItem? = nil) {
        self.facts = facts
        self.against = against
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        facts = (try? container.decodeIfPresent(FixtureItem.self, forKey: .facts))
        against = (try? container.decodeIfPresent(FixtureItem.self, forKey: .against))
    }
    
    enum CodingKeys: String, CodingKey {
        case facts = "for"
        case against = "against"
    }
    
}

public struct PenaltyStatisticItem: Codable {
    
    var scored: MinuteGoalItem?
    var missed: MinuteGoalItem?
    var total: Int?
    
    public init(scored: MinuteGoalItem? = nil, missed: MinuteGoalItem? = nil, total: Int? = nil) {
        self.scored = scored
        self.missed = missed
        self.total = total
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        scored = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: .scored))
        missed = (try? container.decodeIfPresent(MinuteGoalItem.self, forKey: .missed))
        total = (try? container.decodeIfPresent(Int.self, forKey: .total))
    }
    
    enum CodingKeys: String, CodingKey {
        case scored
        case missed
        case total
    }
    
}

public struct LineupsItem: Codable {
    
    var formation: String?
    var played: Int?
    
    public init(formation: String? = nil, played: Int? = nil) {
        self.formation = formation
        self.played = played
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        formation = (try? container.decodeIfPresent(String.self, forKey: .formation))
        played = (try? container.decodeIfPresent(Int.self, forKey: .played))
    }
    
    enum CodingKeys: String, CodingKey {
        case formation
        case played
    }
    
}

public struct CardsStatistics: Codable {
    
    var yellow: MinuteGoalStatistic?
    var red: MinuteGoalStatistic?
    
    public init(yellow: MinuteGoalStatistic? = nil, red: MinuteGoalStatistic? = nil) {
        self.yellow = yellow
        self.red = red
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        yellow = (try? container.decodeIfPresent(MinuteGoalStatistic.self, forKey: .yellow))
        red = (try? container.decodeIfPresent(MinuteGoalStatistic.self, forKey: .red))
    }
    
    enum CodingKeys: String, CodingKey {
        case yellow
        case red
    }
    
}
