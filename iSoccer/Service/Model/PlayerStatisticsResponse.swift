//
//  PlayerStatisticsResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 06/09/22.
//

import Foundation
import SwiftUI

public class PlayerStatisticsResponse: BaseResponse {
    
    var response: ResponsePlayer?
    
    init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: ResponsePlayer? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent(ResponsePlayer.self, forKey: .response))
    }
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}

public struct ResponsePlayer: Codable {
    
    var player: PlayerStatistics?
    var statistics: [Statistics]?
    
    public init(player: PlayerStatistics? = nil, statistics: [Statistics]? = nil) {
        self.player = player
        self.statistics = statistics
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.player = try container.decodeIfPresent(PlayerStatistics.self, forKey: .player)
        self.statistics = try container.decodeIfPresent([Statistics].self, forKey: .statistics)
    }
    
    enum CodingKeys: String, CodingKey {
        case player
        case statistics
    }
    
}

public struct PlayerStatistics: Codable {

    var id: Int?
    var name: String?
    var firstname: String?
    var lastname: String?
    var age: Int?
    var birth: BirthPlayer?
    var nationality: String?
    var height: String?
    var weight: String?
    var injured: Bool?
    var photo: String?
    
    public init(id: Int? = nil, name: String? = nil, firstname: String? = nil, lastname: String? = nil, age: Int? = nil, birth: BirthPlayer? = nil, nationality: String? = nil, height: String? = nil, weight: String? = nil, injured: Bool? = nil, photo: String? = nil) {
        self.id = id
        self.name = name
        self.firstname = firstname
        self.lastname = lastname
        self.age = age
        self.birth = birth
        self.nationality = nationality
        self.height = height
        self.weight = weight
        self.injured = injured
        self.photo = photo
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.firstname = try container.decodeIfPresent(String.self, forKey: .firstname)
        self.lastname = try container.decodeIfPresent(String.self, forKey: .lastname)
        self.age = try container.decodeIfPresent(Int.self, forKey: .age)
        self.birth = try container.decodeIfPresent(BirthPlayer.self, forKey: .birth)
        self.nationality = try container.decodeIfPresent(String.self, forKey: .nationality)
        self.height = try container.decodeIfPresent(String.self, forKey: .height)
        self.weight = try container.decodeIfPresent(String.self, forKey: .weight)
        self.injured = try container.decodeIfPresent(Bool.self, forKey: .injured)
        self.photo = try container.decodeIfPresent(String.self, forKey: .photo)
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case firstname
        case lastname
        case age
        case birth
        case nationality
        case height
        case weight
        case injured
        case photo
    }
    
}

public struct BirthPlayer: Codable {
    
    var date: String?
    var place: String?
    var country: String?
    
    public init(date: String? = nil, place: String? = nil, country: String? = nil) {
        self.date = date
        self.place = place
        self.country = country
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.date = try container.decodeIfPresent(String.self, forKey: .date)
        self.place = try container.decodeIfPresent(String.self, forKey: .place)
        self.country = try container.decodeIfPresent(String.self, forKey: .country)
    }
    
    enum CodingKeys: String, CodingKey {
        
        case date
        case place
        case country
        
    }
    
}
