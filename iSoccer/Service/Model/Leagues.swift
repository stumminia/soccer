//
//  Leagues.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 04/11/21.
//

import Foundation

public struct Leagues: Codable, Identifiable {
    
    
    public var id:UUID?
    var league: League?
    var country: Country?
    var seasons: [Season]?
    
    var preferred: Bool = false
    
    
    public init(league: League? = nil, country: Country? = nil, seasons: [Season]? = nil) {
        id = UUID()
        self.league = league
        self.country = country
        self.seasons = seasons
    }
    
    public init(from decoder: Decoder) throws {
        id = UUID()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        league = (try? container.decodeIfPresent(League.self, forKey: .league))
        country = (try? container.decodeIfPresent(Country.self, forKey: .country))
        seasons = (try? container.decodeIfPresent([Season].self, forKey: .seasons))
    }
    
    enum CodingKeys: String, CodingKey {
        case league
        case country
        case seasons
    }
}


///League model
public struct League: Codable {
    
    var id: Int?
    var name: String?
    var type: String?
    var logo: String?
    
    
    public init(id: Int? = nil, name: String? = nil, type: String? = nil, logo: String? = nil) {
        self.id = id
        self.name = name
        self.type = type
        self.logo = logo
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        type = (try? container.decodeIfPresent(String.self, forKey: .type))
        logo = (try? container.decodeIfPresent(String.self, forKey: .logo))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case type
        case logo
    }
}


/// Country model
public struct Country: Codable, Identifiable {
    
    public var id:UUID?
    var name: String?
    var code: String?
    var flag: String?
    
    public init(name: String? = nil, code: String? = nil, flag: String? = nil) {
        id = UUID()
        self.name = name
        self.code = code
        self.flag = flag
    }
    
    public init(from decoder: Decoder) throws {
        id = UUID()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        code = (try? container.decodeIfPresent(String.self, forKey: .code))
        flag = (try? container.decodeIfPresent(String.self, forKey: .flag))
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case code
        case flag
    }
    
}



public struct Season: Codable {
    
    var year: Int?
    var start: String?
    var end: String?
    var current: Bool?
    var coverage: Coverage?
    
    public init(year: Int? = nil, start: String? = nil, end: String? = nil, current: Bool? = nil, coverage: Coverage? = nil) {
        self.year = year
        self.start = start
        self.end = end
        self.current = current
        self.coverage = coverage
    }
    
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        year = (try? container.decodeIfPresent(Int.self, forKey: .year))
        start = (try? container.decodeIfPresent(String.self, forKey: .start))
        end = (try? container.decodeIfPresent(String.self, forKey: .end))
        current = (try? container.decodeIfPresent(Bool.self, forKey: .current))
        coverage = (try? container.decodeIfPresent(Coverage.self, forKey: .coverage))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case year
        case start
        case end
        case current
        case coverage
    }
}


public struct Coverage: Codable {
    
    var fixtures: Fixtures?
    var standings: Bool?
    var players: Bool?
    var top_scorers: Bool?
    var top_assists: Bool?
    var top_cards: Bool?
    var injuries: Bool?
    var predictions: Bool?
    var odds: Bool?
    
    
    public init(fixtures: Fixtures? = nil, standings: Bool? = nil, players: Bool? = nil, top_scorers: Bool? = nil, top_assists: Bool? = nil, top_cards: Bool? = nil, injuries: Bool? = nil, predictions: Bool? = nil, odds: Bool? = nil) {
        self.fixtures = fixtures
        self.standings = standings
        self.players = players
        self.top_scorers = top_scorers
        self.top_assists = top_assists
        self.top_cards = top_cards
        self.injuries = injuries
        self.predictions = predictions
        self.odds = odds
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        fixtures = (try? container.decodeIfPresent(Fixtures.self, forKey: .fixtures))
        standings = (try? container.decodeIfPresent(Bool.self, forKey: .standings))
        players = (try? container.decodeIfPresent(Bool.self, forKey: .players))
        top_scorers = (try? container.decodeIfPresent(Bool.self, forKey: .top_scorers))
        top_assists = (try? container.decodeIfPresent(Bool.self, forKey: .top_assists))
        top_cards = (try? container.decodeIfPresent(Bool.self, forKey: .top_cards))
        injuries = (try? container.decodeIfPresent(Bool.self, forKey: .injuries))
        predictions = (try? container.decodeIfPresent(Bool.self, forKey: .predictions))
        odds = (try? container.decodeIfPresent(Bool.self, forKey: .odds))
    }
    
    enum CodingKeys: String, CodingKey {
        case fixtures
        case standings
        case players
        case top_scorers
        case top_assists
        case top_cards
        case injuries
        case predictions
        case odds
    }
}


public struct Fixtures: Codable {
    
    
    var events: Bool?
    var lineups: Bool?
    var statistics_fixtures: Bool?
    var statistics_players: Bool?
    
    public init(events: Bool? = nil, lineups: Bool? = nil, statistics_fixtures: Bool? = nil, statistics_players: Bool? = nil) {
        self.events = events
        self.lineups = lineups
        self.statistics_fixtures = statistics_fixtures
        self.statistics_players = statistics_players
    }
    
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        events = (try? container.decodeIfPresent(Bool.self, forKey: .events))
        lineups = (try? container.decodeIfPresent(Bool.self, forKey: .lineups))
        statistics_fixtures = (try? container.decodeIfPresent(Bool.self, forKey: .statistics_fixtures))
        statistics_players = (try? container.decodeIfPresent(Bool.self, forKey: .statistics_players))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case events
        case lineups
        case statistics_fixtures
        case statistics_players
    }
    
}
