//
//  PlayerTranfersResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 06/09/22.
//

import Foundation
import SwiftUI

public class PlayerTranfersResponse: BaseResponse {
    
    var response: PlayerTransfers?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: PlayerTransfers? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent(PlayerTransfers.self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}

public struct PlayerTransfers: Codable {
    
    var player: Player?
    var transfers: [Tranfers]?
    
    public init(player: Player? = nil, transfers: [Tranfers]? = nil) {
        self.player = player
        self.transfers = transfers
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.player = try container.decodeIfPresent(Player.self, forKey: .player)
        self.transfers = try container.decodeIfPresent([Tranfers].self, forKey: .transfers)
    }
    
    enum CodingKeys: String, CodingKey {
        case player
        case transfers
    }
    
}

public struct Tranfers: Codable {
    
    var date: String?
    var type: String?
    var teams: TeamTranfer?
    
    public init(date: String? = nil, type: String? = nil, teams: TeamTranfer? = nil) {
        self.date = date
        self.type = type
        self.teams = teams
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        date = (try? container.decodeIfPresent(String.self, forKey: .date))
        type = (try? container.decodeIfPresent(String.self, forKey: .type))
        teams = (try? container.decodeIfPresent(TeamTranfer.self, forKey: .teams))
    }
    
    enum CodingKeys: String, CodingKey {
        case date
        case type
        case teams
    }
    
}

public struct TeamTranfer: Codable {
    
    var _in: Team?
    var out: Team?
    
    public init(_in: Team? = nil, out: Team? = nil) {
        self._in = _in
        self.out = out
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        _in = (try? container.decodeIfPresent(Team.self, forKey: ._in))
        out = (try? container.decodeIfPresent(Team.self, forKey: .out))
    }
    
    enum CodingKeys: String, CodingKey {
        case _in = "in"
        case out = "out"
    }
    
}
