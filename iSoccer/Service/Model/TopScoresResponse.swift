//
//  TopScoresResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/01/22.
//

import Foundation
import SwiftUI

public class TopScoresResponse: BaseResponse {

    var response: [ScoreList]?
    
    init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [ScoreList]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([ScoreList].self, forKey: .response))
    }
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}

public struct ScoreList: Codable {
    
    var player: PlayerTopScore?
    var statistics: [Statistics]?
    
    public init(player: PlayerTopScore? = nil, statistics: [Statistics]? = nil) {
        self.player = player
        self.statistics = statistics
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        player = (try? container.decodeIfPresent(PlayerTopScore.self, forKey: .player))
        statistics = (try? container.decodeIfPresent([Statistics].self, forKey: .statistics))
    }
    
    enum CodingKeys: String, CodingKey {
        case player
        case statistics
    }
    
}

public struct PlayerTopScore: Codable {
    
    var id: Int?
    var name: String?
    var firstname: String?
    var lastname: String?
    var age: Int?
    var birth: Birth?
    var nationality: String?
    var height: String?
    var weight: String?
    var injured: Bool?
    var photo: String?
    
    public init(id: Int? = nil, name: String? = nil, firstname: String? = nil, lastname: String? = nil, age: Int? = nil, birth: Birth? = nil, nationality: String? = nil, height: String? = nil, weight: String? = nil, injured: Bool? = nil, photo: String? = nil) {
        self.id = id
        self.name = name
        self.firstname = firstname
        self.lastname = lastname
        self.age = age
        self.birth = birth
        self.nationality = nationality
        self.height = height
        self.weight = weight
        self.injured = injured
        self.photo = photo
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        firstname = (try? container.decodeIfPresent(String.self, forKey: .firstname))
        lastname = (try? container.decodeIfPresent(String.self, forKey: .lastname))
        age = (try? container.decodeIfPresent(Int.self, forKey: .age))
        birth = (try? container.decodeIfPresent(Birth.self, forKey: .birth))
        nationality = (try? container.decodeIfPresent(String.self, forKey: .nationality))
        height = (try? container.decodeIfPresent(String.self, forKey: .height))
        weight = (try? container.decodeIfPresent(String.self, forKey: .weight))
        injured = (try? container.decodeIfPresent(Bool.self, forKey: .injured))
        photo = (try? container.decodeIfPresent(String.self, forKey: .photo))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case firstname
        case lastname
        case age
        case birth
        case nationality
        case height
        case weight
        case injured
        case photo
    }
    
}

public struct Birth: Codable {
    
    var date: Int64?
    var place: String?
    var country: String?
    
    ///To convert API result date (ISO8601) to `Date`, this property should not be inside any methods
    let inDateFormatter = ISO8601DateFormatter()
    
    public init(date: Int64? = nil, place: String? = nil, country: String? = nil) {
        self.date = date
        self.place = place
        self.country = country
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dateString = (try? container.decodeIfPresent(String.self, forKey: .date))
        date = inDateFormatter.date(from: dateString ?? "")?.millisecondsSince1970Int64
        place = (try? container.decodeIfPresent(String.self, forKey: .place))
        country = (try? container.decodeIfPresent(String.self, forKey: .country))
    }
    
    enum CodingKeys: String, CodingKey {
        case date
        case place
        case country
    }
    
}


public struct Statistics: Codable {
    
    var team: Team?
    var league: LeagueFixture?
    var games: GamesScores?
    var substitutes: Substitutes?
    var shots: Shots?
    var goals: GoalScores?
    var passes: Passes?
    var tackles: Tackles?
    var duels: Duels?
    var dribbles: Dribbles?
    var fouls: Fouls?
    var cards: Cards?
    var penalty: Penalty?
}

public struct GamesScores: Codable {
    
    var appearences: Int?
    var lineups: Int?
    var minutes: Int?
    var number: Int?
    var position: String?
    var rating: String?
    var captain: Bool?
    
    public init(appearences: Int? = nil, lineups: Int? = nil, minutes: Int? = nil, number: Int? = nil, position: String? = nil, rating: String? = nil, captain: Bool? = nil) {
        self.appearences = appearences
        self.lineups = lineups
        self.minutes = minutes
        self.number = number
        self.position = position
        self.rating = rating
        self.captain = captain
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        appearences = (try? container.decodeIfPresent(Int.self, forKey: .appearences))
        lineups = (try? container.decodeIfPresent(Int.self, forKey: .lineups))
        minutes = (try? container.decodeIfPresent(Int.self, forKey: .minutes))
        number = (try? container.decodeIfPresent(Int.self, forKey: .number))
        position = (try? container.decodeIfPresent(String.self, forKey: .position))
        rating = (try? container.decodeIfPresent(String.self, forKey: .rating))
        captain = (try? container.decodeIfPresent(Bool.self, forKey: .captain))
    }
    
    enum CodingKeys: String, CodingKey {
        case appearences
        case lineups
        case minutes
        case number
        case position
        case rating
        case captain
    }
    
}

public struct Substitutes: Codable {
    
    var _in: Int?
    var out: Int?
    var bench: Int?
    
    public init(_in: Int? = nil, out: Int? = nil, bench: Int? = nil) {
        self._in = _in
        self.out = out
        self.bench = bench
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        _in = (try? container.decodeIfPresent(Int.self, forKey: ._in))
        out = (try? container.decodeIfPresent(Int.self, forKey: .out))
        bench = (try? container.decodeIfPresent(Int.self, forKey: .bench))
    }
    
    enum CodingKeys: String, CodingKey {
        case _in = "in"
        case out = "out"
        case bench = "bench"
    }
    
}

public struct Shots: Codable {
    
    var total: Int?
    var on: Int?
    
    public init(total: Int? = nil, on: Int? = nil) {
        self.total = total
        self.on = on
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        total = (try? container.decodeIfPresent(Int.self, forKey: .total))
        on = (try? container.decodeIfPresent(Int.self, forKey: .on))
    }
    
    enum CodingKeys: String, CodingKey {
        case total
        case on
    }
    
}

public struct GoalScores: Codable {

    var total: Int?
    var conceded: Int?
    var assists: Int?
    var saves: Int?
    
    public init(total: Int? = nil, conceded: Int? = nil, assists: Int? = nil, saves: Int? = nil) {
        self.total = total
        self.conceded = conceded
        self.assists = assists
        self.saves = saves
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        total = (try? container.decodeIfPresent(Int.self, forKey: .total))
        conceded = (try? container.decodeIfPresent(Int.self, forKey: .conceded))
        assists = (try? container.decodeIfPresent(Int.self, forKey: .assists))
        saves = (try? container.decodeIfPresent(Int.self, forKey: .saves))
    }
    
    enum CodingKeys: String, CodingKey {
        case total
        case conceded
        case assists
        case saves
    }
    
}

public struct Passes: Codable {
        
    var total: Int?
    var key: Int?
    var accuracy: Int?
    
    public init(total: Int? = nil, key: Int? = nil, accuracy: Int? = nil) {
        self.total = total
        self.key = key
        self.accuracy = accuracy
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        total = (try? container.decodeIfPresent(Int.self, forKey: .total))
        key = (try? container.decodeIfPresent(Int.self, forKey: .key))
        accuracy = (try? container.decodeIfPresent(Int.self, forKey: .accuracy))
    }
    
    enum CodingKeys: String, CodingKey {
        case total
        case key
        case accuracy
    }
    
}

public struct Tackles: Codable {
    
    var total: Int?
    var blocks: Int?
    var interceptions: Int?
    
    public init(total: Int? = nil, blocks: Int? = nil, interceptions: Int? = nil) {
        self.total = total
        self.blocks = blocks
        self.interceptions = interceptions
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        total = (try? container.decodeIfPresent(Int.self, forKey: .total))
        blocks = (try? container.decodeIfPresent(Int.self, forKey: .blocks))
        interceptions = (try? container.decodeIfPresent(Int.self, forKey: .interceptions))
    }
    
    enum CodingKeys: String, CodingKey {
        case total
        case blocks
        case interceptions
    }
    
}

public struct Duels: Codable {

    var total: Int?
    var won: Int?
    
    public init(total: Int? = nil, won: Int? = nil) {
        self.total = total
        self.won = won
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        total = (try? container.decodeIfPresent(Int.self, forKey: .total))
        won = (try? container.decodeIfPresent(Int.self, forKey: .won))
    }
    
    enum CodingKeys: String, CodingKey {
        case total
        case won
    }
    
}

public struct Dribbles: Codable {
    
    var attempts: Int?
    var success: Int?
    var past: Int?
    
    public init(attempts: Int? = nil, success: Int? = nil, past: Int? = nil) {
        self.attempts = attempts
        self.success = success
        self.past = past
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        attempts = (try? container.decodeIfPresent(Int.self, forKey: .attempts))
        success = (try? container.decodeIfPresent(Int.self, forKey: .success))
        past = (try? container.decodeIfPresent(Int.self, forKey: .past))
    }
    
    enum CodingKeys: String, CodingKey {
        case attempts
        case success
        case past
    }
    
}

public struct Fouls: Codable {
    
    var drawn: Int?
    var committed: Int?
    
    public init(drawn: Int? = nil, committed: Int? = nil) {
        self.drawn = drawn
        self.committed = committed
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        drawn = (try? container.decodeIfPresent(Int.self, forKey: .drawn))
        committed = (try? container.decodeIfPresent(Int.self, forKey: .committed))
    }
    
    enum CodingKeys: String, CodingKey {
        case drawn
        case committed
    }
}

public struct Cards: Codable {
    
    var yellow: Int?
    var yellowred: Int?
    var red: Int?
    
    public init(yellow: Int? = nil, yellowred: Int? = nil, red: Int? = nil) {
        self.yellow = yellow
        self.yellowred = yellowred
        self.red = red
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        yellow = (try? container.decodeIfPresent(Int.self, forKey: .yellow))
        yellowred = (try? container.decodeIfPresent(Int.self, forKey: .yellowred))
        red = (try? container.decodeIfPresent(Int.self, forKey: .red))
    }
    
    enum CodingKeys: String, CodingKey {
        case yellow
        case yellowred
        case red
    }
    
}

public struct Penalty: Codable {
    
    var won: Int?
    var commited: Int?
    var scored: Int?
    var missed: Int?
    var saved: Int?
    
    public init(won: Int? = nil, commited: Int? = nil, scored: Int? = nil, missed: Int? = nil, saved: Int? = nil) {
        self.won = won
        self.commited = commited
        self.scored = scored
        self.missed = missed
        self.saved = saved
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        won = (try? container.decodeIfPresent(Int.self, forKey: .won))
        commited = (try? container.decodeIfPresent(Int.self, forKey: .commited))
        scored = (try? container.decodeIfPresent(Int.self, forKey: .scored))
        missed = (try? container.decodeIfPresent(Int.self, forKey: .missed))
        saved = (try? container.decodeIfPresent(Int.self, forKey: .saved))
    }
    
    enum CodingKeys: String, CodingKey {
        case won
        case commited
        case scored
        case missed
        case saved
    }
    
}
