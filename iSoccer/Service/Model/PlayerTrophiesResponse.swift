//
//  PlayerTrophiesResponse.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 07/09/22.
//

import Foundation
import SwiftUI

public class PlayerTrophiesResponse: BaseResponse {
    
    var response: [PlayerTrophies]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [PlayerTrophies]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([PlayerTrophies].self, forKey: .response))
    }
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}

public struct PlayerTrophies: Codable {
    
    var league: String?
    var country: String?
    var season: String?
    var place: String?
    
    public init(league: String? = nil, country: String? = nil, season: String? = nil, place: String? = nil) {
        self.league = league
        self.country = country
        self.season = season
        self.place = place
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.league = try container.decodeIfPresent(String.self, forKey: .league)
        self.country = try container.decodeIfPresent(String.self, forKey: .country)
        self.season = try container.decodeIfPresent(String.self, forKey: .season)
        self.place = try container.decodeIfPresent(String.self, forKey: .place)
    }
    
    enum CodingKeys: String, CodingKey {
        case league
        case country
        case season
        case place
    }
    
}
