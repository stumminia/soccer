//
//  LeagueResponse.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 18/11/21.
//

import Foundation

public class LeagueResponse: BaseResponse {
    
    public var response: [Leagues]?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [Leagues]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([Leagues].self, forKey: .response))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case response
    }
}

        

