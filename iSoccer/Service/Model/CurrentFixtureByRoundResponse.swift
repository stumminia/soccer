//
//  CurrentFixtureByRoundResponse.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 29/11/21.
//

import Foundation


public class CurrentFixtureByRoundResponse: BaseResponse {
    
    var response: [MatchDay]?
    
    init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil, response: [MatchDay]? = nil) {
        super.init(get: get, parameters: parameters, errors: errors, results: results, paging: paging)
        self.response = response
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = (try? container.decodeIfPresent([MatchDay].self, forKey: .response))
    }
    
    enum CodingKeys: String, CodingKey {
        case response
    }
    
}

public struct MatchDay: Codable, Identifiable, Hashable {
    
    public var id:UUID?
    var fixture: Fixture?
    var league: LeagueFixture?
    var teams: Teams?
    var goals: Goals?
    var score: Score?
    
    init(fixture: Fixture? = nil, league: LeagueFixture? = nil, teams: Teams? = nil, goals: Goals? = nil, score: Score? = nil) {
        self.id = UUID()
        self.fixture = fixture
        self.league = league
        self.teams = teams
        self.goals = goals
        self.score = score
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        fixture = (try? container.decodeIfPresent(Fixture.self, forKey: .fixture))
        league = (try? container.decodeIfPresent(LeagueFixture.self, forKey: .league))
        teams = (try? container.decodeIfPresent(Teams.self, forKey: .teams))
        goals = (try? container.decodeIfPresent(Goals.self, forKey: .goals))
        score = (try? container.decodeIfPresent(Score.self, forKey: .score))
    }
    
    enum CodingKeys: String, CodingKey {
        case fixture
        case league
        case teams
        case goals
        case score
    }
    
    public static func == (lhs: MatchDay, rhs: MatchDay) -> Bool {
        lhs.fixture?.id == rhs.fixture?.id
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(fixture?.id)
    }
    
}

public struct Fixture: Codable {
    
    var id: Int?
    var referee: String?
    var timezone: String?
    var date: String?
    var timestamp: Int64?
    var periods: Periods?
    var venue: Venue?
    var status: Status?
    
    init(id: Int? = nil, referee: String? = nil, timezone: String? = nil, date: String? = nil, timestamp: Int64? = nil, periods: Periods? = nil, venue: Venue? = nil, status: Status? = nil) {
        self.id = id
        self.referee = referee
        self.timezone = timezone
        self.date = date
        self.timestamp = timestamp
        self.periods = periods
        self.venue = venue
        self.status = status
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        referee = (try? container.decodeIfPresent(String.self, forKey: .referee))
        timezone = (try? container.decodeIfPresent(String.self, forKey: .timezone))
        date = (try? container.decodeIfPresent(String.self, forKey: .date))
        timestamp = (try? container.decodeIfPresent(Int64.self, forKey: .timestamp))
        periods = (try? container.decodeIfPresent(Periods.self, forKey: .periods))
        venue = (try? container.decodeIfPresent(Venue.self, forKey: .venue))
        status = (try? container.decodeIfPresent(Status.self, forKey: .status))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case referee
        case timezone
        case date
        case timestamp
        case periods
        case venue
        case status
    }
}

public struct Periods: Codable {
 
    var first: Int64?
    var second: Int64?
    
    init(first: Int64? = nil, second: Int64? = nil) {
        self.first = first
        self.second = second
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        first = (try? container.decodeIfPresent(Int64.self, forKey: .first))
        second = (try? container.decodeIfPresent(Int64.self, forKey: .second))
    }
 
    enum CodingKeys: String, CodingKey {
        case first
        case second
    }
    
}

public struct Venue: Codable {
    
    var id: Int?
    var name: String?
    var city: String?
    
    init(id: Int? = nil, name: String? = nil, city: String? = nil) {
        self.id = id
        self.name = name
        self.city = city
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        city = (try? container.decodeIfPresent(String.self, forKey: .city))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case city
    }
    
}


public struct Status: Codable {

    var long: String?
    var short: StatusMatch?
    var elapsed: Int?
    
    init(long: String? = nil, short: StatusMatch? = nil, elapsed: Int? = nil) {
        self.long = long
        self.short = short
        self.elapsed = elapsed
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        long = (try? container.decodeIfPresent(String.self, forKey: .long))
        short = (try? container.decodeIfPresent(StatusMatch.self, forKey: .short))
        elapsed = (try? container.decodeIfPresent(Int.self, forKey: .elapsed))
    }
    
    enum CodingKeys: String, CodingKey {
        case long
        case short
        case elapsed
    }
    
}

public struct LeagueFixture: Codable {
    
    var id: Int?
    var name: String?
    var country: String?
    var logo: String?
    var flag: String?
    var season: Int?
    var round: String?
    
    init(id: Int? = nil, name: String? = nil, country: String? = nil, logo: String? = nil, flag: String? = nil, season: Int? = nil, round: String? = nil) {
        self.id = id
        self.name = name
        self.country = country
        self.logo = logo
        self.flag = flag
        self.season = season
        self.round = round
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        country = (try? container.decodeIfPresent(String.self, forKey: .country))
        logo = (try? container.decodeIfPresent(String.self, forKey: .logo))
        flag = (try? container.decodeIfPresent(String.self, forKey: .flag))
        season = (try? container.decodeIfPresent(Int.self, forKey: .season))
        round = (try? container.decodeIfPresent(String.self, forKey: .round))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case country
        case logo
        case flag
        case season
        case round
    }
    
}


public struct Teams: Codable {
    
    var home: Team?
    var away: Team?
    
    init(home: Team? = nil, away: Team? = nil) {
        self.home = home
        self.away = away
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        home = (try? container.decodeIfPresent(Team.self, forKey: .home))
        away = (try? container.decodeIfPresent(Team.self, forKey: .away))
    }
    
    enum CodingKeys: String, CodingKey {
        case home
        case away
    }
}


public struct Team: Codable, Equatable, Hashable {
    
    var id: Int?
    var name: String?
    var logo: String?
    var winner: Bool?
    
    init(id: Int? = nil, name: String? = nil, logo: String? = nil, winner: Bool? = nil) {
        self.id = id
        self.name = name
        self.logo = logo
        self.winner = winner
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(Int.self, forKey: .id))
        name = (try? container.decodeIfPresent(String.self, forKey: .name))
        logo = (try? container.decodeIfPresent(String.self, forKey: .logo))
        winner = (try? container.decodeIfPresent(Bool.self, forKey: .winner))
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case logo
        case winner
    }
    
    public static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.id == rhs.id &&
        lhs.name == rhs.name &&
        lhs.logo == rhs.logo &&
        lhs.winner == rhs.winner
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
        hasher.combine(logo)
        hasher.combine(winner)
    }
    
}


public struct Goals: Codable {
    
    var home: Int?
    var away: Int?
    
    init(home: Int? = nil, away: Int? = nil) {
        self.home = home
        self.away = away
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        home = (try? container.decodeIfPresent(Int.self, forKey: .home))
        away = (try? container.decodeIfPresent(Int.self, forKey: .away))
    }
    
    enum CodingKeys: String, CodingKey {
        case home
        case away
    }
    
}


public struct Score: Codable {
    
    var halftime: Goals?
    var fulltime: Goals?
    var extratime: Goals?
    var penalty: Goals?
    
    init(halftime: Goals? = nil, fulltime: Goals? = nil, extratime: Goals? = nil, penalty: Goals? = nil) {
        self.halftime = halftime
        self.fulltime = fulltime
        self.extratime = extratime
        self.penalty = penalty
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        halftime = (try? container.decodeIfPresent(Goals.self, forKey: .halftime))
        fulltime = (try? container.decodeIfPresent(Goals.self, forKey: .fulltime))
        extratime = (try? container.decodeIfPresent(Goals.self, forKey: .extratime))
        penalty = (try? container.decodeIfPresent(Goals.self, forKey: .penalty))
    }
    
    enum CodingKeys: String, CodingKey {
        case halftime
        case fulltime
        case extratime
        case penalty
    }
    
}


public struct MatchDayStruct: Identifiable {
    
    public var id: UUID?
    public var sectionTitle: String?
    public var mathDay: [MatchDay]?
    
    internal init(sectionTitle: String? = nil, mathDay: [MatchDay]? = nil) {
        self.id = UUID()
        self.sectionTitle = sectionTitle
        self.mathDay = mathDay
    }
    
}
