//
//  BaseResponse.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 19/11/21.
//

import Foundation

public class BaseResponse: Codable {
    
    var get: String?
    var parameters: [Parameters]?
    var errors: [Errors]?
    var results: Int?
    var paging: Paging?
    
    public init(get: String? = nil, parameters: [Parameters]? = nil, errors: [Errors]? = nil, results: Int? = nil, paging: Paging? = nil) {
        self.get = get
        self.parameters = parameters
        self.errors = errors
        self.results = results
        self.paging = paging
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        get = (try? container.decodeIfPresent(String.self, forKey: .get))
        parameters = (try? container.decodeIfPresent([Parameters].self, forKey: .parameters))
        errors = (try? container.decodeIfPresent([Errors].self, forKey: .errors))
        results = (try? container.decodeIfPresent(Int.self, forKey: .results))
        paging = (try? container.decodeIfPresent(Paging.self, forKey: .paging))
    }
    
    enum CodingKeys: String, CodingKey {
        case get
        case parameters
        case errors
        case results
        case paging
    }
    
}


public struct Parameters: Codable {

    var id: String?
    var league: String?
    var current: Bool?
    var season: String?
    
    init(id: String? = nil, league: String? = nil, current: Bool? = nil, season: String? = nil) {
        self.id = id
        self.league = league
        self.current = current
        self.season = season
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decodeIfPresent(String.self, forKey: .id))
        league = (try? container.decodeIfPresent(String.self, forKey: .league))
        current = (try? container.decodeIfPresent(Bool.self, forKey: .current))
        season = (try? container.decodeIfPresent(String.self, forKey: .season))
    }
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case league
        case current
        case season
    }
    
}

public struct Errors: Codable {
    
    var time: String?
    var bug: String?
    var report: String?
    
    public init(time: String? = nil, bug: String? = nil, report: String? = nil) {
        self.time = time
        self.bug = bug
        self.report = report
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        time = (try? container.decodeIfPresent(String.self, forKey: .time))
        bug = (try? container.decodeIfPresent(String.self, forKey: .bug))
        report = (try? container.decodeIfPresent(String.self, forKey: .report))
    }
    
    enum CodingKeys: String, CodingKey {
        case time
        case bug
        case report
    }
    
}


public struct Paging: Codable {

    var current: Int?
    var total: Int?
    
    public init(current: Int? = nil, total: Int? = nil) {
        self.current = current
        self.total = total
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        current = (try? container.decodeIfPresent(Int.self, forKey: .current))
        total = (try? container.decodeIfPresent(Int.self, forKey: .total))
    }
    
    enum CodingKeys: String, CodingKey {
        case current
        case total
    }
    
}
