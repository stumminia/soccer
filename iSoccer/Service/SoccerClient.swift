//
//  SoccerClient.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/11/21.
//

import Foundation
import Combine
import Moya
import CombineMoya

public typealias SoccerClient = ServiceClient<SoccerService>

extension SoccerClient where ServiceDefinition == SoccerService {
    
    public func getAllLeagues(auth: ApiAuth) -> AnyPublisher<LeagueResponse,AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .allLeagues(auth: auth), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(LeagueResponse.self)
            .mapError { AppError.mapAppError(error: $0) }
            .eraseToAnyPublisher()
    }
    
    public func getAllCountries(auth: ApiAuth) -> AnyPublisher<CountriesResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .allCountries(auth: auth), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(CountriesResponse.self)
            .mapError { AppError.mapAppError(error: $0) }
            .eraseToAnyPublisher()
    }
    
    public func getAllFixtureByTeam(auth: ApiAuth, teamID: Int, season: Int) -> AnyPublisher<FixtureResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getAllFixtureByTeam(auth: auth, season: season, teamID: teamID), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(FixtureResponse.self)
            .mapError { AppError.mapAppError(error: $0) }
            .eraseToAnyPublisher()
    }
    
    public func getCurrentFixture(auth: ApiAuth, league: Int, season: Int, current: Bool = true) -> AnyPublisher<CurrentFeatureResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .currentFixture(auth: auth, league: league, season: season, current: current), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(CurrentFeatureResponse.self)
            .mapError {AppError.mapAppError(error: $0) }
            .eraseToAnyPublisher()
        }
    
    public func getFixtureByRound(auth: ApiAuth, league: Int, season: Int, round: String?) -> AnyPublisher<CurrentFixtureByRoundResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getFixtureByRound(auth: auth, league: league, season: season, round: round), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(CurrentFixtureByRoundResponse.self)
            .mapError {AppError.mapAppError(error: $0) }
            .eraseToAnyPublisher()
    }
    
    public func getStandings(auth: ApiAuth, league: Int, season: Int) -> AnyPublisher<StandingsResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getStandings(auth: auth, league: league, season: season), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(StandingsResponse.self)
            .mapError {AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    
    public func getEventsMatch(auth: ApiAuth, fixtureID: Int) -> AnyPublisher<EventsResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getEvent(auth: auth, fixture: fixtureID), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(EventsResponse.self)
            .mapError {AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getLineups(auth: ApiAuth, fixture: Int) -> AnyPublisher<LineupsResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getLineups(auth: auth, fixture: fixture), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(LineupsResponse.self)
            .mapError {AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getTopScores(auth: ApiAuth, league: Int, season: Int) -> AnyPublisher<TopScoresResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getTopScores(auth: auth, league: league, season: season), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(TopScoresResponse.self)
            .mapError {AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getTopAssist(auth: ApiAuth, league: Int, season: Int) -> AnyPublisher<TopScoresResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getTopAssist(auth: auth, league: league, season: season), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(TopScoresResponse.self)
            .mapError {AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getTopRedCard(auth: ApiAuth, league: Int, season: Int) -> AnyPublisher<TopScoresResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getTopRedCards(auth: auth, league: league, season: season), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(TopScoresResponse.self)
            .mapError {AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getTopYellowCard(auth: ApiAuth, league: Int, season: Int) -> AnyPublisher<TopScoresResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getTopYellowCards(auth: auth, league: league, season: season), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(TopScoresResponse.self)
            .mapError {AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getStatistics(auth: ApiAuth, fixture: Int) -> AnyPublisher<StatisticsResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getStatistics(auth: auth, fixture: fixture), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(StatisticsResponse.self)
            .mapError {AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getPlayersTeam(auth: ApiAuth, teamID: Int) -> AnyPublisher<PlayersTeamResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getPlayersTeam(auth: auth, teamID: teamID), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(PlayersTeamResponse.self)
            .mapError{AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
        
    }
    
    public func getTeamInformation(auth: ApiAuth, teamID: Int) -> AnyPublisher<TeamInformationResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getTeamInformation(auth: auth, teamID: teamID), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(TeamInformationResponse.self)
            .mapError{AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
        
    }
    
    public func getTeamStatistics(auth: ApiAuth, league: Int, season: Int, teamID: Int) -> AnyPublisher<TeamStatisticsResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getTeamStatistics(auth: auth, league: league, season: season, teamID: teamID), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(TeamStatisticsResponse.self)
            .mapError{AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
        
    }
    
    public func getTeamSeasons(auth: ApiAuth, teamID: Int) -> AnyPublisher<TeamSeasonsResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getTeamSeasons(auth: auth, teamID: teamID), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(TeamSeasonsResponse.self)
            .mapError{AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getSeasonPlayer(auth: ApiAuth, playerId: Int) -> AnyPublisher<SeasonPlayerResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getSeasonPlayer(auth: auth, playerId: playerId), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(SeasonPlayerResponse.self)
            .mapError{AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
        
    }
    
    public func getPlayerStatistics(auth: ApiAuth, playerId: Int, season: Int) -> AnyPublisher<PlayerStatisticsResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getPlayerStatistics(auth: auth, playerId: playerId, season: season), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(PlayerStatisticsResponse.self)
            .mapError{AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getPlayerTransfers(auth: ApiAuth, playerId: Int) -> AnyPublisher<PlayerTranfersResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getPlayerTransfers(auth: auth, playerId: playerId), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(PlayerTranfersResponse.self)
            .mapError{AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
    public func getPlayerTranspher(auth: ApiAuth, playerId: Int) -> AnyPublisher<PlayerTrophiesResponse, AppError> {
        
        return provider.requestPublisher(SoccerService(serviceRequest: .getPlayerTrophies(auth: auth, playerId: playerId), baseURL: baseURL), callbackQueue: DispatchQueue.global(qos: .background))
            .filterSuccessfulStatusCodes()
            .map(PlayerTrophiesResponse.self)
            .mapError{ AppError.mapAppError(error: $0)}
            .eraseToAnyPublisher()
    }
    
}
