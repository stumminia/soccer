//
//  SessionService.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 04/11/21.
//

import Foundation
import Combine
import Alamofire
import AlamofireNetworkActivityLogger


public final class SessionService: APIHandler, ObservableObject{
    
    var subscriptions = Set<AnyCancellable>()
    public let loginError = PassthroughSubject<String, Never>()
        
    // Network client
    private var soccerClient: SoccerClient
    private var auth: ApiAuth?
    
    
    internal init(soccerClient: SoccerClient) {
        
        self.soccerClient = soccerClient
        
        self.auth = ApiAuth()
        
        #if DEBUG
            NetworkActivityLogger.shared.startLogging()
            NetworkActivityLogger.shared.level = .debug
        #endif
    }
    
    
}
