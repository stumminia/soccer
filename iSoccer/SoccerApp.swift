//
//  SoccerApp.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/11/21.
//

import SwiftUI

@main
struct SoccerApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @Environment(\.scenePhase) var scenePhase
    
    @StateObject var firestoreManager = FirestoreManager()
        
    var body: some Scene {
        WindowGroup {
            RootView()
                .environmentObject(firestoreManager)
        }
    }
}
