//
//  NavigationBar.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 07/04/22.
//

import SwiftUI

struct NavigationBar: View {
    
    var title: String = ""
    @Binding var hasScrolled: Bool
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        ZStack {
            Color.clear
                .background(.ultraThinMaterial)
                .blur(radius: 10)
                .opacity(hasScrolled ? 1 : 0)
            
            HStack(spacing: 4) {
                
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image(systemName: "chevron.backward.circle.fill")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .cornerRadius(10)
                        .padding(8)
                        .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 18, style: .continuous))
                        
                        .strokeStyle(cornerRadius: 18)
                }
                .padding(.top, 20)
                
                Text(title)
                    .font(.largeTitle.weight(.bold))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading, 20)
                    .padding(.top, 20)
                    .offset(y: 0)
                
            }
            .padding(.horizontal, 12)
            
            
        }
        .frame(height: 70)
        .frame(maxHeight: .infinity, alignment: .top)
    }
}

struct NavigationBar_Previews: PreviewProvider {
    static var previews: some View {
        NavigationBar(title: "Team information", hasScrolled: .constant(false))
    }
}
