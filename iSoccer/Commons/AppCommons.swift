//
//  AppCommons.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 04/11/21.
//

import Foundation
import SVGKit

public enum StatusMatch: String, Codable, Equatable, CodingKey {
    
    case TBD = "TBD"
    case NS = "NS"
    case _1H = "H1"
    case HT = "HT"
    case _2H = "H2"
    case ET = "ET"
    case P = "P"
    case FT = "FT"
    case AET = "AET"
    case PEN = "PEN"
    case BT = "BT"
    case SUSP = "SUSP"
    case INT = "INT"
    case PST = "PST"
    case CANC = "CANC"
    case ABD = "ABD"
    case AWD = "AWD"
    case WO = "WO"
    case LIVE = "LIVE"
    
    func nameForKind() -> String {
        return self.rawValue
    }
    
    
}

public class AppCommons {
    
    static public func readLocalFile(forName name: String, type: String = "json") -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: type),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
    
}

extension AppCommons {
    
    private func downloadImage(from url: URL, completion: @escaping (Data?) -> ()) {
        //print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
//            print(response?.suggestedFilename ?? url.lastPathComponent)
            //print("Download Finished")
            completion(data)
        }
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    public func getImageSVG(_ flagURL: String?, completion: @escaping (UIImage?) -> ()) {
        guard let flag = flagURL, let url = URL(string:  flag) else { return }
        
        downloadImage(from: url) { data in
            if let dataImage = data {
                let svgImage = SVGKImage.init(data: dataImage)
                let image = svgImage?.uiImage.scale(with: CGSize(width: CGFloat(15), height: CGFloat(15)))
                completion(image)
            }
        }
    }
    
    func getImage(_ logoURL: String?, size: CGSize = CGSize(width: 20, height: 30), completion: @escaping (UIImage?) -> ()) {
        guard let logo = logoURL, let leagueURL = URL(string:  logo) else { return }
        
        downloadImage(from: leagueURL) { data in
            if let dataImage = data {
                let image = UIImage(data: dataImage)?.scale(with: size)
                completion(image)
            }
        }
    }
    
}
