//
//  AppContext.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 04/11/21.
//

import Foundation
import Moya
import Combine
import Swinject

public final class AppContext {
    static let shared = AppContext()
    
    var sessionService: SessionService
    var soccerClient: SoccerClient
    
    private var subscriptions = Set<AnyCancellable>()
    
    // dependency injection container
    let container: Container
    
    init() {
        self.container = AppContext.setupContainer()
        self.soccerClient = container.resolve(SoccerClient.self)!
        self.sessionService = container.resolve(SessionService.self)!
    }
}

extension AppContext {
    
    static func setupContainer() -> Container {
        
        let container = Container()
        
        let apiUrl: String
        let jsonData = AppCommons.readLocalFile(forName: "Config")!
        do {
            // make sure this JSON is in the format we expect
            let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: String]
            apiUrl = json["base_url"]!
        } catch {
            fatalError("Failed to load Config file: \(error)")
        }
        
        
        container.register(SoccerClient.self) { (_) in
            let apiUrl = URL(string: apiUrl)!
            return SoccerClient(baseURL: apiUrl)
        }.inObjectScope(.container)
        
        // Session Manager
        container.register(SessionService.self) { (r) in
            let client = r.resolve(SoccerClient.self)!
            return SessionService(soccerClient: client)
        }.inObjectScope(.container)
        
        return container
        
    }
    
}
