//
//  TabBar.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 01/03/22.
//

import SwiftUI

struct TabBar: View {
    
    @AppStorage("selectedTab") var selectedTab: Tab = .leagues
    @Environment(\.colorScheme) var colorScheme
    @EnvironmentObject var firestoreManager: FirestoreManager
    @State var color: Color
    
    
    var body: some View {
        ZStack(alignment: .bottom) {
            
            Group {
                switch selectedTab {
                case .leagues:
                    LeaguesView(LeaguesViewModel(firestoreManager: firestoreManager))
                case .today:
                    TodayView()
                case .home:
                    HomeView()
                case .live:
                    LiveView()
                case .more:
                    MoreView()
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            
            HStack {
                ForEach(tabItems) { item in
                    Button {
                        withAnimation(.spring(response: 0.3,
                                              dampingFraction: 0.7)) {
                            selectedTab = item.tab
                            color = colorScheme == .dark ? item.colorDark : item.colorLight
                        }
                    } label: {
                        VStack(spacing: 0) {
                            Image(systemName: item.icon)
                                .symbolVariant(.fill)
                                .font(.body.bold())
                                .frame(width: 44, height: 29)
                            Text(item.text)
                                .font(.caption2.bold())
                                .lineLimit(1)
                            
                        }
                        .frame(maxWidth: .infinity)
                    }
                    .foregroundStyle(selectedTab == item.tab ? .primary : .secondary)
                    .blendMode(selectedTab == item.tab ? .overlay : .normal)
                }
            }
            .padding(.horizontal, 12)
            .padding(.top, 14)
            .frame(height: 78, alignment: .top)
            .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 8, style: .continuous))
            .background(
                backGroundTabBar
            )
            .overlay(
                overlayBGTabBar
            )
            .strokeStyle(cornerRadius: 8)
            .frame(maxHeight: .infinity, alignment: .bottom)
            .ignoresSafeArea()
            //.padding(.horizontal, 8)
        }
    }
    
    var backGroundTabBar: some View {
        HStack {
            if selectedTab == .more { Spacer() }
            if selectedTab == .live {
                Spacer()
                Spacer()
                Spacer()
            }
            if selectedTab == .home {
                Spacer()
                Spacer()
            }
            if selectedTab == .today { Spacer() }
            
            Circle().fill(color).frame(width: 88)
            
            if selectedTab == .leagues { Spacer() }
            if selectedTab == .live { Spacer() }
            if selectedTab == .home {
                Spacer()
                Spacer()
            }
            if selectedTab == .today {
                Spacer()
                Spacer()
                Spacer()
            }
        }
        .padding(.horizontal, 8)
    }
    
    var overlayBGTabBar: some View {
        HStack {
            if selectedTab == .more { Spacer() }
            if selectedTab == .live {
                Spacer()
                Spacer()
                Spacer()
            }
            if selectedTab == .home {
                Spacer()
                Spacer()
            }
            if selectedTab == .today { Spacer() }
            
            Rectangle()
                .fill(color)
                .frame(width: 28, height: 5)
                .cornerRadius(3)
                .frame(width: 88)
                .frame(maxHeight: .infinity, alignment: .top)
            
            if selectedTab == .leagues { Spacer() }
            if selectedTab == .live { Spacer() }
            if selectedTab == .home {
                Spacer()
                Spacer()
            }
            if selectedTab == .today {
                Spacer()
                Spacer()
                Spacer()
            }
        }
        .padding(.horizontal, 0)
    }
    
}

struct TabBar_Previews: PreviewProvider {
    static var previews: some View {
        TabBar(color: .blue)
        TabBar(color: .teal).preferredColorScheme(.dark)
    }
}
