//
//  Background.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/02/22.
//

import SwiftUI

struct Background: View {
    var body: some View {
        
        ZStack {
            Rectangle()
                .border(.white, width: 4.0)
                .foregroundColor(.clear)
                .background(Color.init(uiColor: UIColor(red: 0.1, green: 0.6, blue: 0.4, alpha: 1)))
            
            Rectangle()
                .fill(.white)
                .frame(width: UIScreen.main.bounds.width, height: 4, alignment: .center)
            
            Circle()
                .stroke(.white, lineWidth: 4)
                .padding(.horizontal, 100)
            
            Circle()
                .fill(.white)
                .frame(width: 25, height: 25, alignment: .center)
                
            
        }
    }
}

struct StrokeStyle: ViewModifier {
    var cornerRadius: CGFloat
    @Environment(\.colorScheme) var colorScheme
    func body(content: Content) -> some View {
        content.overlay(
            RoundedRectangle(cornerRadius: cornerRadius, style: .continuous)
                .stroke(
                    .linearGradient(
                        colors: [
                            .white.opacity(colorScheme == .dark ? 0.6 : 0.3),
                            .black.opacity(colorScheme == .dark ? 0.3 : 0.1)
                        ], startPoint: .top, endPoint: .bottom
                    )
                )
                .blendMode(.overlay)
        )
    }
}

extension View {
    func strokeStyle(cornerRadius: CGFloat = 30) -> some View {
        modifier(StrokeStyle(cornerRadius: cornerRadius))
    }
}


struct Background_Previews: PreviewProvider {
    
    static var previews: some View {
        Background()
    }
    
}
