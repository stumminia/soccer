//
//  Formatter.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 09/12/21.
//

import Foundation
import SwiftUI


struct Formatter {
    
    static func formatDate(_ timestamp: Double, dateStyle:DateFormatter.Style, timeStyle:DateFormatter.Style) -> String {
        
        let dateToFormat: Date = self.formatDate(timestamp)
        
        let formatter = DateFormatter()
        formatter.locale = NSLocale.current
        formatter.dateStyle = dateStyle
        formatter.timeStyle = timeStyle
        
        return formatter.string(from: dateToFormat)
    }
    
    static func formatDate(_ timestamp: Double) -> Date {
        
        let numString = String(timestamp)
        let dateToFormat:Date
        if (numString.count>12){
            dateToFormat = Date(timeIntervalSince1970: timestamp/1000)
        }else{
            dateToFormat = Date(timeIntervalSince1970: timestamp)
        }
        
        return dateToFormat
        
    }
    
}
