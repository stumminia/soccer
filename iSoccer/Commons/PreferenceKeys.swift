//
//  PreferenceKeys.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 07/04/22.
//

import SwiftUI

struct ScrollPreferenceKey: PreferenceKey {
    static var defaultValue: CGFloat = 0
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}
