//
//  DateExtension.swift
//  ConectusCore
//
//  Created by Salvatore Tumminia on 14/04/21.
//

import Foundation

import Foundation

public extension Date {
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    var millisecondsSince1970Int64:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
    
    init(millisecond:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(millisecond) / 1000)
    }
    
    var dotsFormat: String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.YYYY"
        return formatter.string(from: self)
    }
    
    private func getFormatterForDate() -> DateFormatter {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
        
    }
    
    /*
     *  Ritorna il primo giorno del quarter in corso
     */
    var startOfQuarter: String {
        var components = Calendar.current.dateComponents([.month, .day, .year], from: self)
        
        let newMonth: Int
        switch components.month! {
        case 1,2,3: newMonth = 1
        case 4,5,6: newMonth = 4
        case 7,8,9: newMonth = 7
        case 10,11,12: newMonth = 10
        default: newMonth = 1
        }
        components.month = newMonth
        components.day = 1
        
        
        let date = Calendar.current.date(from: components)!
        return getFormatterForDate().string(from: date)
    }
    
    /*
     *  Ritorna l'ultimo giorno del quarter in corso
     */
    var endOfQuarter: String {
        var components = Calendar.current.dateComponents([.month, .day, .year], from: self)
        
        let newMonth: Int
        switch components.month! {
        case 1,2,3: newMonth = 3
        case 4,5,6: newMonth = 6
        case 7,8,9: newMonth = 9
        case 10,11,12: newMonth = 12
        default: newMonth = 3
        }
        components.month = newMonth
        components.day = 1
        
        let date = Calendar.current.date(from: components)!
        var comps2 = DateComponents()
        comps2.month = 1
        comps2.day = -1
        let endOfMonth = Calendar.current.date(byAdding: comps2, to: date)
        return getFormatterForDate().string(from: endOfMonth!)
    }
    
    /*
     *  Ritorna il primo giorno del mese in corso
     */
    var startOfMonth: String {
        var components = Calendar.current.dateComponents([.month, .day, .year], from: self)
        
        components.day = 1
        
        let date = Calendar.current.date(from: components)!
        return getFormatterForDate().string(from: date)
    }
    
    
    /*
     *  Ritorna l'ultimo giorno del mese in corso
     */
    var endOfMonth: String {
        var components = Calendar.current.dateComponents([.month, .day, .year], from: self)
        
        components.day = 1
        
        let date = Calendar.current.date(from: components)!
        var comps2 = DateComponents()
        comps2.month = 1
        comps2.day = -1
        let endOfMonth = Calendar.current.date(byAdding: comps2, to: date)!
        return getFormatterForDate().string(from: endOfMonth)
    }
    
    
    /*
     *  Ritorna il primo giorno dell'anno in corso
     */
    var firstOfYear: String {
        var components = Calendar.current.dateComponents([.month, .day, .year], from: self)
        
        components.day = 1
        components.month = 1
        
        let date = Calendar.current.date(from: components)!
        return getFormatterForDate().string(from: date)
    }
    
    
    /*
     *  Ritorna l'ultimo giorno dell'anno in corso
     */
    var lastOfYear: String {
        var components = Calendar.current.dateComponents([.month, .day, .year], from: self)
        components.month = 12
        components.day = 31
        components.hour = 00
        
        let lastOfYear = Calendar.current.date(from: components)!
        
        return getFormatterForDate().string(from: lastOfYear)
    }
    
    
    /*
     *  Ritorna il primo giorno dell semestre
     */
    var firstOfSemester: String {
        var components = Calendar.current.dateComponents([.month, .day, .year], from: self)
        
        let newMonth: Int
        switch components.month! {
        case 1,2,3,4,5,6: newMonth = 1
        case 7,8,9,10,11,12: newMonth = 7
        default: newMonth = 1
        }
        components.month = newMonth
        components.day = 1
        
        
        let date = Calendar.current.date(from: components)!
        return getFormatterForDate().string(from: date)
    }
    
    /*
     *  Ritorna l'ultimo giorno dell semestre
     */
    var lastOfSemester: String {
        var components = Calendar.current.dateComponents([.month, .day, .year], from: self)
        
        let newMonth: Int
        switch components.month! {
        case 1,2,3,4,5,6: newMonth = 6
        case 7,8,9,10,11,12: newMonth = 12
        default: newMonth = 1
        }
        components.month = newMonth
        components.day = 1
        
        let date = Calendar.current.date(from: components)!
        return getFormatterForDate().string(from: date)
    }
    
}

public extension Date {
    
    func localDate() -> Date {
        let nowUTC = Date()
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: nowUTC))
        guard let localDate = Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: nowUTC) else {return Date()}
        
        return localDate
    }
    
    func format(customFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = customFormat
        
        return formatter.string(from: self)
    }
    
    func isToday() -> Bool {
        
        let format: String = NSLocalizedString("date.formatter", comment: "")
        let today = Date.init().format(customFormat: format)
        let date = self.format(customFormat: format)
        
        return today == date
        
    }
}
