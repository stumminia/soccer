//
//  String-Extension.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 10/11/21.
//

import Foundation

extension String {
    
    func mainLocalized(_ args:CVarArg...) -> String {
        
        let localizeString = String(format: NSLocalizedString(self, bundle: Bundle.main, comment: ""), arguments: args)
        
        return localizeString
    }
    
}
