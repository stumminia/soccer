//
//  TabRouter.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 10/11/21.
//

import SwiftUI

public class TabRouter: ObservableObject {
    
    var isNetworkDown = false
    
    @Published public var selectedTab = 0 
    
    
    
}

struct TabItem: Identifiable {
    
    var id = UUID()
    var text: String
    var icon: String
    var tab: Tab
    var colorDark: Color
    var colorLight: Color
    
}

var tabItems = [
    TabItem(text: "LEAGUES_NAME".mainLocalized(), icon: "sportscourt", tab: .leagues, colorDark: .teal, colorLight: .blue),
    TabItem(text: "TODAY_NAME".mainLocalized(), icon: "calendar", tab: .today, colorDark: .teal, colorLight: .blue),
    TabItem(text: "HOME_NAME".mainLocalized(), icon: "house", tab: .home, colorDark: .teal, colorLight: .blue),
    TabItem(text: "LIVE_NAME".mainLocalized(), icon: "doc.plaintext", tab: .live, colorDark: .teal, colorLight: .blue),
    TabItem(text: "MORE_NAME".mainLocalized(), icon: "ellipsis", tab: .more, colorDark: .teal, colorLight: .blue)
]

public enum Tab: Int {
    case leagues = 0
    case today = 1
    case home = 2
    case live = 3
    case more = 4
}
