//
//  AppDelegate.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 05/11/21.
//

import Foundation
import Firebase
import FirebaseMessaging
import FirebaseCrashlytics
import UIKit
import CoreData

class AppDelegate: NSObject {
    
    var notificationCenter = UNUserNotificationCenter.current()
    public var coreDataContext: NSManagedObjectContext!
}

extension AppDelegate: UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        self.registerForPushNotifications()
        
        FirebaseApp.configure()
        Crashlytics.crashlytics()
                
        Messaging.messaging().delegate = self
        coreDataContext = PersistenceController.shared.container.viewContext
        
        return true
    }
    
}

extension AppDelegate {
    func registerForPushNotifications() {
        
        notificationCenter.delegate = self
        
        let autOption: UNAuthorizationOptions =  [.alert, .sound, .badge]
        
        notificationCenter.requestAuthorization(options: autOption) { // 2
                [weak self]  granted, error in

                guard granted else { return }
                self?.getNotificationSettings()
        }
    }
    
    /*
     * Abilito la parte di settings delle notifiche.
     */
    func getNotificationSettings() {
        notificationCenter.getNotificationSettings { settings in
            
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            
        }
    }
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")

          let dataDict: [String: String] = ["token": fcmToken ?? ""]
          NotificationCenter.default.post(
            name: Notification.Name("FCMToken"),
            object: nil,
            userInfo: dataDict
          )
    }
    
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokens = deviceToken.map{ data in String(format: "%02.2hhx", data) }
        let token = tokens.joined()
        print("token: \(token)")
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error registered notification")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("ARRIVO'!!")
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("ARRIVO'!!")
        
    }
    
    
}
