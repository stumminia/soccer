//
//  FirestoreManager.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 03/03/22.
//

import Foundation
import Firebase

class FirestoreManager: ObservableObject {
    
    var db = Firestore.firestore()
    var selectedDay: String = ""
    
    func changePreferredState(leagueID: Int?) {
        
        if let leagueID = leagueID {
            
            let docRef = db.collection("soccerData").document("LeagueSelected")
            
            docRef.getDocument() { (document, error) in
                guard let document = document, document.exists else {
                    print("Document does not exist")
                    return
                }
                
                var savedLeague: [Int] = (document["savedIDLeague"] as? [Int]) ?? []
                if savedLeague.contains(leagueID) {
                    savedLeague.removeAll(where: { $0 == leagueID })
                }
                else {
                    savedLeague.append(leagueID)
                }
                
                self.updateLeagueSelected(savedLeague: savedLeague)
            }
        }
    }
    
    func updateLeagueSelected(savedLeague: [Int]) {
        
        let docRef = db.collection("soccerData").document("LeagueSelected")
        
        docRef.updateData(["savedIDLeague" : savedLeague]) { error in
            if let error = error {
                print("Error updating document: \(error)")
            } else {
                print("Document successfully updated!")
            }
        }
    }
    
    func isPreferred(leagues: [Leagues], _ completion: @escaping (_ data: [Leagues]) -> Void) {
        
        let docRef = db.collection("soccerData").document("LeagueSelected")
        
        var tempLeagues = leagues
        
        docRef.getDocument() { (document, error) in
            guard let document = document, document.exists else {
                print("Document does not exist")
                return
            }
            
            let savedLeague: [Int] = (document["savedIDLeague"] as? [Int]) ?? []
            
            for i in 0..<tempLeagues.count {
                let league = tempLeagues[i]
                if savedLeague.contains(league.league?.id ?? 0) {
                    tempLeagues[i].preferred = true
                }
            }
            completion(tempLeagues)
        }
    }
    
    func getPrincipalLeagues(_ completion: @escaping ([String]) -> Void) {
        
        let docRef = db.collection("soccerData").document("principalLeagues")
        
        docRef.getDocument() { (document, error) in
            guard let document = document, document.exists else {
                print("Document does not exist")
                return
            }
            
            let principalLeagues = (document["leagues"] as? [String]) ?? []
            completion(principalLeagues)
        }
    }
    
    func isDaySelected(actualDay: String, _ completion: @escaping (Bool) -> Void) {
        self.getMatchDay() { day in
            completion(actualDay == day)
        }
    }
    
    func setMatchDaySelected(matchDay: String, idMatchDay: Int) {
        
        self.selectedDay = matchDay
        let docRef = db.collection("soccerData").document("UtilsSaved")
        
        docRef.updateData(["selectedMatchDay": matchDay, "id" : idMatchDay]){ error in
            if let error = error {
                print("Error updating document: \(error)")
            } else {
                print("Document successfully updated!")
            }
        }
        
    }
    
    func getMatchDay(_ completion: @escaping (String) -> Void) {
        
        let docRef = db.collection("soccerData").document("UtilsSaved")
        
        docRef.getDocument() { (document, error) in
            guard let document = document, document.exists else {
                print("Document does not exist")
                return
            }
            
            let selectedMatch = document["selectedMatchDay"] as! String
            
            completion(selectedMatch)
        }
        
    }
    
    func getMatchDayId(_ completion: @escaping (Int) -> Void) {
        
        let docRef = db.collection("soccerData").document("UtilsSaved")
        
        docRef.getDocument() { (document, error) in
            guard let document = document, document.exists else {
                print("Document does not exist")
                return
            }
            
            let selectetMatchId = document["id"] as! Int
            
            completion(selectetMatchId)
            
        }
        
    }
    
}
