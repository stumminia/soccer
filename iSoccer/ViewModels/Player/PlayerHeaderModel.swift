//
//  PlayerHeaderModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 07/09/22.
//

import Foundation
import SwiftUI

public class PlayerHeaderModel: ObservableObject{

    var player: PlayerStatistics?
    var team: Team?
    
    public init(player: PlayerStatistics? = nil, team: Team? = nil) {
        self.player = player
        self.team = team
    }
    
    
    
}
