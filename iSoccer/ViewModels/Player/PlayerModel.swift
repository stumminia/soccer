//
//  PlayerModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 06/09/22.
//

import Foundation
import SwiftUI
import Combine

class PlayerModel: ObservableObject {
    
    var playerId: Int?
    var seasonsPlayer: [Int]?
    var statistics: [ResponsePlayer]?
    var transfers: PlayerTransfers?
    var trophies: [PlayerTrophies]?
    
    var cancellables: Set<AnyCancellable> = []
    
    init(playerId: Int? = nil) {
        self.playerId = playerId
        
        self.getPlayerSeason()
        self.getPlayerTranfers()
        self.getPlayerTrophies()
    }
    
    private func getPlayerSeason() {
        
        AppContext.shared.soccerClient.getSeasonPlayer(auth: ApiAuth(), playerId: self.playerId ?? 0)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            } receiveValue: { [weak self] (seasons) in
                guard let weakSelf = self else { fatalError("error referencing self") }
                weakSelf.seasonsPlayer = seasons.response
                weakSelf.getPlayerStatistics()
            }
            .store(in: &cancellables)
    }
    
    private func getPlayerStatistics() {
        if(seasonsPlayer != nil && seasonsPlayer!.count > 0) {
            for seas in seasonsPlayer! {
                AppContext.shared.soccerClient.getPlayerStatistics(auth: ApiAuth(), playerId: self.playerId ?? 0, season: seas)
                    .receive(on: DispatchQueue.main)
                    .sink { [weak self] (completion) in
                        guard self != nil else { fatalError("error referencing self") }
                        
                        switch completion {
                        case .failure(let error):
                            print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                        case .finished: print("finished")
                        }
                    } receiveValue: { [weak self] (stats) in
                        guard let weakSelf = self else { fatalError("error referencing self") }
                        weakSelf.statistics?.append(stats.response!)
                    }
                    .store(in: &cancellables)
            }
        }
    }
    
    private func getPlayerTranfers() {
        
        AppContext.shared.soccerClient.getPlayerTransfers(auth: ApiAuth(), playerId: self.playerId ?? 0)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            }receiveValue: { [weak self] (transfer) in
                guard let weakSelf = self else { fatalError("error referencing self") }
                weakSelf.transfers = transfer.response
            }
            .store(in: &cancellables)
    }
    
    private func getPlayerTrophies() {
        
        AppContext.shared.soccerClient.getPlayerTranspher(auth: ApiAuth(), playerId: self.playerId ?? 0)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            }receiveValue: { [weak self] (trophies) in
                guard let weakSelf = self else { fatalError("error referencing self") }
                weakSelf.trophies = trophies.response
            }
            .store(in: &cancellables)
        
    }
    
}
