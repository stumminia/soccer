//
//  CountryRowModel.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 19/11/21.
//

import Foundation
import SwiftUI
import Combine
import SVGKit

class CountryRowModel: ObservableObject, Identifiable {
    
    var country: Country
    
    lazy var imageCountrySink = PassthroughSubject<UIImage?, Never>()
    var cancellables: Set<AnyCancellable> = []
    
    @Published var imageCountry: UIImage?
    
    init(country: Country) {
        self.country = country
        
        getImageCounty()
        
        setupBindings()
    }
    
    private func setupBindings() {
        
        self.imageCountrySink
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [weak self] (image) in
                self?.imageCountry = image
            })
            .store(in: &cancellables)
    }
        
    func downloadImage(from url: URL, completion: @escaping (Data?) -> ()) {
        //print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            //print(response?.suggestedFilename ?? url.lastPathComponent)
            //print("Download Finished")
            completion(data)
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func getImageCounty() {
        guard let flag = country.flag, let flagURL = URL(string:  flag) else { return }
        
        downloadImage(from: flagURL) { data in
            if let dataImage = data {
                let svgImage = SVGKImage.init(data: dataImage)
                let image = svgImage?.uiImage.scale(with: CGSize(width: CGFloat(25), height: CGFloat(25)))
                self.imageCountrySink.send(image)
            }
        }
    }
    
}
