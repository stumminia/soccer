//
//  TeamCalendarModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 03/08/22.
//

import Foundation
import SwiftUI
import Combine

class TeamCalendarModel: ObservableObject {
    
    var match: MatchStruct
    
    init(match: MatchStruct) {
        self.match = match
    }
    
    var matchDay: String {
        return Formatter.formatDate(Double(match.timestamp ?? 0), dateStyle: .medium, timeStyle: .none).replacingOccurrences(of: ",", with: "")
    }
    
    var matchDetail: String {
        if match.status == .NS || match.status == .TBD {
            let matchTime = Formatter.formatDate(Double(match.timestamp ?? 0), dateStyle: .none, timeStyle: .short).replacingOccurrences(of: ",", with: "")
            
            return match.status == .TBD ? matchTime + "*" : matchTime
        }
        
        return String(match.goalHome ?? 0) + " - " + String(match.goalAway ?? 0)
    }
    
}
