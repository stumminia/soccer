//
//  TeamsViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 25/03/22.
//

import Foundation
import SwiftUI
import Combine

class TeamsViewModel: ObservableObject {
    
    var teamID: Int
    var season: Int
    
    @Published var players: [PlayerTeam]?
    @Published var teamInformation: TeamInformation?
    @Published var seasonsTeam: [String]?
    @Published var logoImage: UIImage?
    @Published var matches: [TeamStructure]?
    var cancellables: Set<AnyCancellable> = []
    
    var venueAddress: String {
        
        (self.teamInformation?.venue?.address ?? "") + " - " + (self.teamInformation?.venue?.city ?? "")
        
    }
    
    init(teamID: Int, season: Int) {
        self.teamID = teamID
        self.season = season
        
        self.getTeamInformation()
        self.getPlayersSquads()
        self.getTeamSeasons()
        self.getTeamFixturesByTeam()
        
    }
    
    private func getTeamInformation() {
        
        AppContext.shared.soccerClient.getTeamInformation(auth: ApiAuth(), teamID: self.teamID)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            } receiveValue: { [weak self] (leagues) in
                guard let weakSelf = self else { fatalError("error referencing self") }
                weakSelf.teamInformation = leagues.response?.first
                
                AppCommons().getImage(weakSelf.teamInformation?.team?.logo, size: CGSize(width: 36.0, height: 36.0)) { image in
                    DispatchQueue.main.async {
                        weakSelf.logoImage = image
                    }
                }
            }
            .store(in: &cancellables)
        
    }
    
    
    private func getTeamFixturesByTeam() {
        
        AppContext.shared.soccerClient.getAllFixtureByTeam(auth: ApiAuth(), teamID: self.teamID, season: self.season)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            } receiveValue: { [weak self] (fixtures) in
                guard let weakSelf = self else { fatalError("error referencing self") }
                let sorted: [MatchDay] = (fixtures.response?.sorted {
                    $0.fixture?.timestamp ?? 0 < $1.fixture?.timestamp ?? 0
                })!
                
                var teamList: [TeamStructure] = [];
                
                for match in sorted {
                    let matchStruct = MatchStruct(logoHome: match.teams?.home?.logo, logoAway: match.teams?.away?.logo, teamHome: match.teams?.home?.name, teamAway: match.teams?.away?.name, status: match.fixture?.status?.short, timestamp: match.fixture?.timestamp, goalHome: match.goals?.home, goalAway: match.goals?.away, matchType: match.league?.id);
                    if teamList.count > 0 {
                        var teamStruct = teamList.last

                        if teamStruct?.matches?.first?.matchType == match.league?.id {
                            teamStruct?.matches?.append(matchStruct)
                            teamList[teamList.count - 1] = teamStruct!
                        }
                        else {
                            let matchesStruct: [MatchStruct] = [matchStruct]
                            let newTeamStruct = TeamStructure(sectionTitle: match.league?.name, matches: matchesStruct)
                            teamList.append(newTeamStruct)
                        }
                    }
                    else {
                        let matchesStruct: [MatchStruct] = [matchStruct]
                        let newTeamStruct = TeamStructure(sectionTitle: match.league?.name, matches: matchesStruct)
                        teamList.append(newTeamStruct)
                    }
                }
                
                weakSelf.matches = teamList
                
            }
            .store(in: &cancellables)
        
    }
    
    private func getPlayersSquads() {
        
        AppContext.shared.soccerClient.getPlayersTeam(auth: ApiAuth(), teamID: self.teamID)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            } receiveValue: { [weak self] (leagues) in
                guard let weakSelf = self else { fatalError("error referencing self") }
                weakSelf.players = leagues.response?.first?.players ?? []
            }
            .store(in: &cancellables)
    }
    
    private func getTeamSeasons() {
        
        AppContext.shared.soccerClient.getTeamSeasons(auth: ApiAuth(), teamID: self.teamID)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }
                    
                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (seasons) in
                guard let weakSelf = self else { fatalError("error referencing self") }
                weakSelf.seasonsTeam = seasons.response ?? []
            }
            .store(in: &cancellables)
    }

    
}
