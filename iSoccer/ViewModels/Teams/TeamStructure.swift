//
//  TeamStructure.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 02/09/22.
//

import Foundation
import Combine
import SwiftUI

struct TeamStructure: Identifiable, Hashable {
    
    public var id:UUID = UUID()
    var sectionTitle: String?
    var matches: [MatchStruct]?
    
    
    init(sectionTitle: String? = nil, matches: [MatchStruct]? = nil) {
        self.sectionTitle = sectionTitle
        self.matches = matches
    }
    
    static func == (lhs: TeamStructure, rhs: TeamStructure) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(sectionTitle)
        hasher.combine(matches)
    }
}

struct MatchStruct: Identifiable, Hashable {
    
    public var id:UUID = UUID()
    var logoHome: String?
    var logoAway: String?
    var teamHome: String?
    var teamAway: String?
    var status: StatusMatch?
    var timestamp: Int64?
    var goalHome: Int?
    var goalAway: Int?
    var matchType: Int?
    
    internal init(logoHome: String? = nil, logoAway: String? = nil, teamHome: String? = nil, teamAway: String? = nil, status: StatusMatch? = nil, timestamp: Int64? = nil, goalHome: Int? = nil, goalAway: Int? = nil, matchType: Int? = nil) {
        self.logoHome = logoHome
        self.logoAway = logoAway
        self.teamHome = teamHome
        self.teamAway = teamAway
        self.status = status
        self.timestamp = timestamp
        self.goalHome = goalHome
        self.goalAway = goalAway
        self.matchType = matchType
    }
    
    static func == (lhs: MatchStruct, rhs: MatchStruct) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(logoHome)
        hasher.combine(logoAway)
        hasher.combine(teamHome)
        hasher.combine(teamAway)
        hasher.combine(status)
        hasher.combine(timestamp)
        hasher.combine(goalHome)
        hasher.combine(goalAway)
        hasher.combine(matchType)
    }
}
