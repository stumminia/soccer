//
//  TeamPlayerModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 08/04/22.
//

import Foundation
import SwiftUI
import Combine

class TeamPlayerModel: ObservableObject {
    
    var player: PlayerTeam?
    @Published var playerImage: UIImage?
    
    var playerShow: String {
        return (String(self.player?.number ?? 0)) + " - " + (self.player?.name ?? "")
    }
    
    init(player: PlayerTeam) {
        self.player = player
        
        
        AppCommons().getImage(self.player?.photo, size: CGSize(width: 36.0, height: 36.0)) { image in
            DispatchQueue.main.async {
                self.playerImage = image
            }
        }
        
    }
    
    
}
