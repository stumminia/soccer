//
//  LineupsViewModel.swift
//  iSoccer
//
//  Created by Giuseppe Lisanti on 02/02/22.
//

import Foundation
import Combine
import SwiftUI

class LineupsViewModel: ObservableObject {
    
    var cancellables: Set<AnyCancellable> = [] // combine
    var fixture: Int?
    
    @Published var lineups: [Lineup]?

    init(fixture: Int? = nil) {
        self.fixture = fixture
        self.getLineups()
    }

    deinit {
        cancellables.removeAll()
        self.lineups?.removeAll()
    }
    
    func getLineups() {
        
        guard let fixture = self.fixture else { return }
        
        AppContext.shared.soccerClient.getLineups(auth: ApiAuth(), fixture: fixture)
        .receive(on: DispatchQueue.main)
        .sink { [weak self] (completion) in
            guard self != nil else { fatalError("error referencing self") }
                    
            switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
            }
        } receiveValue: { [weak self] (response) in
            guard let weakself = self else { fatalError("error referencing self")}
            weakself.lineups = response.response ?? []
        }
        .store(in: &cancellables)
    }
    
}
