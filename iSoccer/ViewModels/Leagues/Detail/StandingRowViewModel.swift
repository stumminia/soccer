//
//  StandingRowViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/12/21.
//

import Foundation
import Combine
import SwiftUI

enum StandingEnumPosition: String {
    case rank_1, rank_2, rank_3, rank_4  = "ChampionsColor"
    case rank_5, rank_6 = "EuropaLeagueColor"
    case rank_7 = "EuLeaguePrelColor"
    case rank_20, rank_19, rank_18 = "RetrocessioneColor"
    case rank_8, rank_9, rank_10, rank_11, rank_12, rank_13, rank_14, rank_15, rank_16, rank_17 = ""
    
    static func getColorString(rank: StandingEnumPosition) -> String {
        switch rank {
        case .rank_1, .rank_2, .rank_3, .rank_4:
            return "ChampionsColor"
        case .rank_5, .rank_6:
            return "EuropaLeagueColor"
        case .rank_7:
            return "EuLeaguePrelColor"
        case .rank_18, .rank_19, .rank_20:
            return "RetrocessioneColor"
        default:
            return ""
        }
    }
}

class StandingRowViewModel: ObservableObject {
    
    var stand: Stands?
    
    @Published var imageClub: UIImage?
    
    init(stand: Stands? = nil) {
        self.stand = stand
        
        AppCommons().getImage(stand?.team?.logo, size: CGSize(width: CGFloat(15), height: CGFloat(20))) { image in
            DispatchQueue.main.async {
                self.imageClub = image
            }
        }
    }
    
    func getRankColor() -> String {
        
        let col = self.getColorString(rank: stand?.rank ?? 0)
        return col
    }
    
    func getColorString(rank: Int) -> String {
        switch rank {
        case 1, 2, 3, 4:
            return "ChampionsColor"
        case 5, 6:
            return "EuropaLeagueColor"
        case 7:
            return "EuLeaguePrelColor"
        case 18, 19, 20:
            return "RetrocessioneColor"
        default:
            return "AppDimmedTextColor"
        }
    }
    
}
