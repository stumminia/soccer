//
//  CalendarViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 17/12/21.
//

import Foundation
import Combine
import SwiftUI

class CalendarViewModel: ObservableObject {
    
    var cancellables: Set<AnyCancellable> = [] // combine
    var currentLeague: Leagues?
    @Published var calendar: [CalendarStructure] = []
    
    init(currentLeague: Leagues? = nil) {
        self.currentLeague = currentLeague
        self.getFixtures()
        
    }
    
    func getFixtures() {
        
        guard let league = currentLeague?.league?.id, let season = currentLeague?.seasons?.last,  let seasonID = season.year else { return }
        
        AppContext.shared.soccerClient.getCurrentFixture(auth: ApiAuth(), league: league, season: seasonID, current: false)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }
                    
                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (countryResp) in
                guard let weakself = self else { fatalError("error referencing self")}
                var calStruct: [CalendarStructure] = []
                let calendar = countryResp.response ?? []
                for i in 0..<calendar.count {
                    let day = calendar[i]
                    let daySplit = day.split(separator: " ").last
                    let matchCalendar = "match_day".mainLocalized(String(daySplit ?? ""))
                    let structure: CalendarStructure = CalendarStructure(id: i, calendarDay: matchCalendar, calendarDate: nil, round: day)
                    calStruct.append(structure)
                }
                weakself.calendar = calStruct
            }
            .store(in: &cancellables)
        
    }
    
    func getMatchDays(from round: String) {
        
        guard let league = currentLeague?.league?.id, let season = currentLeague?.seasons?.last,  let seasonID = season.year else { return }
        
        AppContext.shared.soccerClient.getFixtureByRound(auth: ApiAuth(), league: league, season: seasonID, round: round)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (result) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch result {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            } receiveValue: { [weak self] (fixture) in
                guard let weakself = self else { fatalError("error referencing self")}
                //weakself.createCalendarArray(fixture.response ?? [])
            }
            .store(in: &cancellables)
        
    }
    
    
}
