//
//  LeagueDetailModel.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 26/11/21.
//

import Foundation
import Combine
import SwiftUI

enum SelectedView: String {
    case calendar = "detail.league.calendar"
    case match = "detail.league.match"
    case ranking = "detail.league.ranking"
    case goals = "detail.league.goals"
}

class LeagueDetailModel: ObservableObject {
    
    var currentLeague: Leagues?
    var cancellables: Set<AnyCancellable> = [] // combine
    
    @Published var imageLeague: UIImage?
    var firestoreManager: FirestoreManager
    
    init(currentLeague: Leagues? = nil, firestoreManager: FirestoreManager) {
        self.currentLeague = currentLeague
        
        self.firestoreManager = firestoreManager
        
        AppCommons().getImage(currentLeague?.league?.logo, size: CGSize(width: CGFloat(15), height: CGFloat(20))) { image in
            DispatchQueue.main.async {
                self.imageLeague = image
            }
        }
    }
    
    func getCurrentFixtures() {
        
        guard let league = currentLeague?.league?.id, let season = currentLeague?.seasons?.last,  let seasonID = season.year else { return }
        
        AppContext.shared.soccerClient.getCurrentFixture(auth: ApiAuth(), league: league, season: seasonID)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            } receiveValue: { [weak self] (countryResp) in
                guard let weakself = self else { fatalError("error referencing self")}
                
                weakself.firestoreManager.setMatchDaySelected(matchDay: countryResp.response?.first ?? "", idMatchDay: weakself.getIdMatch(day: countryResp.response?.first ?? ""))
            }
            .store(in: &cancellables)
    }
    
    func getIdMatch(day: String) -> Int {
        
        if day.contains(" - ") {
            let number = day.split(separator: "-").last?.description.trimmingCharacters(in: .whitespaces) ?? ""
            let num = Int(number) ?? 0
            return num-1
        }
        
        
        return 0
    }
    
}

