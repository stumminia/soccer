//
//  MatchDayRowModel.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/12/21.
//

import Foundation
import Combine
import SwiftUI

class MatchDayRowModel: ObservableObject {

    var match: MatchDay?
    
    @Published var imageClubHome: UIImage?
    @Published var imageClubAway: UIImage?
    
    @Published var resultHome: String?
    @Published var resultAway: String?
    
    var matchText: String {
        
        var text = ""
        if let homeName = match?.teams?.home?.name, let awayName = match?.teams?.away?.name {
            text = homeName + " " + (resultHome ?? "-") + " : " + (resultAway ?? "-") + " " + awayName
        }
        return text
    }
    
    internal init(match: MatchDay? = nil) {
        self.match = match
        
        
        AppCommons().getImage(match?.teams?.home?.logo, size: CGSize(width: CGFloat(15), height: CGFloat(20))) { image in
            DispatchQueue.main.async {
                self.imageClubHome = image
            }
        }
        
        AppCommons().getImage(match?.teams?.away?.logo, size: CGSize(width: CGFloat(15), height: CGFloat(20))) { image in
            DispatchQueue.main.async {
                self.imageClubAway = image
            }
        }
        
        if let homeResult = self.match?.goals?.home {
            self.resultHome = String(homeResult)
        }
        
        if let awayResult = self.match?.goals?.away {
            self.resultAway = String(awayResult)
        }
        
    }
    
    
    
}
