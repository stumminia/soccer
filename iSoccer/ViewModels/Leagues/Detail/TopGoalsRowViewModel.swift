//
//  TopGoalsRowViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/01/22.
//

import Foundation
import Combine
import SwiftUI

class TopGoalsRowViewModel: ObservableObject {
    
    var goalStruct: TopGoalStructure?
    var typeList: TopPlayerType?
    
    @Published var logoImage: UIImage?
    
    @Published var value: String = ""
    
    init(goalStruct: TopGoalStructure, type: TopPlayerType) {
        self.goalStruct = goalStruct
        self.typeList = type
        self.populateValue()
        self.getImage(logoTeam: self.goalStruct?.logoTeam ?? "")
    }
    
    func getImage(logoTeam: String) {
        
        AppCommons().getImage(logoTeam, size: CGSize(width: CGFloat(15), height: CGFloat(20))) { image in
            DispatchQueue.main.async {
                self.logoImage = image
            }
        }
        
    }
    
    func populateValue() {
        
        switch self.typeList {
        case .goal:
            self.value = self.goalStruct?.goals ?? ""
        case .assist:
            self.value = self.goalStruct?.assist ?? ""
        case .cards:
            self.value = self.goalStruct?.cards ?? ""
        case .none:
            self.value = ""
        }
        

    }
}
