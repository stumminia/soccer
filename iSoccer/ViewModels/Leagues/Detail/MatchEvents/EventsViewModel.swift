//
//  EventsViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 24/01/22.
//

import Foundation
import Combine
import SwiftUI

class EventsViewModel: ObservableObject {
    
    var cancellables: Set<AnyCancellable> = [] // combine
    var fixtureID: Int?
    var teamHomeID: Int?
    var teamAwayID: Int?
    
    @Published var events: [Events]?

    init(fixtureID: Int? = nil, homeID: Int?, awayID: Int?) {
        self.fixtureID = fixtureID
        self.teamHomeID = homeID
        self.teamAwayID = awayID
        
        self.getEvents()
    }

    deinit {
        cancellables.removeAll()
        self.events?.removeAll()
    }
    
    func getEvents() {
        
        guard let fixture = self.fixtureID else { return }
        
        AppContext.shared.soccerClient.getEventsMatch(auth: ApiAuth(), fixtureID: fixture)
        .receive(on: DispatchQueue.main)
        .sink { [weak self] (completion) in
            guard self != nil else { fatalError("error referencing self") }
                    
            switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
            }
        } receiveValue: { [weak self] (response) in
            guard let weakself = self else { fatalError("error referencing self")}
            weakself.events = response.response ?? []
        }
        .store(in: &cancellables)
    }
    
    func getButtonImage(type: EventsViewType) -> UIImage {
        
        var imageName = ""
        if type == .compact {
            imageName = "goal_menu"
        }
        else {
            imageName = "all_event"
        }
                                    
        guard let compactImage = UIImage(named: imageName) else { return UIImage() }
        let image = compactImage.scale(with: CGSize(width: CGFloat(25), height: CGFloat(25))) ?? UIImage()
        return image
    }
    
    func getButtonImageChecked(type: EventsViewType) -> UIImage {
        
        var imageName = ""
        if type == .compact {
            imageName = "goal_menu_check"
        }
        else {
            imageName = "all_events_check"
        }
                                    
        guard let compactImage = UIImage(named: imageName) else { return UIImage() }
        let image = compactImage.scale(with: CGSize(width: CGFloat(25), height: CGFloat(25))) ?? UIImage()
        return image
    }
    
    
    func events(viewType: EventsViewType)  ->  [Events]? {
        switch viewType {
        case .compact:
            return self.events?.filter {$0.type == .goal}
        case .full:
            return self.events
        }
    }
}
