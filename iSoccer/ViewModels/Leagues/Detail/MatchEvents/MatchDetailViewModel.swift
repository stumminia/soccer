//
//  MatchDetailViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 20/01/22.
//

import Foundation
import Combine
import SwiftUI

enum SelectedMatchDetailView: String {
    case events = "match.detail.event.tab"
    case lineup = "match.detail.lineup.tab"
    case statistics = "match.detail.statistics.tab"
    case bet = "match.detail.bet.tab"
    case footfant = "match.detail.footfant.tab"
}

class MatchDetailViewModel: ObservableObject {
    
    var fixtureID: Int?
    var teams: Teams?
    var goals: Goals?
    var matchDate: Int64?
    var season: Int?
    
    @Published var imageClubHome: UIImage?
    @Published var imageClubAway: UIImage?
    
    var resultHome: String?
    var resultAway: String?
    
    var matchText: String {
        
        if let homeResult = self.goals?.home {
            self.resultHome = String(homeResult)
        }
        
        if let awayResult = self.goals?.away {
            self.resultAway = String(awayResult)
        }
        
        let text = (resultHome ?? "-") + " : " + (resultAway ?? "-")
        
        return text
    }
   
    init(fixtureID: Int? = nil, teams: Teams?, goals: Goals?, matchDate: Int64?, season: Int?) {
        self.fixtureID = fixtureID
        self.teams = teams
        self.goals = goals
        self.matchDate = matchDate
        self.season = season
        
        AppCommons().getImage(self.teams?.home?.logo, size: CGSize(width: CGFloat(70), height: CGFloat(60))) { image in
            DispatchQueue.main.async {
                self.imageClubHome = image
            }
        }
        
        AppCommons().getImage(self.teams?.away?.logo, size: CGSize(width: CGFloat(70), height: CGFloat(60))) { image in
            DispatchQueue.main.async {
                self.imageClubAway = image
            }
        }
        
    }
    
    func showBets() -> Bool {
       
        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateStyle = .full
        formatter.timeZone = TimeZone.current
        
        let today = (formatter.date(from: formatter.string(from: Date()))?.millisecondsSince1970Int64 ?? 0)/1000
        
        return (self.matchDate ?? 0) > today
    }
    
}
