//
//  EventBoxModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 25/01/22.
//

import Foundation
import Combine
import SwiftUI

class EventBoxModel: ObservableObject {
    
    @Published var event: Events?
    
    var iconEvent: UIImage {
        guard var imageName = event?.type?.rawValue else { return UIImage() }
        
        var size: CGSize = .zero
        
        switch event?.type {
        case .subst:
            imageName = "subst_out"
            size = CGSize(width: 15, height: 15)
        case .card:
            let details = event?.detail?.components(separatedBy: " ")
            let color = details?.first ?? ""
            imageName = "Card_" + color
            size = CGSize(width: 12, height: 12)
        case .goal:
            if event?.detail?.contains("Own") ?? false {
                imageName = "Goal_own"
            }
            size = CGSize(width: 12, height: 12)
        default:
            size = CGSize(width: 15, height: 15)
            break
        }

        let image = UIImage(named: imageName)?.scale(with: size) ?? UIImage()
        
        return image
    }
    
    var getRenderingForIconEvent: Image.TemplateRenderingMode {
        
        if event?.type == .card || event?.type == .subst || event?.type == ._var {
            return .original
        }
        if event?.type == .goal && (event?.detail?.contains("Own") ?? false) {
            return .original
        }
        
        return .template
        
    }
    
    var iconSecundary: UIImage {
        
        if event?.type == .goal && event?.assist?.name != nil {
            return UIImage(named: "Assist")?.scale(with: CGSize(width: 15, height: 15)) ?? UIImage()
        }
        else if event?.type == .subst {
            return UIImage(named: "subst_in")?.scale(with: CGSize(width: 15, height: 15)) ?? UIImage()
        }
        
        
        return UIImage().scale(with: CGSize(width: 15, height: 15)) ?? UIImage()
        
    }
    
    var timeEvent: String {
        guard let time = self.event?.time?.elapsed else { return "" }
        
        return String(time) + "'"
        
    }
    
    var iconColor: Color {
        
        if self.event?.type == .goal {
            if self.event?.detail == "Normal Goal" {
                return Color("AppTextColor")
            }
            else if self.event?.detail == "Own Goal" {
                return Color.red
            }
        }
        
        return Color.clear
        
    }
    

    init(event: Events) {
        self.event = event
    }
    
    func getDetailText() -> String {
        
        var detail: String = ""
        
        switch self.event?.type {
        case .subst, .goal:
            detail = self.event?.assist?.name ?? ""
            break
        case .card:
            detail = self.event?.comments ?? ""
            break
        case ._var:
            detail = (self.event?.detail ?? "")
            if let comment = self.event?.comments {
                detail = detail + " - " + comment
            }
            break
        case .none:
            detail = ""
        }
        
        return detail
    }
    
}
