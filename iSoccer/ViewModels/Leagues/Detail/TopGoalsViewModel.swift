//
//  TopGoalsViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/01/22.
//

import Foundation
import Combine
import SwiftUI

enum TopPlayerType: String {
    
    case goal
    case assist
    case cards
    
}

class TopGoalsViewModel: ObservableObject {

    var cancellables: Set<AnyCancellable> = [] // combine
    
    var leagueID: Int?
    var seasonID: Int?
    var cardList: [TopGoalStructure] = []
    
    @Published var topScores: [TopGoalStructure] = []
    @Published var type: TopPlayerType? = .goal
    
    
    init(leagueID: Int? = nil, seasonID: Int? = nil) {
        self.leagueID = leagueID
        self.seasonID = seasonID
        
        self.loadData()
        
    }
    
    func loadData() {
        
        switch self.type {
        case .goal:
            self.getTopScore()
        case .assist:
            self.getTopAssist()
        case .cards:
            self.getTopRedCards()
        case .none:
            self.getTopScore()
        }
        
    }
    
    func getTopRedCards() {
        guard let league = leagueID, let season = seasonID else { return }
        
        AppContext.shared.soccerClient.getTopRedCard(auth: ApiAuth(), league: league, season: season)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }
                    
                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (resp) in
                guard let weakself = self else { fatalError("error referencing self")}
                weakself.cardList.append(contentsOf: self?.createStructure(list: resp.response ?? []) ?? [])
                weakself.getTopYellowCard()
            }
            .store(in: &cancellables)
        
    }
    
    func getTopYellowCard() {
        
        guard let league = leagueID, let season = seasonID else { return }
        
        AppContext.shared.soccerClient.getTopYellowCard(auth: ApiAuth(), league: league, season: season)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }
                    
                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (resp) in
                guard let weakself = self else { fatalError("error referencing self")}
                weakself.cardList.append(contentsOf: self?.createStructure(list: resp.response ?? []) ?? [])
                weakself.topScores = self?.createStructure(list: resp.response ?? []) ?? []
//                weakself.topScores = weakself.cardList
            }
            .store(in: &cancellables)
        
    }
    
    func getTopAssist() {
        
        guard let league = leagueID, let season = seasonID else { return }
        
        AppContext.shared.soccerClient.getTopAssist(auth: ApiAuth(), league: league, season: season)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }
                    
                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (resp) in
                guard let weakself = self else { fatalError("error referencing self")}
                weakself.topScores = self?.createStructure(list: resp.response ?? []) ?? []
            }
            .store(in: &cancellables)
        
    }
    
    
    func getTopScore() {
        
        guard let league = leagueID, let season = seasonID else { return }
        
        AppContext.shared.soccerClient.getTopScores(auth: ApiAuth(), league: league, season: season)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }
                    
                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (resp) in
                guard let weakself = self else { fatalError("error referencing self")}
                weakself.topScores = self?.createStructure(list: resp.response ?? []) ?? []
            }
            .store(in: &cancellables)
        
    }
    
    fileprivate func orderList(_ toOrderList: inout [ScoreList]) {
        
        switch self.type {
            
        case .goal:
            //Ordinare la lista in base al numero di goal (meno rigori)
            toOrderList.sort(by: {
                if ($0.statistics?.first?.goals?.total ?? 0) != ($1.statistics?.first?.goals?.total ?? 0) {
                    return ($0.statistics?.first?.goals?.total ?? 0) >= ($1.statistics?.first?.goals?.total ?? 0)
                }
                else {
                    return ($0.statistics?.first?.penalty?.scored ?? 0) < ($1.statistics?.first?.penalty?.scored ?? 0)
                }
            })
        case .assist:
            toOrderList.sort(by: {
                return ($0.statistics?.first?.goals?.assists ?? 0) >= ($1.statistics?.first?.goals?.assists ?? 0)
            })
            break
        case .cards:
            toOrderList.sort(by: {
                let card0 = ($0.statistics?.first?.cards?.yellow ?? 0) + ($0.statistics?.first?.cards?.yellowred ?? 0) + ($0.statistics?.first?.cards?.red ?? 0)
                let card1 = ($1.statistics?.first?.cards?.yellow ?? 0) + ($1.statistics?.first?.cards?.yellowred ?? 0) + ($1.statistics?.first?.cards?.red ?? 0)
                return card0 >= card1
            })
            
        case .none:
            break
        }
        
    }
    
    func createStructure(list: [ScoreList]) -> [TopGoalStructure] {
        
        var scores: [TopGoalStructure] = []
        
        var toOrderList: [ScoreList] = []
        toOrderList.append(contentsOf: list)
        
        orderList(&toOrderList)
        
        //Create la struct
        for i in 0..<toOrderList.count {
            let bean = toOrderList[i]
            let cards = (bean.statistics?.first?.cards?.yellow ?? 0) + (bean.statistics?.first?.cards?.red ?? 0) + (bean.statistics?.first?.cards?.yellowred ?? 0)
            let structure = TopGoalStructure(position: (i+1), logoTeam: (bean.statistics?.first?.team?.logo ?? ""), playerName: bean.player?.name, goals: String(bean.statistics?.first?.goals?.total ?? 0), penalty: String(bean.statistics?.first?.penalty?.scored ?? 0), assist: String(bean.statistics?.first?.goals?.assists ?? 0), cards: String(cards))
            
            scores.append(structure)
        }
        
        return scores
    }
    
}
