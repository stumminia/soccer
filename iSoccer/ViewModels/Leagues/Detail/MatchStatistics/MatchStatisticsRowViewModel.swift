//
//  MatchStatisticsRowViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 03/02/22.
//

import Foundation
import Combine
import SwiftUI

class MatchStatisticsRowViewModel: ObservableObject {
    
    var row: MatchStatisticsStructure?
    
    init(row: MatchStatisticsStructure) {
        self.row = row
    }
    
    func getColorRow() -> String {
        
        return (self.row?.position ?? 0) % 2 == 0 ? "League_row_BG_09" : "League_row_BG"
    }
    
    func getValue1() -> Double {
        
        let total = Double(self.row?.homeInt ?? 0) + Double(self.row?.awayInt ?? 0)
        return Double(self.row?.homeInt ?? 0) / total
        
    }
    
    func getValue2() -> Double {
        
        let total = Double(self.row?.awayInt ?? 0) + Double(self.row?.homeInt ?? 0)
        return Double(self.row?.awayInt ?? 0) / total
        
    }
    
}
