//
//  MatchStatisticsViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 02/02/22.
//

import Foundation
import Combine
import SwiftUI

class MatchStatisticsViewModel: ObservableObject {
    
    var cancellables: Set<AnyCancellable> = [] // combine
    var fixtureID: Int?
    
    @Published var statStructList: [MatchStatisticsStructure] = []
    
    internal init(fixtureID: Int? = nil) {
        self.fixtureID = fixtureID
        
        self.getStatistics()
    }
    
    
    func getStatistics() {
        
        guard let fixtureID = fixtureID else {
            return
        }

        
        AppContext.shared.soccerClient.getStatistics(auth: ApiAuth(), fixture: fixtureID)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                        
                switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                }
            } receiveValue: { [weak self] (response) in
                guard let weakself = self else { fatalError("error referencing self")}
                weakself.createStructure(statistics: response.response ?? [])
            }
            .store(in: &cancellables)
        
    }
    
    
    func createStructure(statistics: [TeamStatisics]) {
        
        var statisticsList: [MatchStatisticsStructure] = []
        
        let statisticHome = statistics.first?.statistics
        let statisticAway = statistics.last?.statistics
        
        let count = min(statisticHome?.count ?? 0, statisticAway?.count ?? 0)
        
        for i in 0..<count {
            let statHome = statisticHome?[i]
            let statAway = statisticAway?[i]
            
            if statHome?.statisticType == statAway?.statisticType {
                let statStruct: MatchStatisticsStructure = MatchStatisticsStructure(valueHome: statHome?.statisticValue ?? "", valueAway: statAway?.statisticValue ?? "", statisticType: StatisticType.getLocalizable(type: statHome?.statisticType).mainLocalized(), position: i, homeInt: Int(statHome?.statisticValue ?? "0"), awayInt: Int(statAway?.statisticValue ?? "0"))
                statisticsList.append(statStruct)
            }
        }
        
        self.statStructList = statisticsList
        
    }
    
}
