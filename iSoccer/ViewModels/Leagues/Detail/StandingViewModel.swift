//
//  StandingViewModel.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/12/21.
//

import Foundation
import SwiftUI
import Combine


class StandingViewModel: ObservableObject {
    
    var cancellables: Set<AnyCancellable> = [] // combine
    var currentLeague: Leagues?
    @Published var standings: [Stands] = []
    
    
    init(currentLeague: Leagues? = nil){
        self.currentLeague = currentLeague
        
        self.getStandings()
    }
 
    
    func getStandings() {
        
        guard let league = currentLeague?.league?.id, let season = currentLeague?.seasons?.last,  let seasonID = season.year else { return }
        
        AppContext.shared.soccerClient.getStandings(auth: ApiAuth(), league: league, season: seasonID)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }

                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (standingsResp) in
                guard let weakself = self else { fatalError("error referencing self")}
                let standing = standingsResp.response?.first?.league?.standings?.first
                weakself.standings = standing ?? []
            }
            .store(in: &cancellables)
        

    }
    
}
