//
//  MatchesModel.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 09/12/21.
//

import Foundation
import Combine
import SwiftUI

class MatchesModel: ObservableObject {

    var cancellables: Set<AnyCancellable> = [] // combine
    var currentLeague: Leagues?
    
    @Published var matches: [MatchStructure] = []
    
    init(currentLeague: Leagues? = nil, round: String? = nil) {
        self.currentLeague = currentLeague
        
        if let round = round, !round.isEmpty {
            self.getFixtureByRound(round)
        }
        else {
            self.getCurrentMatchDay()
        }
        
        //self.createStubMatchesList()
                
    }
    
    func getCurrentMatchDay() {

        guard let league = currentLeague?.league?.id, let season = currentLeague?.seasons?.last,  let seasonID = season.year else { return }
        
        AppContext.shared.soccerClient.getCurrentFixture(auth: ApiAuth(), league: league, season: seasonID)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }
                    
                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (countryResp) in
                guard let weakself = self else { fatalError("error referencing self")}
                for round in countryResp.response ?? [] {
                    weakself.getFixtureByRound(round)
                }
                if let round = countryResp.response?.first {
                    weakself.getFixtureByRound(round)
                }
            }
            .store(in: &cancellables)
    }
    
    func getFixtureByRound(_ round: String?) {
        
        guard let league = currentLeague?.league?.id, let season = currentLeague?.seasons?.last,  let seasonID = season.year else { return }
        
        AppContext.shared.soccerClient.getFixtureByRound(auth: ApiAuth(), league: league, season: seasonID, round: round)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (result) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch result {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            } receiveValue: { [weak self] (fixture) in
                guard let weakself = self else { fatalError("error referencing self")}
                weakself.createMatchesList(fixture.response ?? [])
            }
            .store(in: &cancellables)
        
    }
    
    func createStubMatchesList() {
        
        let match1: MatchStructure = MatchStructure(date: "17 dic 2021 18:30", timeDate: 1639762200, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731758, referee: nil, timezone: "UTC", date: "2021-12-17T17:30:00+00:00", timestamp: 1639762200, periods: Periods(first: nil, second: nil), venue: Venue(id: 910, name: "Stadio Olimpico", city: "Roma"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 487, name: "Lazio", logo: "https://media.api-sports.io/football/teams/487.png", winner: nil), away: Team(id: 495, name: "Genoa", logo: "https://media.api-sports.io/football/teams/495.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731758)
        
        let match2: MatchStructure = MatchStructure(date: "17 dic 2021 20:45", timeDate: 1639770300, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731760, referee: nil, timezone: "UTC", date: "2021-12-17T19:45:00+00:00", timestamp: 1639770300, periods: Periods(first: nil, second: nil), venue: Venue(id: 933, name: "Stadio Arechi", city: "Salerno"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 514, name: "Salernitana", logo: "https://media.api-sports.io/football/teams/514.png", winner: nil), away: Team(id: 505, name: "Inter", logo: "https://media.api-sports.io/football/teams/505.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731760)
        
        let match3: MatchStructure = MatchStructure(date: "18 dic 2021 15:00", timeDate: 1639836000, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731754, referee: nil, timezone: "UTC", date: "2021-12-18T14:00:00+00:00", timestamp: 1639836000, periods: Periods(first: nil, second: nil), venue: Venue(id: 879, name: "Gewiss Stadium", city: "Bergamo"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 499, name: "Atalanta", logo: "https://media.api-sports.io/football/teams/499.png", winner: nil), away: Team(id: 497, name: "Roma", logo: "https://media.api-sports.io/football/teams/497.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731754)
        
        let match4: MatchStructure = MatchStructure(date: "18 dic 2021 18:00", timeDate: 1639846800, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731755, referee: nil, timezone: "UTC", date: "2021-12-18T17:00:00+00:00", timestamp: 1639846800, periods: Periods(first: nil, second: nil), venue: Venue(id: 881, name: "Stadio Renato Dall\'Ara", city: "Bologna"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 500, name: "Bologna", logo: "https://media.api-sports.io/football/teams/500.png", winner: nil), away: Team(id: 496, name: "Juventus", logo: "https://media.api-sports.io/football/teams/496.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731755)
        
        let match5: MatchStructure = MatchStructure(date: "18 dic 2021 20:45", timeDate: 1639856700, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731756, referee: nil, timezone: "UTC", date: "2021-12-18T19:45:00+00:00", timestamp: 1639856700, periods: Periods(first: nil, second: nil), venue: Venue(id: 12275, name: "Unipol Domus", city: "Cagliari"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 490, name: "Cagliari", logo: "https://media.api-sports.io/football/teams/490.png", winner: nil), away: Team(id: 494, name: "Udinese", logo: "https://media.api-sports.io/football/teams/494.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731756)
        
        let match6: MatchStructure = MatchStructure(date: "19 dic 2021 12:30", timeDate: 1639913400, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731757, referee: nil, timezone: "UTC", date: "2021-12-19T11:30:00+00:00", timestamp: 1639913400, periods: Periods(first: nil, second: nil), venue: Venue(id: 902, name: "Stadio Artemio Franchi", city: "Firenze"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 502, name: "Fiorentina", logo: "https://media.api-sports.io/football/teams/502.png", winner: nil), away: Team(id: 488, name: "Sassuolo", logo: "https://media.api-sports.io/football/teams/488.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731757)
        
        let match7: MatchStructure = MatchStructure(date: "19 dic 2021 15:00", timeDate: 1639922400, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731762, referee: nil, timezone: "UTC", date: "2021-12-19T14:00:00+00:00", timestamp: 1639922400, periods: Periods(first: nil, second: nil), venue: Venue(id: 939, name: "Stadio Alberto Picco", city: "La Spezia"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 515, name: "Spezia", logo: "https://media.api-sports.io/football/teams/515.png", winner: nil), away: Team(id: 511, name: "Empoli", logo: "https://media.api-sports.io/football/teams/511.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731762)
        
        let match8: MatchStructure = MatchStructure(date: "19 dic 2021 18:00", timeDate: 1639933200, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731761, referee: nil, timezone: "UTC", date: "2021-12-19T17:00:00+00:00", timestamp: 1639933200, periods: Periods(first: nil, second: nil), venue: Venue(id: 905, name: "Stadio Comunale Luigi Ferraris", city: "Genova"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 498, name: "Sampdoria", logo: "https://media.api-sports.io/football/teams/498.png", winner: nil), away: Team(id: 517, name: "Venezia", logo: "https://media.api-sports.io/football/teams/517.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil))),
            MatchDay(fixture: Fixture(id: 731763, referee: nil, timezone: "UTC", date: "2021-12-19T17:00:00+00:00", timestamp: 1639933200, periods: Periods(first: nil, second: nil), venue: Venue(id: 943, name: "Stadio Olimpico Grande Torino", city: "Torino"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 503, name: "Torino", logo: "https://media.api-sports.io/football/teams/503.png", winner: nil), away: Team(id: 504, name: "Verona", logo: "https://media.api-sports.io/football/teams/504.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731761)
        
        let match9: MatchStructure = MatchStructure(date: "19 dic 2021 20:45", timeDate: 1639943100, matchDay: "Giornata 18", matches: [MatchDay(fixture: Fixture(id: 731759, referee: nil, timezone: "UTC", date: "2021-12-19T14:00:00+00:00", timestamp: 1639943100, periods: Periods(first: nil, second: nil), venue: Venue(id: 907, name: "Stadio Giuseppe Meazza", city: "Milano"), status: Status(long: "Not Started", short: nil, elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 18"), teams: Teams(home: Team(id: 489, name: "Milan", logo: "https://media.api-sports.io/football/teams/489.png", winner: nil), away: Team(id: 492, name: "Napoli", logo: "https://media.api-sports.io/football/teams/492.png", winner: nil)), goals: Goals(home: nil, away: nil), score: Score(halftime: Goals(home: nil, away: nil), fulltime: Goals(home: nil, away: nil), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))], matchId: 731759)
        
        let matchOrd: [MatchStructure] = [match1, match2, match3, match4, match5, match6, match7, match8, match9]
        
        self.matches = matchOrd
        
    }
    
    
    func createMatchesList(_ matchesDay: [MatchDay]) {
        
        let matchesOrdered = matchesDay.sorted(by: { ($0.fixture?.timestamp ?? 0) < ($1.fixture?.timestamp ?? 0) })
 
        var matchOrd: [MatchStructure] = []
        
        for match in matchesOrdered {
            if matchOrd.contains(where: { $0.timeDate == match.fixture?.timestamp }) {
                if matchOrd.contains(where: { $0.matchId != match.fixture?.id }) {
                    matchOrd.first(where: { $0.timeDate == match.fixture?.timestamp })!.matches.append(match)
                }
            }
            else {
                let matchDate = Formatter.formatDate(Double(match.fixture?.timestamp ?? 0), dateStyle: .medium, timeStyle: .short).replacingOccurrences(of: ",", with: "")
                let matchCalendar = "match_day".mainLocalized(String(match.league?.round?.split(separator: " ").last ?? ""))
                let matchStruct = MatchStructure(date: matchDate, timeDate: match.fixture?.timestamp, matchDay: matchCalendar, matches: [match], matchId: match.fixture?.id)
                matchOrd.append(matchStruct)
            }

        }
        
        self.matches = matchOrd
        
    }
}
