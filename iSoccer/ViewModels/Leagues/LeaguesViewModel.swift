//
//  LeaguesViewModel.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 11/11/21.
//

import SwiftUI
import Combine
import Firebase

class LeaguesViewModel: ObservableObject {
        
    var cancellables: Set<AnyCancellable> = [] // combine
    
    @Published var leaguesPublisher: [Leagues] = []
    @Published var countriesPublisher: [Country] = []
    @Published var sectionPublisher: [LeagueStructure] = []
    
    var firestoreManager: FirestoreManager
    
    private var principalLeagues: [String] = []
    
    init(firestoreManager: FirestoreManager) {
        self.firestoreManager = firestoreManager
        
        self.firestoreManager.getPrincipalLeagues() { leagues in
            self.principalLeagues = leagues
            self.getAllCountries()
        }
    }
    

    func getAllLeagues()  {
        AppContext.shared.soccerClient.getAllLeagues(auth: ApiAuth())
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                guard self != nil else { fatalError("error referencing self") }
                
                switch completion {
                case .failure(let error):
                    print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                case .finished: print("finished")
                }
            } receiveValue: { [weak self] (leagues) in
                guard let weakSelf = self else { fatalError("error referencing self") }
                weakSelf.leaguesPublisher = leagues.response ?? []
                weakSelf.createListForView() { structures in
                    
                    var finalStruct = structures
                    let preferredStruct = weakSelf.getPreferredLeagues(sections: finalStruct)
                    if (preferredStruct.leagues?.count ?? 0) > 0 {
                        finalStruct.insert(preferredStruct, at: 0)
                    }
                    
                    weakSelf.sectionPublisher = finalStruct
                    
                }
            }
            .store(in: &cancellables)
    }
    
    func getAllCountries() {
        AppContext.shared.soccerClient.getAllCountries(auth: ApiAuth())
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (completion) in
                    guard self != nil else { fatalError("error referencing self") }
                    
                    switch completion {
                    case .failure(let error):
                        print("error: \(error.code) - \(error.message) - \(String(describing:error.originalPayload))")
                    case .finished: print("finished")
                    }
            } receiveValue: { [weak self] (countryResp) in
                guard let weakself = self else { fatalError("error referencing self")}
                weakself.getAllLeagues()
                weakself.countriesPublisher = (weakself.sortCountries(countryResp.response))
            }
            .store(in: &cancellables)
    }
    
    func reloadAfterPreferred(leagueID: Int?) {
        
        var structures: [LeagueStructure] = []
        
        for i in 0..<self.sectionPublisher.count {
            let itemStruct = self.sectionPublisher[i]
            var leagues = itemStruct.leagues
            
            for j in 0..<(leagues?.count ?? 0) {
                var league = leagues?[j]
                if league?.league?.id == leagueID {
                    let pref = league?.preferred ?? true
                    league?.preferred = !pref
                    leagues?[j] = league!
                }
            }
            
            itemStruct.leagues = leagues
            self.sectionPublisher[i] = itemStruct
            
            structures.append(itemStruct)
        }
        self.sectionPublisher = structures
    }
    
    
    private func createListForView(_ completion: @escaping (_ data: [LeagueStructure]) -> Void ) {
        
        var structures: [LeagueStructure] = []
        
        for i in 0..<self.countriesPublisher.count {
            let country = self.countriesPublisher[i]
            let leagues: [Leagues] = self.leaguesPublisher.filter({$0.country?.code == country.code})
            let structure = LeagueStructure(countryCode: country.code, sectionTitle: country.name, urlImageTitle: country.flag, leagues: leagues)
            firestoreManager.isPreferred(leagues: leagues) {leagues in
                structure.leagues = leagues
                structures.append(structure)
                
                if i == self.countriesPublisher.count-1 {
                    completion(structures)
                }
            }
        }
    }
    
    private func getPreferredLeagues(sections: [LeagueStructure]) -> LeagueStructure {
                
        let leagueStructPreferred: LeagueStructure = LeagueStructure(countryCode: "", sectionTitle: "leagues.title.preferred".mainLocalized(), urlImageTitle: "", leagues: [])
        
        for item in sections {
            for league in item.leagues ?? [] {
                if league.preferred {
                    leagueStructPreferred.leagues?.append(league)
                }
            }
        }
        
        return leagueStructPreferred
        
    }
    
    private func sortCountries(_ countries: [Country]?) -> [Country] {
        
        var sorted: [Country] = []
        
        guard let countries = countries else {
            return []
        }

        sorted = self.principalLeagues.compactMap{ code in
            countries.filter{ $0.code == code }.first
        }
                
        //sorted.append(contentsOf: countries.filter({ !self.principalLeagues.contains($0.code ?? "")}))
        
        return sorted
    }
    
}
