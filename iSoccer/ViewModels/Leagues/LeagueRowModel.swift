//
//  LeagueRowModel.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 11/11/21.
//

import Foundation
import Combine
import SwiftUI

class LeagueRowModel: ObservableObject, Identifiable {

    var league: Leagues
    
    var cancellables: Set<AnyCancellable> = []
    
    @Published var imageLeague: UIImage?
    
    init(league: Leagues) {
        self.league = league
        
        AppCommons().getImage(league.league?.logo) { image in
            DispatchQueue.main.async {
                self.imageLeague = image
            }
        }
        
    }
    
}
