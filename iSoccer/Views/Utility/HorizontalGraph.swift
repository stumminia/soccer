//
//  HorizontalGraph.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 07/02/22.
//

import SwiftUI

struct HorizontalGraph: View {

    var value1: Double?
    var value2: Double?
    
    
    var body: some View {
        Color.clear    // << placeholder
            .frame(maxWidth: .infinity, maxHeight: 12)
            .overlay(GeometryReader { gp in
                // chart is here
                HStack(spacing: 0) {
                    Rectangle().fill(Color("League_row_BG_09"))
                        .frame(width: (self.value1 ?? 0) * gp.size.width)
                    Rectangle()
                        .fill(Color("League_row_BG"))
                        .frame(width: (self.value2 ?? 0) * gp.size.width)
                }
            })
            .clipShape(RoundedRectangle(cornerRadius: 4))
    }
}

struct HorizontalGraph_Previews: PreviewProvider {
    static var previews: some View {
        HorizontalGraph(value1: 0.85, value2: 0.15)
    }
}
