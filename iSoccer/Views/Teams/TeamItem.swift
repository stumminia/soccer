//
//  TeamItem.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 07/04/22.
//

import SwiftUI

struct TeamItem: View {
    
    var namespace: Namespace.ID
    @Binding var show: Bool
    @ObservedObject var viewModel: TeamsViewModel
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading, spacing: 4.0) {
                
                Spacer()
                
                Image(uiImage: self.viewModel.logoImage ?? UIImage())
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(10)
                    .padding(8)
                    .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 16, style: .continuous))
                    .strokeStyle(cornerRadius: 16)
                    .matchedGeometryEffect(id: "logoImage", in: namespace)
                
                Text(self.viewModel.teamInformation?.team?.name ?? "")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundStyle(.primary)
                    .lineLimit(1)
                    .matchedGeometryEffect(id: "teamName", in: namespace)
                
                Text((self.viewModel.teamInformation?.venue?.name ?? "").uppercased())
                    .font(.footnote)
                    .fontWeight(.semibold)
                    .foregroundStyle(.secondary)
                    .matchedGeometryEffect(id: "venueName", in: namespace)
                
                
                Text(self.viewModel.venueAddress)
                    .font(.footnote)
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .foregroundColor(.secondary)
                    .matchedGeometryEffect(id: "venueAddress", in: namespace)
                
            }
            .padding(.all, 20.0)
            .padding(.vertical, 20)
            .frame(height: 260.0)
            .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 30, style: .continuous))
            .strokeStyle()
            .padding(.horizontal, 20)
            .overlay(
                AsyncImage(url: URL(string: self.viewModel.teamInformation?.venue?.image ?? "")){
                    image in
                    
                    image.image?
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(height: 110)
                        .offset(x: 80, y: -50)
                        .transition(.asymmetric(
                            insertion: .opacity.animation(.easeInOut(duration: 0.1)),
                            removal: .opacity.animation(.easeInOut(duration: 0.3).delay(0.2))))
                }
                .matchedGeometryEffect(id: "venueImage", in: namespace)
            )
            
        }
    }
}

struct TeamItem_Previews: PreviewProvider {
    @Namespace static var namespace
    
    static var previews: some View {
        TeamItem(namespace: namespace, show: .constant(true), viewModel: TeamsViewModel(teamID: 505, season: 2022))
            .preferredColorScheme(.dark)
    }
}
