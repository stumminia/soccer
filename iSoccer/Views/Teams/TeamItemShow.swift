//
//  TeamItemShow.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 20/04/22.
//

import SwiftUI

struct TeamItemShow: View {
    
    var namespace: Namespace.ID
    @Binding var show: Bool
    @ObservedObject var viewModel: TeamsViewModel
    
    var body: some View {
        ZStack {
            ScrollView {
                
                cover
                    .padding(.bottom, -90)
                
                ForEach(self.viewModel.players ?? [], id: \.self) { player in
                    TeamPlayerItem(viewModel: TeamPlayerModel(player: player))
                }
                .padding(.horizontal, 20)
                .padding(.bottom, 12)
                
            }
            .ignoresSafeArea()
            
            button
        }
        .background(Color("AppBackgroundColor"))
    }
    
    var cover: some View {
        GeometryReader { proxy in
            let scrollY = proxy.frame(in: .global).minY
            
            VStack {
                Spacer()
            }
            .frame(maxWidth: .infinity)
            .frame(height: 500)
            .background(AsyncImage(url: URL(string: self.viewModel.teamInformation?.venue?.image ?? "")){
                image in
                
                image.image?
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: 500)
                    .frame(maxWidth: .infinity)
                    .mask(
                        RoundedRectangle(cornerRadius: 30, style: .continuous)
                    )
                    .offset(x: 0, y: -105)
                    .offset(y: scrollY > 0 ? -scrollY : 0)
                    .scaleEffect(scrollY > 0 ? scrollY / 1000 + 1 : 1)
                    .blur(radius: scrollY / 10)
                    .transition(.asymmetric(
                        insertion: .opacity.animation(.easeInOut(duration: 0.1)),
                        removal: .opacity.animation(.easeInOut(duration: 0.3).delay(0.2))))
                
            }
                .matchedGeometryEffect(id: "venueImage", in: namespace))
            .overlay(
                overlayCover
                    .offset(y: scrollY > 0 ? scrollY * -0.6 : 0)
            )
        }
        .frame(height: 500)
    }
    
    var overlayCover: some View {
        VStack(alignment: .leading, spacing: 12) {
            
            HStack(alignment: .center, spacing: 12) {
                
                Image(uiImage: self.viewModel.logoImage ?? UIImage())
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(10)
                    .padding(8)
                    .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 16, style: .continuous))
                    .strokeStyle(cornerRadius: 16)
                    .matchedGeometryEffect(id: "logoImage", in: namespace)
                
                
                Text(self.viewModel.teamInformation?.team?.name ?? "")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundStyle(.primary)
                    .lineLimit(1)
                    .matchedGeometryEffect(id: "teamName", in: namespace)
            }
            
            Text((self.viewModel.teamInformation?.venue?.name ?? "").uppercased())
                .font(.footnote)
                .fontWeight(.semibold)
                .foregroundStyle(.secondary)
                .matchedGeometryEffect(id: "venueName", in: namespace)
            
            
            Text(self.viewModel.venueAddress)
                .font(.footnote)
                .multilineTextAlignment(.leading)
                .lineLimit(2)
                .frame(maxWidth: .infinity, alignment: .leading)
                .foregroundColor(.secondary)
                .matchedGeometryEffect(id: "venueAddress", in: namespace)
            
        }
        .padding(20)
        .background(
            Rectangle()
                .fill(.ultraThinMaterial)
                .mask(RoundedRectangle(cornerRadius: 30, style: .continuous))
                .matchedGeometryEffect(id: "blur", in: namespace)
        )
        .offset(y: 50)
        .padding(20)
    }
    
    var button: some View {
        Button {
            withAnimation(.closeCard) {
                show.toggle()
            }
        } label: {
            Image(systemName: "xmark")
                .font(.body.weight(.bold))
                .foregroundColor(.secondary)
                .padding(8)
                .background(.ultraThinMaterial, in: Circle())
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
        .padding(20)
        .ignoresSafeArea()
    }
}

struct TeamItemShow_Previews: PreviewProvider {
    @Namespace static var namespace
    
    static var previews: some View {
        TeamItemShow(namespace: namespace, show: .constant(true), viewModel: TeamsViewModel(teamID: 505, season: 2022))
    }
}
