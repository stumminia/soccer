//
//  TeamPlayerItem.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 08/04/22.
//

import SwiftUI

struct TeamPlayerItem: View {
    
    @ObservedObject var viewModel: TeamPlayerModel
    
    var body: some View {
        HStack(alignment: .center, spacing: 8.0) {
            
            Image(uiImage: self.viewModel.playerImage ?? UIImage())
                .aspectRatio(contentMode: .fit)
                .cornerRadius(10)
                .padding(8)
            
            VStack (alignment: .leading, spacing: 4.0) {
                Text((self.viewModel.player?.name ?? "").uppercased())
                    .font(.body)
                    .fontWeight(.bold)
                    .foregroundStyle(.primary)
                    
                Text((self.viewModel.player?.position ?? "").uppercased())
                    .font(.footnote)
                    .fontWeight(.semibold)
                    .foregroundStyle(.secondary)
            }
            
            Spacer()
            
            Text(self.viewModel.player!.number != nil ? String(self.viewModel.player!.number!) : "")
                .font(.largeTitle)
                .fontWeight(.semibold)
                .foregroundStyle(.linearGradient(colors: [.primary, .primary.opacity(0.5)], startPoint: .topLeading, endPoint: .bottomTrailing))
        }
        .padding(.horizontal, 16)
        .padding(.vertical, 12)
        .frame(maxWidth: .infinity, maxHeight: 56.0, alignment: .center)
        .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 16, style: .continuous))
        .strokeStyle(cornerRadius: 16)
    }
}

struct TeamPlayerItem_Previews: PreviewProvider {
    static var previews: some View {
        TeamPlayerItem(viewModel: TeamPlayerModel(player: PlayerTeam(id: 30558, name: "N. Barella", age: 25, number: 23, position: "Midfielder", photo: "https://media.api-sports.io/football/players/30558.png")))
            .preferredColorScheme(.dark)
    }
}
