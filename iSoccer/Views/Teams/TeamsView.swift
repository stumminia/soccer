//
//  TeamsView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 11/03/22.
//

import SwiftUI

struct TeamsView: View {
    
    @ObservedObject var viewModel: TeamsViewModel
    @State var hasScrolled = false
    @Namespace var namespace
    @State var show = false
    @State var showStatusBar = true
    
    var body: some View {
        ZStack {
            Color("HomeBackgroundColor")
                .edgesIgnoringSafeArea(.all)
            
            ScrollView {
                scrollDetection
                
                if !show {
                    TeamItem(namespace: namespace, show: $show, viewModel: self.viewModel)
                        .padding(.bottom, 24)
                        .onTapGesture {
                            withAnimation(.openCard) {
                                show.toggle()
                                showStatusBar = false
                            }
                        }
                    
                    ForEach(viewModel.matches ?? []) { section in
                        Section (header: TeamHeader(title: section.sectionTitle ?? "")) {
                            ForEach(section.matches ?? [], id: \.self) { match in
                                TeamCalendarItem(viewModel: TeamCalendarModel(match: match))
                            }
                        }
                    }
                    .listStyle(PlainListStyle())
                    .listRowInsets(EdgeInsets())
                    .padding(.horizontal, 20)
                    .padding(.bottom, 4)
                }
            }
            .padding(.bottom, 12)
            .navigationBarHidden(true)
            .coordinateSpace(name: "scroll")
            .safeAreaInset(edge: .top, content: {
                Color.clear.frame(height: 70)
            })
            .overlay(
                NavigationBar(title: "team.information.title".mainLocalized(), hasScrolled: $hasScrolled)
                    .background(.ultraThinMaterial)
                    .frame(height: 70)
                    .frame(maxHeight: .infinity, alignment: .top)
            )
            
            if show {
                TeamItemShow(namespace: namespace, show: $show, viewModel: self.viewModel)
                    .zIndex(1)
                    .transition(.asymmetric(
                        insertion: .opacity.animation(.easeInOut(duration: 0.1)),
                        removal: .opacity.animation(.easeInOut(duration: 0.3).delay(0.2))))
            }
        }
        .statusBar(hidden: !showStatusBar)
        .onChange(of: show) { newValue in
            withAnimation(.closeCard) {
                if newValue {
                    showStatusBar = false
                } else {
                    showStatusBar = true
                }
            }
        }
    }
    
    var scrollDetection: some View {
        GeometryReader { proxy in
            Color.clear.preference(key: ScrollPreferenceKey.self, value: proxy.frame(in: .named("scroll")).minY)
        }
        .frame(height: 0)
        .onPreferenceChange(ScrollPreferenceKey.self, perform: { value in
            withAnimation(.easeInOut) {
                if value < 0 {
                    hasScrolled = true
                } else {
                    hasScrolled = false
                }
            }
        })
    }
}
                                 

struct TeamsView_Previews: PreviewProvider {
    static var previews: some View {
        TeamsView(viewModel: TeamsViewModel(teamID: 505, season: 2022))
                    .preferredColorScheme(.dark)
    }
}

struct TeamHeader: View {
 
    var title: String = ""
    var body: some View {
        VStack(alignment: .leading) {
            Text(title)
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(.system(size: 10, weight: .semibold, design: .serif))
                .padding(.leading, 4)
                
        }
        
    }
    
}
