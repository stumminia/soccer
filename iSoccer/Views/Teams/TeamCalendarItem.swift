//
//  TeamCalendarItem.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 03/08/22.
//

import SwiftUI

struct TeamCalendarItem: View {
    
    var viewModel: TeamCalendarModel
    
    var body: some View {
        
        HStack(alignment: .center, spacing: 8.0) {
            
            Text(viewModel.matchDay)
                .font(.system(size: 10, weight: .semibold))
                .frame(width: 40)
                .multilineTextAlignment(.leading)
                .lineLimit(nil)
                .fixedSize(horizontal: false, vertical: true)
                .padding(.leading, 10)
            
            VStack(alignment: .leading, spacing: 2.0) {
                HStack(alignment: .center, spacing: 4.0) {
                    AsyncImage(url: URL(string: self.viewModel.match.logoHome ?? ""), scale: 4) { image in
                            image.resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(maxWidth: 15, maxHeight: 20)
                    }
                    placeholder: {
                        ProgressView()
                    }
                                    
                    Text(viewModel.match.teamHome ?? "")
                        .font(.system(size: 12, weight: .semibold))
                }
                
                HStack(alignment: .center, spacing: 4.0) {
                    AsyncImage(url: URL(string: self.viewModel.match.logoAway ?? ""), scale: 4) { image in
                            image.resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(maxWidth: 15, maxHeight: 20)
                    }
                    placeholder: {
                        ProgressView()
                    }
                                    
                    Text(viewModel.match.teamAway ?? "")
                        .font(.system(size: 12, weight: .semibold))
                }
            }
            
            Spacer()
            
            Text(viewModel.matchDetail)
                .font(.system(size: 10, weight: .semibold))
                .padding(.trailing, 10)
            
        }
        .padding(.horizontal, 8)
        .padding(.vertical, 12)
        .frame(maxWidth: .infinity, maxHeight: 56.0, alignment: .center)
        .background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 16, style: .continuous))
        .strokeStyle(cornerRadius: 16)
    }
}

struct TeamCalendarItem_Previews: PreviewProvider {
    static var previews: some View {
        TeamCalendarItem(viewModel: TeamCalendarModel(match: MatchStruct(logoHome: "https://media.api-sports.io/football/teams/867.png", logoAway: "https://media.api-sports.io/football/teams/505.png", teamHome: "Lecce", teamAway: "Inter", status: StatusMatch.NS, timestamp: 1660416300, goalHome: 1, goalAway: 2, matchType: 135)))
    }
}
