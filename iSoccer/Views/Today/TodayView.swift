//
//  TodayView.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 08/11/21.
//

import SwiftUI

struct TodayView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct TodayView_Previews: PreviewProvider {
    static var previews: some View {
        TodayView()
    }
}
