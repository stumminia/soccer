//
//  CountryRow.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 19/11/21.
//

import SwiftUI

struct CountryRow: View {
    
    @ObservedObject var viewModel: CountryRowModel
    
    init(viewModel: CountryRowModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ZStack {
            //Color("League_row_BG")
            
            HStack(spacing: CGFloat(8)) {
                Image(uiImage: viewModel.imageCountry ?? UIImage())
                    .frame(width: 25, height: 25)
                
                VStack(alignment: .leading) {
                    Text(viewModel.country.name ?? "")
                        .font(.headline)
                        
                    
                    Text(viewModel.country.code ?? "")
                        .font(.subheadline)
                }
                
                Spacer()
            }
            .padding(.leading, 8)
            
        }
        .padding(.horizontal, 8)
        .frame(width: .none, height: CGFloat(40))
        
        
    }
}

struct CountryRow_Previews: PreviewProvider {
    static var previews: some View {
        CountryRow(viewModel: CountryRowModel(country: Country(name: "England", code: "GB", flag: "https://media.api-sports.io/flags/gb.svg")))
    }
}
