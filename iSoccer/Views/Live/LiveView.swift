//
//  LiveView.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 08/11/21.
//

import SwiftUI

struct LiveView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct LiveView_Previews: PreviewProvider {
    static var previews: some View {
        LiveView()
    }
}
