//
//  RootView.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 10/11/21.
//

import SwiftUI

struct RootView: View {
    
    @ObservedObject var viewModel: RootViewModel
    @AppStorage("selectedTab") var selectedTab: Tab = .leagues
    @Environment(\.colorScheme) var colorScheme
    @EnvironmentObject var firestoreManager: FirestoreManager
    
    init() {
        self.viewModel = RootViewModel.init()
    }
    
    var body: some View {
        
        //return
        GeometryReader { geometry in
            ZStack {

                switch selectedTab {
                case .leagues:
                    LeaguesView(LeaguesViewModel(firestoreManager: firestoreManager))
                        .environmentObject(firestoreManager)
                case .today:
                    TodayView()
                case .home:
                    HomeView()
                case .live:
                    LiveView()
                case .more:
                    MoreView()
                }
                
                TabBar(color: colorScheme == .dark ? .teal : .blue)
                    .environmentObject(firestoreManager)
            }
        }
        
    }
    
}

//struct RootView_Previews: PreviewProvider {
//    static var previews: some View {
//        RootView()
//    }
//}
