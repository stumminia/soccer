//
//  PlayerView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 06/09/22.
//

import SwiftUI

struct PlayerView: View {
    
    @ObservedObject var viewModel: PlayerModel
    
    init(viewModel: PlayerModel) {
        self.viewModel = viewModel
        UISegmentedControl.appearance().backgroundColor = UIColor(named: "League_row_BG")
    }
    
    var body: some View {
        ZStack {
            Color("HomeBackgroundColor")
                .edgesIgnoringSafeArea(.all)
            
            HStack(alignment: .top) {
                Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
            }
            
        }
    }
}

struct PlayerView_Previews: PreviewProvider {
    static var previews: some View {
        PlayerView(viewModel: PlayerModel(playerId: 217))
    }
}
