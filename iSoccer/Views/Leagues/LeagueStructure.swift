//
//  LeagueStructure.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 23/11/21.
//

import Foundation
import Combine
import SVGKit


class LeagueStructure: Identifiable, Hashable, ObservableObject {
    
    public var id:UUID?
    public var countryCode: String?
    public var sectionTitle: String?
    @Published public var imageTitle: UIImage?
    public var leagues: [Leagues]?
    
    internal init(countryCode: String?, sectionTitle: String? = nil, urlImageTitle: String? = nil, leagues: [Leagues]? = nil) {
        self.id = UUID()
        self.countryCode = countryCode
        self.sectionTitle = sectionTitle
        self.leagues = leagues
        
        AppCommons().getImageSVG(urlImageTitle) { image in
            DispatchQueue.main.async {
                self.imageTitle = image
            }
        }
    }
    
    static func == (lhs: LeagueStructure, rhs: LeagueStructure) -> Bool {
        lhs.countryCode == rhs.countryCode
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(countryCode)
    }
    
    
    
}
