//
//  LeagueDetail.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 26/11/21.
//

import SwiftUI

struct LeagueDetail: View {
    
    @ObservedObject var viewModel: LeagueDetailModel
    @State var selectedOption: SelectedView = .match
    @State var roundSelected: String = ""
    @State var type: TopPlayerType = .goal
    @EnvironmentObject var firestoreManager: FirestoreManager
    
    private var calendarView: CalendarView?
    private var matchView: MatchDayView?
    private var standingView: StandingView?
    private var topGoalView: TopGoalsView?
    

    init(_ model: LeagueDetailModel) {
        self.viewModel = model
        UISegmentedControl.appearance().backgroundColor = UIColor(named: "League_row_BG")
        
        viewModel.getCurrentFixtures()
    }
    
    var body: some View {
        
            ZStack {
                Color("HomeBackgroundColor")
                    .edgesIgnoringSafeArea(.all)
                
                HStack(alignment: .top) {
                    VStack() {
                        Picker("", selection: $selectedOption) {
                            Text(SelectedView.calendar.rawValue.mainLocalized()).tag(SelectedView.calendar)
                            Text(SelectedView.match.rawValue.mainLocalized()).tag(SelectedView.match)
                            Text(SelectedView.ranking.rawValue.mainLocalized()).tag(SelectedView.ranking)
                            Text(SelectedView.goals.rawValue.mainLocalized()).tag(SelectedView.goals)
                        }.pickerStyle(.segmented)
                        .padding(.horizontal, 8)
                        
                        displayCorrectView(selectedOption)
                        
                        Spacer()
                    }
                    .background(Color("HomeBackgroundColor"))
                }
            }
            .navigationTitle(Text(self.viewModel.currentLeague?.league?.name ?? ""))
    }
    
    func createNavTitle() -> some View {
        
        return HStack(spacing: 8) {
            
            Image(uiImage: self.viewModel.imageLeague ?? UIImage())
                .frame(width: CGFloat(10), height: CGFloat(10), alignment: .center)
            
            Text(self.viewModel.currentLeague?.league?.name ?? "")
            
        }
        
    }
    
    @ViewBuilder
    func displayCorrectView(_ selectedOption: SelectedView) -> some View {
        switch selectedOption {
        case .calendar:
            CalendarView(viewModel: CalendarViewModel(currentLeague: viewModel.currentLeague), selectedOption: self.$selectedOption, roundSelected: self.$roundSelected)
                .environmentObject(firestoreManager)
        case .match:
            MatchDayView(MatchesModel(currentLeague: viewModel.currentLeague, round: self.roundSelected))
        case .ranking:
            StandingView(StandingViewModel(currentLeague: viewModel.currentLeague))
        case .goals:
            TopGoalsView(viewModel: TopGoalsViewModel(leagueID: viewModel.currentLeague?.league?.id, seasonID: viewModel.currentLeague?.seasons?.last?.year))
        }
    }
}

struct LeagueDetail_Previews: PreviewProvider {
    static var previews: some View {
        LeagueDetail(LeagueDetailModel(currentLeague: Leagues(league: League(id: 135, name: "Serie A Tim", type: "", logo: "https://media.api-sports.io/football/leagues/135.png"), country: Country(name: "Italy", code: "", flag: ""), seasons: [Season(year: 2021, start: "2021", end: "2022", current: true, coverage: nil)]), firestoreManager: FirestoreManager()))
            .preferredColorScheme(.dark)
    }
}
