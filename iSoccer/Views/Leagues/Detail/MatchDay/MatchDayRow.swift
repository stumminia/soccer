//
//  MatchDayRow.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 03/12/21.
//

import SwiftUI

struct MatchDayRow: View {
    
    @ObservedObject var viewModel: MatchDayRowModel
    
    init(model: MatchDayRowModel) {
        self.viewModel = model
    }
    
    var body: some View {
        ZStack {
            Color(UIColor.clear)
            HStack(alignment: .center, spacing: 8) {
                
                AsyncImage(url: URL(string: self.viewModel.match?.teams?.home?.logo ?? ""), scale: 4) { image in
                        image.resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 15, maxHeight: 20)
                }
                placeholder: {
                    ProgressView()
                }
                                
                Text(viewModel.matchText)
                    
                AsyncImage(url: URL(string: self.viewModel.match?.teams?.away?.logo ?? ""), scale: 4) { image in
                        image.resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 15, maxHeight: 20)
                }
                placeholder: {
                    ProgressView()
                }
            }.frame(maxWidth: .infinity, alignment: .center)
        }
        .frame(maxWidth: .infinity, maxHeight: 40, alignment: .center)
    }
    
}

struct MatchDayRow_Previews: PreviewProvider {
    static var previews: some View {
        MatchDayRow(model: MatchDayRowModel(match: MatchDay(fixture: Fixture(id: 731740, referee: "Mazzoleni", timezone: "UTC", date: "2021-12-04T17:00:00+00:00", timestamp: 1638637200, periods: Periods(first: nil, second: nil), venue: Venue(id: 910, name: "Stadio Olimpico", city: "Roma"), status: Status(long: "Not Started", short: StatusMatch(rawValue: "FT"), elapsed: nil)), league: LeagueFixture(id: 135, name: "Serie A", country: "Italy", logo: "https://media.api-sports.io/football/leagues/135.png", flag: "https://media.api-sports.io/flags/it.svg", season: 2021, round: "Regular Season - 16"), teams: Teams(home: Team(id: 497, name: "AS Roma", logo: "https://media.api-sports.io/football/teams/497.png", winner: nil), away: Team(id: 505, name: "Inter", logo: "https://media.api-sports.io/football/teams/505.png", winner: nil)), goals: Goals(home: 0, away: 3), score: Score(halftime: Goals(home: 0, away: 3), fulltime: Goals(home: 0, away: 3), extratime: Goals(home: nil, away: nil), penalty: Goals(home: nil, away: nil)))))
            .preferredColorScheme(.dark)
    }
}
