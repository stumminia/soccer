//
//  MatchDayHeader.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 14/12/21.
//

import SwiftUI

struct MatchDayHeader: View {
    
    let label: String
    let horizontalPadding: CGFloat
    let color: Color
    
    init(label: String, horizontalPadding: CGFloat = 0, color: Color = Color("AppTextColor")) {
        self.label = label
        self.horizontalPadding = horizontalPadding
        self.color = color
    }
    
    
    var body: some View {
        HStack {
            line
            Text(label).foregroundColor(color)
                .font(.system(size: 10))
            line
        }
        .frame(minWidth: 0, maxWidth: .infinity,alignment:.leading)
    }
    
    var line: some View {
        VStack { Divider().background(color) }.padding(horizontalPadding)
    }
}

struct MatchDayHeader_Previews: PreviewProvider {
    static var previews: some View {
        MatchDayHeader(label: "17 dic 2021 18:30")
            .preferredColorScheme(.dark)
    }
}
