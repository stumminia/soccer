//
//  MatchStructure.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 09/12/21.
//

import SwiftUI
import Foundation
import Combine

class MatchStructure: Identifiable, Hashable {
    
    public var id:UUID?
    var date: String?
    var timeDate: Int64?
    var matchDay: String?
    var matches: [MatchDay] = []
    var matchId: Int?
    
    init(date: String?, timeDate: Int64?, matchDay: String?, matches: [MatchDay]?, matchId: Int?) {
        self.id = UUID()
        self.date = date
        self.timeDate = timeDate
        self.matchDay = matchDay
        self.matches = matches ?? []
        self.matchId = matchId
    }
    
    static func == (lhs: MatchStructure, rhs: MatchStructure) -> Bool {
        lhs.matchId == rhs.matchId
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(matchId)
    }
}
