//
//  MatchDay.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 02/12/21.
//

import SwiftUI

struct MatchDayView: View {
    
    @ObservedObject var viewModel: MatchesModel
    
    init(_ model: MatchesModel) {
        self.viewModel = model
        UITableView.appearance().showsVerticalScrollIndicator = false
    }
    
    var body: some View {
        ZStack {
            Color("HomeBackgroundColor")
                .edgesIgnoringSafeArea(.all)
            
            List(viewModel.matches) { matches in
                Section(header: MatchDayHeader(label: matches.date ?? "")) {
                    
                    ForEach(matches.matches, id: \.self) { match in
                        NavigationLink(destination: MatchDetailView(MatchDetailViewModel(fixtureID: match.fixture?.id, teams: match.teams, goals: match.goals, matchDate: match.fixture?.timestamp, season: self.viewModel.currentLeague?.seasons?.last?.year))) {
                            MatchDayRow(model: MatchDayRowModel(match: match))
                        }
                    }
                }
            }
            .listStyle(InsetGroupedListStyle())
            .onAppear(perform: {
                UITableView.appearance().contentInset.top = 0
            })
        }
        .padding(.bottom, 24)
    }
}

struct MatchDay_Previews: PreviewProvider {
    static var previews: some View {
        MatchDayView(MatchesModel(currentLeague: Leagues(league: League(id: 135, name: "Serie A Tim", type: "", logo: "https://media.api-sports.io/football/leagues/135.png"), country: Country(name: "Italy", code: "", flag: ""), seasons: [Season(year: 2021, start: "2021", end: "2022", current: true, coverage: nil)])))
            .preferredColorScheme(.dark)
    }
}
