//
//  LineupsView.swift
//  iSoccer
//
//  Created by Giuseppe Lisanti on 02/02/22.
//

import SwiftUI
enum LineupsViewType  {
    case listView
    case fieldView
    
    var localizedString : String  {
        switch self {
        case .listView:
            return "ListView"
        case .fieldView:
            return "FieldView"
        }
    }
}


struct LineupsView: View {
    @ObservedObject var viewModel: LineupsViewModel
    @State var selectedViewType: LineupsViewType = LineupsViewType.listView
    var selectedViewTypeTitle: String {
        self.selectedViewType.localizedString
    }
    
    init(viewModel: LineupsViewModel) {
        self.viewModel = viewModel
        UITableView.appearance().showsVerticalScrollIndicator = false
    }
    
    var body: some View {
        if (viewModel.lineups == nil) || ((viewModel.lineups?.count ?? 0) <  2) {
//       TODO: Era una specie di empty ma fa un brutto effetto mentre carica i dati
//          Text("No available lineups")
//          .padding(.vertical, 16)
        }
        else {
            VStack {
                Picker(selectedViewType.localizedString, selection: $selectedViewType) {
                    Text(LineupsViewType.listView.localizedString)
                        .tag(LineupsViewType.listView)
                    Text(LineupsViewType.fieldView.localizedString)
                        .tag(LineupsViewType.fieldView)
                }
                .pickerStyle(MenuPickerStyle())
                .padding(.vertical, 8)
                switch selectedViewType{
                case .listView:
                    LineupsListView(viewModel:viewModel)
                case .fieldView:
                    Text ("FieldView")
                }
            }
            .background(Color("HomeBackgroundColor"))
        }

    }
}

struct LineupsView_Previews: PreviewProvider {
    static var previews: some View {
        LineupsView(viewModel: LineupsViewModel.init(fixture: 731591))
    }
}
