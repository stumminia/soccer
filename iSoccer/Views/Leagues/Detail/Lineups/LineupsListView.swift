//
//  LineupsListView.swift
//  iSoccer
//
//  Created by Giuseppe Lisanti on 02/02/22.
//

import SwiftUI

struct LineupsListView: View {
    @ObservedObject var viewModel: LineupsViewModel
    
    init(viewModel: LineupsViewModel) {
        self.viewModel = viewModel
        UITableView.appearance().showsVerticalScrollIndicator = false
        
    }
    
    var body: some View {
        generateLineupList(home: viewModel.lineups?[0], away: viewModel.lineups?[1])
    }

    
    @ViewBuilder
    private func generateLineupList(home: Lineup?, away: Lineup?) -> some View {
        let maxSubtitutes = max( home?.substitutes?.count ?? 0, away?.substitutes?.count ?? 0)
        
        List {
            Section(header:
                        HStack{
                Text(home?.formation ?? "")
                Spacer()
                Text(away?.formation ?? "")}
            ) {
                    ForEach(0..<11) { i in
                        HStack{
                            Text(home?.startXI?[i].player?.name ?? "")
                            
                            Spacer()
                            Text(away?.startXI?[i].player?.name ?? "")
                        }
                    }
                    .listRowSeparator(.hidden)
                }
            .listRowBackground(Color("League_row_BG"))

            Section(header:
                        HStack{
                Spacer()
                Text("Substitutes")
                Spacer()}) {
                    ForEach(0..<maxSubtitutes) { i in
                        HStack{
                            Text(getPlayerName(inLineup: home?.substitutes, atIndex: i))
                            Spacer()
                            Text(getPlayerName(inLineup: away?.substitutes, atIndex: i))
                        }
                    }
                    .listRowSeparator(.hidden)
                }
                .textCase(nil)
                .listRowBackground(Color("League_row_BG"))

            Section(header:
                        HStack{
                Spacer()
                Text("Coach")
                Spacer()}) {
                    HStack{
                        Text(home?.coach?.name ?? "")
                        Spacer()
                        Text(away?.coach?.name ?? "")
                    }
                }
                .textCase(nil)
                .listRowBackground(Color("League_row_BG"))

        }
        .background(Color("HomeBackgroundColor"))
        .onAppear(perform: {
            UITableView.appearance().contentInset.top = -35
        })
    }
    
    private func getPlayerName (inLineup: [PlayerElement]?, atIndex: Int) -> String {
        var name = ""
        guard let lineup = inLineup else {
            return name
        }
        if existElement(inLineup: lineup, atIndex:atIndex){
            name =  inLineup?[atIndex].player?.name ?? ""
        }
        return name
    }
 
    private func existElement(inLineup: [PlayerElement]?, atIndex: Int) -> Bool {
        guard let lineup = inLineup else {
            return false
        }
        return atIndex < lineup.count
    }
}

struct LineupsListView_Previews: PreviewProvider {
    static var previews: some View {
        LineupsListView(viewModel: LineupsViewModel.init(fixture: 731591))

    }
}
