//
//  EventsView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 24/01/22.
//

import SwiftUI

enum EventsViewType  {
    case compact
    case full
    
    var localizedString : String  {
        switch self {
        case .compact:
            return "match.detail.filter.compact".mainLocalized()
        case .full:
            return "match.detail.filter.full".mainLocalized()
        }
    }
}

struct EventsView: View {
    
    @ObservedObject var viewModel: EventsViewModel
    
    @State var selectedViewType: EventsViewType = EventsViewType.compact
    
    var selectedViewTypeTitle: String {
        self.selectedViewType.localizedString
    }
    
    init(viewModel: EventsViewModel) {
        self.viewModel = viewModel
        UITableView.appearance().showsVerticalScrollIndicator = false
    }
    
    var body: some View {
        if (viewModel.events == nil || viewModel.events?.count == 0) {
//       TODO: Era una specie di empty ma fa un brutto effetto mentre carica i dati
//            Text("No available events")
//            .padding(.vertical, 16)
        }
        else {
            VStack {
                
                HStack(spacing: 8) {
                    
                    Spacer()
                    
                    Button {
                        self.selectedViewType = .compact
                    }
                    label: {
                        Image(uiImage: self.selectedViewType == .compact ? viewModel.getButtonImageChecked(type: .compact) : viewModel.getButtonImage(type: .compact))
                    }
                    
                    Button {
                        self.selectedViewType = .full
                    }
                    label: {
                        Image(uiImage: self.selectedViewType == .full ? viewModel.getButtonImageChecked(type: .full) : viewModel.getButtonImage(type: .full))
                    }
                    
                }.padding(.trailing, 16)
                .padding(.vertical, 8)
                
                ZStack {
                    Color("HomeBackgroundColor")
                        .edgesIgnoringSafeArea(.all)
                    
                    List{
                        ForEach(viewModel.events(viewType:self.selectedViewType) ?? [], id: \.self) { event in
                            EventBoxView(EventBoxModel(event: event))
                                .listRowSeparator(.hidden)
                                .listRowBackground(Color.clear)
                                .padding(EdgeInsets(top: 6, leading: getAlignmentEvent(event), bottom: 6, trailing: 0))
                                .listRowInsets(EdgeInsets())
                        }
                    }
                    .listStyle(InsetGroupedListStyle())
                    .onAppear(perform: {
                        UITableView.appearance().contentInset.top = -35
                    })
                }
            }
            .background(Color("HomeBackgroundColor"))
        }
    }
    
    func getAlignmentEvent(_ event: Events) -> CGFloat {
        
        if event.team?.id == viewModel.teamHomeID {
            return .zero
        }
        
        return CGFloat(UIScreen.main.bounds.width/2 - 8)
        
    }
}

struct EventsView_Previews: PreviewProvider {
    static var previews: some View {
        EventsView(viewModel: EventsViewModel(fixtureID: 731617, homeID: 505, awayID: 500))
    }
}
