//
//  MatchDetailHeader.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 21/01/22.
//

import SwiftUI

struct MatchDetailHeader: View {
    
    @ObservedObject var viewModel: MatchDetailViewModel
    @State var isActiveNav: Bool = false
    @State var selectedTeamId: Int?
    
    var body: some View {
        ZStack {
            Color("HomeBackgroundColor")
                .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 8) {
                HStack(alignment: .center, spacing: 16) {
                    VStack(spacing: 0) {
                        Image(uiImage: viewModel.imageClubHome ?? UIImage())
                        Text(viewModel.teams?.home?.name ?? "")
                    }
                    .onTapGesture {
                        self.selectedTeamId = self.viewModel.teams?.home?.id
                        self.isActiveNav = true
                    }
                    
                    Text(viewModel.matchText)
                        .font(.title2)
                        .fontWeight(.bold)
                    
                    VStack(spacing: 0) {
                        Image(uiImage: viewModel.imageClubAway ?? UIImage())
                        Text(viewModel.teams?.away?.name ?? "")
                    }
                    .onTapGesture {
                        self.selectedTeamId = self.viewModel.teams?.away?.id
                        self.isActiveNav = true
                    }
                    
                    NavigationLink(destination: TeamsView(viewModel: TeamsViewModel(teamID: selectedTeamId ?? 0, season: viewModel.season ?? 0)), isActive: $isActiveNav) {
                        
                    }.hidden()
                }
            }
            
        }
        .frame(width: .none, height: CGFloat(80), alignment: .center)
    }
}

struct MatchDetailHeader_Previews: PreviewProvider {
    static var previews: some View {
        MatchDetailHeader(viewModel: MatchDetailViewModel(fixtureID: nil, teams: Teams(home: Team(id: 505, name: "Inter", logo: "https://media.api-sports.io/football/teams/505.png", winner: nil), away: Team(id: 517, name: "Venezia", logo: "https://media.api-sports.io/football/teams/517.png", winner: nil)), goals: Goals(home: 2, away: 0), matchDate: 1647093600, season: 2022))
    }
}
