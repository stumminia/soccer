//
//  MatchDetailView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 20/01/22.
//

import SwiftUI

struct MatchDetailView: View {
    
    @ObservedObject var viewModel: MatchDetailViewModel
    @State var selectedMatchView: SelectedMatchDetailView = .events
    
    init(_ model: MatchDetailViewModel) {
        self.viewModel = model
        UISegmentedControl.appearance().backgroundColor = UIColor(named: "League_row_BG")
    }
    
    var body: some View {
        ZStack {
            Color("HomeBackgroundColor")
                .edgesIgnoringSafeArea(.all)
            
            HStack(alignment: .top) {
                VStack() {
                    
                    MatchDetailHeader(viewModel: viewModel)
                    
                    Picker("", selection: $selectedMatchView) {
                        Text(SelectedMatchDetailView.events.rawValue.mainLocalized()).tag(SelectedMatchDetailView.events)
                        
                        Text(SelectedMatchDetailView.lineup.rawValue.mainLocalized()).tag(SelectedMatchDetailView.lineup)
                            .font(.callout)
                        Text(SelectedMatchDetailView.statistics.rawValue.mainLocalized()).tag(SelectedMatchDetailView.statistics)
                            .font(.callout)
                        
                        if self.viewModel.showBets() {
                            Text(SelectedMatchDetailView.bet.rawValue.mainLocalized()).tag(SelectedMatchDetailView.bet)
                            .font(.callout)
                        }
                        Text(SelectedMatchDetailView.footfant.rawValue.mainLocalized()).tag(SelectedMatchDetailView.footfant)
                            .font(.callout)
                    }.pickerStyle(.segmented)
                    .padding(.horizontal, 8)
                    .padding(.top, 4)
                    .foregroundColor(Color(.clear))
                    
                    displayCorrectView(selectedMatchView)
                    
                    Spacer()
                }
                .background(Color("HomeBackgroundColor"))
            }
        }
    }
    
    @ViewBuilder
    func displayCorrectView(_ selectedOption: SelectedMatchDetailView) -> some View {
        switch selectedOption {
        case .events:
            EventsView(viewModel: EventsViewModel(fixtureID: viewModel.fixtureID, homeID: viewModel.teams?.home?.id, awayID: viewModel.teams?.away?.id))
        case .lineup:
            LineupsView(viewModel: LineupsViewModel(fixture: viewModel.fixtureID))
        case .statistics:
            MatchStatisticsView(MatchStatisticsViewModel(fixtureID: viewModel.fixtureID))
        case .bet:
            Text("selected bet")
        case .footfant:
            Text("selected footfant")
        }
    }
}

struct MatchDetail_Previews: PreviewProvider {
    static var previews: some View {
        MatchDetailView(MatchDetailViewModel(fixtureID: 731799, teams: Teams(home: Team(id: 505, name: "Inter", logo: "https://media.api-sports.io/football/teams/505.png", winner: nil), away: Team(id: 517, name: "Venezia", logo: "https://media.api-sports.io/football/teams/517.png", winner: nil)), goals: Goals(home: 2, away: 0), matchDate: 1647093600, season: 2022))
    }
}
