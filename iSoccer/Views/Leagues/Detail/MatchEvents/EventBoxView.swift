//
//  EventBoxView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 25/01/22.
//

import SwiftUI

struct EventBoxView: View {
    
    @ObservedObject var viewModel: EventBoxModel
    
    init(_ model: EventBoxModel) {
        self.viewModel = model
    }
    
    var body: some View {
        ZStack {
            Color("League_row_BG")
                .edgesIgnoringSafeArea(.all)
            
            VStack(alignment: .leading, spacing: 2) {
                HStack(spacing: 4) {
                    Image(uiImage: viewModel.iconEvent)
                        .renderingMode(viewModel.getRenderingForIconEvent)
                        .tint(Color("AppTextColor"))
                        .frame(width: 20, alignment: .center)
                    
                    Text(viewModel.event?.player?.name ?? "")
                        .font(.caption2)
                        .frame(width: 100, alignment: .center)
                    
                    Text(viewModel.timeEvent)
                        .font(.caption2)
                        .frame(width: 25, alignment: .leading)
                }
                
                HStack(spacing: 4) {
                    
                    Image(uiImage: viewModel.iconSecundary)
                        .renderingMode(.original)
                        .frame(width: 20, alignment: .center)
                    
                    Text(viewModel.getDetailText())
                        .font(.caption2)
                        .frame(width: 100, alignment: .center)
                }
            }
            .padding(.leading, 8)
            .padding(.trailing, 4)
            .padding(.vertical, 8)
        }
        .frame(width: UIScreen.main.bounds.width/2 - 32, height: 45)
        .clipShape(RoundedCorner(radius: 8, corners: .allCorners))
    }
}

struct EventBoxView_Previews: PreviewProvider {
    static var previews: some View {
        EventBoxView(EventBoxModel(event: Events(time: Time(elapsed: 18, extra: nil), team: Team(id: 505, name: "Inter", logo: "https://media.api-sports.io/football/teams/505.png", winner: nil), player: Player(id: 30558, name: "Nicolò Barella"), assist: Player(id: nil, name: nil), type: .card, detail: "Yellow Card", comments: "Foul"))).preferredColorScheme(.dark)
    }
}
