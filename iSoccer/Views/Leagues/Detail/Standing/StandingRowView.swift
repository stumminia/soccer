//
//  StandingRowView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/12/21.
//

import SwiftUI

struct StandingRowView: View {
    
    @ObservedObject var viewModel: StandingRowViewModel
    
    init(model: StandingRowViewModel) {
        self.viewModel = model
    }
    
    var body: some View {
        ZStack{
            Color(UIColor.clear)
            
            HStack {
                HStack(spacing: 8) {
                    
                    Divider()
                        .frame(width: 3, height: 25)
                        .background(Color(viewModel.getRankColor()))
                    
                    Text(String((viewModel.stand?.rank)!))
                        .font(.body)
                    
                    
                    HStack(spacing: 4) {
                        Image(uiImage: viewModel.imageClub ?? UIImage())
                        
                        Text(viewModel.stand?.team?.name ?? "")
                        
                            .font(.callout)
                    }
                }
                
                Spacer()
                
                
                
                HStack(spacing: 6) {
                    
                    Text(String(viewModel.stand?.points ?? 0))
                        .font(.callout)
                        .foregroundColor(.green)
                        .frame(width: 20, alignment: .trailing)
                    
                    Text(String(viewModel.stand?.all?.played ?? 0))
                        .font(.callout)
                        .frame(width: 20, alignment: .trailing)
                    
                    Text(String(viewModel.stand?.all?.win ?? 0))
                        .font(.callout)
                        .frame(width: 20, alignment: .trailing)
                    
                    Text(String(viewModel.stand?.all?.draw ?? 0))
                        .font(.callout)
                        .frame(width: 20, alignment: .trailing)
                    
                    Text(String(viewModel.stand?.all?.lose ?? 0))
                        .font(.callout)
                        .frame(width: 20, alignment: .trailing)
                    
                    Text(String(viewModel.stand?.goalsDiff ?? 0))
                        .font(.callout)
                        .frame(width: 30, alignment: .trailing)
              
                }
                .frame(width: 140)
            }
        }
        .padding(.horizontal, 0)
        .frame(width: .none, height: CGFloat(52))
    }
}

struct StandingRowView_Previews: PreviewProvider {
    static var previews: some View {
        StandingRowView(model: StandingRowViewModel(stand: Stands(rank: 1, team: Team(id: 505, name: "Inter", logo: "https://media.api-sports.io/football/teams/505.png", winner: nil), points: 49, goalsDiff: 34, group: "Serie A", form: "WWWWW", status: "same", description: "Promotion - Champions League (Group Stage)", all: Goal(played: 20, win: 15, draw: 4, lose: 1, goals: GoalsStand(facts: 51, against: 16)), home: Goal(played: 9, win: 7, draw: 2, lose: 0, goals: GoalsStand(facts: 25, against: 6)), away: Goal(played: 10, win: 7, draw: 2, lose: 1, goals: GoalsStand(facts: 24, against: 9)), update: "2021-12-22T00:00:00+00:00")))
            .preferredColorScheme(.dark)
    }
}
