//
//  StandingHeaderView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 11/01/22.
//

import SwiftUI

struct StandingHeaderView: View {
    var body: some View {
        ZStack{
            HStack(spacing: 8) {
                
                Spacer()
                
                HStack(spacing: 6) {
                    Text("POINTS".mainLocalized())
                        .font(.caption)
                        .frame(width: 20, alignment: .center)
                    
                    Text("MATCH_PLAYED".mainLocalized())
                        .font(.caption)
                        .frame(width: 20, alignment: .center)
                    
                    Text("WINNER".mainLocalized())
                        .font(.caption)
                        .frame(width: 20, alignment: .center)
                    
                    Text("DRAW".mainLocalized())
                        .font(.caption)
                        .frame(width: 20, alignment: .center)
                    
                    Text("LOSE".mainLocalized())
                        .font(.caption)
                        .frame(width: 20, alignment: .center)
                    
                    Text("GOAL_DIFF".mainLocalized())
                        .font(.caption)
                        .frame(width: 30, alignment: .center)
                }
                .frame(width: 140)
            }
        }
        .frame(width: .none, height: CGFloat(10))
        .padding(.trailing, 36)
        .padding(.top, 24)
        .padding(.bottom, 0)
    }
}

struct StandingHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        StandingHeaderView()
            .preferredColorScheme(.dark)
    }
}
