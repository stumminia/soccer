//
//  StandingView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/12/21.
//

import SwiftUI

struct StandingView: View {
    
    @ObservedObject var viewModel: StandingViewModel
    
    init(_ model: StandingViewModel) {
        self.viewModel = model
    }
    
    var body: some View {
        ZStack {
            Color("HomeBackgroundColor")
                .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 4) {
                StandingHeaderView()
                
                List {
                    Section() {
                        ForEach(viewModel.standings, id: \.self) { stand in
                            StandingRowView(model: StandingRowViewModel(stand: stand))
                        }
                    }
                        
                    
                }.padding(.horizontal, 0)
                .onAppear(perform: {
                    UITableView.appearance().contentInset.top = -35
                })
            }.padding(.top, 0)
        }
    }
}

struct StandingView_Previews: PreviewProvider {
    static var previews: some View {
        StandingView(StandingViewModel(currentLeague: Leagues(league: League(id: 135, name: "Serie A Tim", type: "", logo: "https://media.api-sports.io/football/leagues/135.png"), country: Country(name: "Italy", code: "", flag: ""), seasons: [Season(year: 2021, start: "2021", end: "2022", current: true, coverage: nil)])))
    }
}
