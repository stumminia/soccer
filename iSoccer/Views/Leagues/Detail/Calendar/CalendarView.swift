//
//  CalendarView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 17/12/21.
//

import SwiftUI
import Combine

struct CalendarView: View {
    
    @StateObject var viewModel = CalendarViewModel()
    @State var scrollTo = 22
    @Binding var selectedOption: SelectedView
    @Binding var roundSelected: String
    @EnvironmentObject var firestoreManager: FirestoreManager
    
    var body: some View {
        ZStack {
            Color("HomeBackgroundColor")
                .edgesIgnoringSafeArea(.all)
            
            ScrollView {
                ScrollViewReader { value in
                    ForEach(0..<viewModel.calendar.count, id: \.self) { i in
                        CalendarViewRow(structure: viewModel.calendar[i]).id(i)
                            .environmentObject(firestoreManager)
                            .onTapGesture(perform: {
                                self.roundSelected = viewModel.calendar[i].round ?? ""
                                self.selectedOption = .match
                                
                                firestoreManager.setMatchDaySelected(matchDay: viewModel.calendar[i].round ?? "", idMatchDay: i)
                            })
                            .onAppear() {
                                withAnimation {
                                    firestoreManager.getMatchDayId() { id in
                                        value.scrollTo(id, anchor: .center)
                                    }
                                }
                            }
                    }.onAppear(perform: {
                        UITableView.appearance().contentInset.top = 0
                    })
                    
                }
            }
            
        }
    }
}

//struct CalendarView_Previews: PreviewProvider {
//    static var previews: some View {
//        CalendarView(viewModel: CalendarViewModel(currentLeague: Leagues(league: League(id: 135, name: "Serie A Tim", type: "", logo: "https://media.api-sports.io/football/leagues/135.png"), country: Country(name: "Italy", code: "", flag: ""), seasons: [Season(year: 2021, start: "2021", end: "2022", current: true, coverage: nil)])), selectedOption: $selected, roundSelected: $round)
//    }
//}
