//
//  CalendarStructure.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 17/12/21.
//

import Foundation
import SwiftUI
import Combine

class CalendarStructure: Identifiable, Hashable {
    
    public var id:Int?
    var calendarDay: String?
    var calendarDate: String?
    var round: String?
    
    init(id: Int?, calendarDay: String? = nil, calendarDate: String? = nil, round: String? = nil) {
        self.id = id
        self.calendarDay = calendarDay
        self.calendarDate = calendarDate
        self.round = round
    }
    
    static func == (lhs: CalendarStructure, rhs: CalendarStructure) -> Bool {
        lhs.calendarDay == rhs.calendarDay
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(calendarDay)
    }
    
}
