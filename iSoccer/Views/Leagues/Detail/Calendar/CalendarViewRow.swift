//
//  CalendarViewRow.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 20/12/21.
//

import SwiftUI

struct CalendarViewRow: View {
    
    var structure: CalendarStructure?
    @EnvironmentObject var firestoreManager: FirestoreManager
    
    var body: some View {
        ZStack {
            Color(UIColor.clear)
            
            HStack(alignment: .center, spacing: CGFloat(8)) {
                VStack(alignment: .leading, spacing: CGFloat(8)) {
                    
                    Text(structure?.calendarDay ?? "")
                        .font(.callout)
                        .padding(.vertical, 14)
                    
                    /*Text(structure?.calendarDate ?? "")
                        .font(.caption2)
                        .padding(.bottom, 14)*/
                }
                
                Spacer()
                
                Image("calendar_soccer")
                .renderingMode(.template)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 25, height: 25)
                .foregroundColor(firestoreManager.selectedDay == structure?.round ? Color("EuropaLeagueColor") : Color("AppTextColor"))
                
            }
            .foregroundColor(firestoreManager.selectedDay == structure?.round ? Color("EuropaLeagueColor") : Color("AppTextColor"))
            .padding()
            .frame(maxWidth: .infinity, maxHeight: CGFloat(40))
        }
    }
}

struct CalendarViewRow_Previews: PreviewProvider {
    static var previews: some View {
        CalendarViewRow(structure: CalendarStructure(id: 1, calendarDay: "Giornata 1", calendarDate: "8 ago")).preferredColorScheme(.dark)
    }
}
