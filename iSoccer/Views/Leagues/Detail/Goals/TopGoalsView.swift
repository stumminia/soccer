//
//  TopGoalsView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/01/22.
//

import SwiftUI

struct TopGoalsView: View {
    
    @ObservedObject var viewModel: TopGoalsViewModel
    
    var body: some View {
        ZStack {
                        
            Color("HomeBackgroundColor")
                .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 4) {
            
                TopGoalsHeaderView().environmentObject(viewModel)
                
                List {
                    ForEach(viewModel.topScores, id: \TopGoalStructure.id) { score in
                            TopGoalsRowView(model: TopGoalsRowViewModel(goalStruct: score, type: self.viewModel.type ?? .goal))
                                .environmentObject(viewModel)
                        }
                    
                }
                .padding(.horizontal, 0)
                .onAppear(perform: {
                    UITableView.appearance().contentInset.top = -35
                })
            }.padding(.top, 0)
        }
    }
}

//struct TopGoalsView_Previews: PreviewProvider {
//    static var previews: some View {
//        TopGoalsView(viewModel: TopGoalsViewModel(leagueID: 135, seasonID: 2021), type: Binding.init(.goal))
//    }
//}
