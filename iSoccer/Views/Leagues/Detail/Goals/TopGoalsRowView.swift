//
//  TopGoalsRowView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 28/01/22.
//

import SwiftUI

struct TopGoalsRowView: View {
    
    @ObservedObject var viewModel: TopGoalsRowViewModel
    
    init(model: TopGoalsRowViewModel) {
        self.viewModel = model
    }
    
    var body: some View {
        ZStack {
            Color(UIColor.clear)
            
            HStack(spacing: 4) {
                
                Text(String(viewModel.goalStruct?.position ?? 0))
                    .frame(width: 20, alignment: .trailing)
                    .font(.callout)
                
                Image(uiImage: viewModel.logoImage ?? UIImage())
                    .frame(width: 20, alignment: .center)
                
                Text(viewModel.goalStruct?.playerName ?? "")
                    .font(.callout)
                    .frame(width: 150, alignment: .leading)
                
                Spacer()
                 
                Text(viewModel.value)
                    .frame(width: 20, alignment: .trailing)
                    .font(.callout)
                
                if self.viewModel.typeList == .goal {
                    Text("(" + (self.viewModel.goalStruct?.penalty ?? "") + ")")
                        .frame(width: 25, alignment: .leading)
                        .font(.callout)
                }
                
            }
            
        }
        .frame(maxWidth: .infinity, maxHeight: 40, alignment: .center)
    }
}

struct TopGoalsRowView_Previews: PreviewProvider {
    static var previews: some View {
        TopGoalsRowView(model: TopGoalsRowViewModel(goalStruct: TopGoalStructure(position: 1, logoTeam: "https://media.api-sports.io/football/teams/487.png", playerName: "C. Immobile", goals: "17", penalty: "3"), type: .goal))
    }
}
