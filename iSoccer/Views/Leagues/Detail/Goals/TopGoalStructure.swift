//
//  TopGoalStructure.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 31/01/22.
//

import Foundation
import SwiftUI
import Combine

class TopGoalStructure: Identifiable, Hashable {
    
    public var id:UUID?
    var position: Int?
    var logoTeam: String?
    var playerName: String?
    var goals: String?
    var penalty: String?
    var assist: String?
    var cards: String?
    
    
    init(position: Int? = nil, logoTeam: String? = nil, playerName: String? = nil, goals: String? = nil, penalty: String? = nil, assist: String? = nil, cards: String? = nil) {
        self.id = UUID()
        self.position = position
        self.playerName = playerName
        self.goals = goals
        self.penalty = penalty
        self.assist = assist
        self.cards = cards
        self.logoTeam = logoTeam
        
    }
    
    static func == (lhs: TopGoalStructure, rhs: TopGoalStructure) -> Bool {
        lhs.position == rhs.position
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(position)
        hasher.combine(playerName)
    }
}
