//
//  TopGoalsHeaderView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 31/01/22.
//

import SwiftUI

struct TopGoalsHeaderView: View {
    
    @State var titlePage = "topGoals.type.goal".mainLocalized()
    @EnvironmentObject private var model: TopGoalsViewModel
    
    var body: some View {
        ZStack(alignment: .trailing) {
            Color(.clear)
                .edgesIgnoringSafeArea(.all)
            
            Menu(self.titlePage) {
                Button {
                    self.titlePage = "topGoals.type.goal".mainLocalized()
                    self.model.type = .goal
                    self.model.loadData()
                } label: {
                    Label("topGoals.type.goal".mainLocalized(), image: "goal_menu")
                }
                
//                Button {
//                    self.titlePage = "topGoals.type.penalties".mainLocalized()
//                    self.model.type = .penalty
//                } label: {
//                    Label("topGoals.type.penalties".mainLocalized(), image: "penalty")
//                }
                
                Button {
                    self.titlePage = "topGoals.type.assists".mainLocalized()
                    self.model.type = .assist
                    self.model.loadData()
                } label: {
                    Label("topGoals.type.assists".mainLocalized(), image: "Assist")
                }
                
                Button {
                    self.titlePage = "topGoals.type.cards".mainLocalized()
                    self.model.type = .cards
                    self.model.loadData()
                } label: {
                    Label("topGoals.type.cards".mainLocalized(), image: "cards")
                }
            }
            
        }
        .frame(width: .none, height: CGFloat(10))
        .padding(.trailing, 36)
        .padding(.top, 24)
        .padding(.bottom, 12)
    }
}

//struct TopGoalsHeaderView_Previews: PreviewProvider {
//    static var previews: some View {
//        TopGoalsHeaderView(type: TopPlayerType.goal)
//    }
//}
