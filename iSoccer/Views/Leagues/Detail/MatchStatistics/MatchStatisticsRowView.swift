//
//  MatchStatisticsRowView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 03/02/22.
//

import SwiftUI

struct MatchStatisticsRowView: View {
    
    @ObservedObject var viewModel: MatchStatisticsRowViewModel
    
    init(_ model: MatchStatisticsRowViewModel) {
        self.viewModel = model
    }
    
    var body: some View {
        ZStack{
            
            Color(.clear)
                .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 4) {
                HStack(spacing: 8) {
                    
                    Text(viewModel.row?.valueHome ?? "")
                        .frame(width: 35, alignment: .center)
                        .font(.caption)
                        .padding(.leading, 8)
                    
                    Spacer()
                    
                    Text(viewModel.row?.statisticType ?? "")
                        .font(.footnote)
                    
                    Spacer()
                    
                    Text(viewModel.row?.valueAway ?? "")
                        .frame(width: 35, alignment: .center)
                        .font(.caption)
                        .padding(.trailing, 8)
                    
                }.frame(width: .none, height: 16, alignment: .center)
                
                HorizontalGraph(value1: viewModel.getValue1(), value2: viewModel.getValue2())
                
            }
        }
        .padding(.horizontal, 0)
        .frame(width: .none, height: 50, alignment: .center)
        .clipShape(RoundedCorner(radius: 8, corners: .allCorners))
    }
}

struct MatchStatisticsRowView_Previews: PreviewProvider {
    static var previews: some View {
        MatchStatisticsRowView(MatchStatisticsRowViewModel(row: MatchStatisticsStructure(valueHome: "11", valueAway: "2", statisticType: "Shots on Goal", position: 1)))
            .preferredColorScheme(.dark)
    }
}
