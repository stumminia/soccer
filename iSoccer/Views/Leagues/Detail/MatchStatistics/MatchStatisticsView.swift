//
//  MatchStatisticsView.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 02/02/22.
//

import SwiftUI

struct MatchStatisticsView: View {
    
    @ObservedObject var viewModel: MatchStatisticsViewModel
    
    init(_ model: MatchStatisticsViewModel) {
        self.viewModel = model
    }
    
    var body: some View {
        ZStack {
            Color(.clear)
                .edgesIgnoringSafeArea(.all)
            
            List{
                ForEach(self.viewModel.statStructList) { stats in
                    MatchStatisticsRowView(MatchStatisticsRowViewModel(row: stats))
                        .listRowSeparator(.hidden)
                        .listRowBackground(Color.clear)
                        .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
                }
            }
            .frame(maxWidth: .infinity)
            .padding(.horizontal, 4)
            .onAppear(perform: {
                UITableView.appearance().contentInset.top = 0
            })
            
        }
    }
}

struct MatchStatisticsView_Previews: PreviewProvider {
    static var previews: some View {
        MatchStatisticsView(MatchStatisticsViewModel(fixtureID: 731617))
    }
}
