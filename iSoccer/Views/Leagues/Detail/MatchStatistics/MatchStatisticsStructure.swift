//
//  MatchStatisticsStructure.swift
//  iSoccer
//
//  Created by Salvatore Tumminia on 03/02/22.
//

import Foundation
import Combine
import SwiftUI

struct MatchStatisticsStructure: Identifiable, Hashable {
    
    public var id:UUID?
    var valueHome: String?
    var valueAway: String?
    var statisticType: String?
    var position: Int?
    var homeInt: Int?
    var awayInt: Int?
    
    init(valueHome: String? = nil, valueAway: String? = nil, statisticType: String? = nil, position: Int? = nil, homeInt: Int? = nil, awayInt: Int? = nil) {
        self.id = UUID()
        self.valueHome = (valueHome?.isEmpty ?? true) ? "0" : valueHome
        self.valueAway = (valueAway?.isEmpty ?? true) ? "0" : valueAway
        self.statisticType = statisticType
        self.homeInt = homeInt
        self.awayInt = awayInt
        self.position = position
    }
    
    static func == (lhs: MatchStatisticsStructure, rhs: MatchStatisticsStructure) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(valueAway)
        hasher.combine(valueHome)
        hasher.combine(statisticType)
        hasher.combine(position)
    }
    
}
