//
//  LeagueHeader.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 25/11/21.
//

import SwiftUI

struct LeagueHeader: View {
    
    @ObservedObject var section: LeagueStructure
    
    var body: some View {
        ZStack(alignment: .leading) {
            
            HStack(alignment: .center, spacing: CGFloat(4)) {
                Text(section.sectionTitle ?? "")
                    .font(.callout)
                
                Image(uiImage: section.imageTitle ?? UIImage())
                    .frame(width: 15, height: 15)
            }
        }
        .frame(height: CGFloat(25))
    }
}

struct LeagueHeader_Previews: PreviewProvider {
    static var previews: some View {
        LeagueHeader(section: LeagueStructure(countryCode: "GB", sectionTitle: "England", urlImageTitle: "https://media.api-sports.io/flags/gb.svg", leagues: []))
    }
}
