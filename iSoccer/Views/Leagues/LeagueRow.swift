//
//  LeagueRow.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 11/11/21.
//

import SwiftUI

struct LeagueRow: View {
    
    @ObservedObject var viewModel: LeagueRowModel
    
    init(viewModel: LeagueRowModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ZStack {
            Color(UIColor.clear)
            
            HStack(spacing: CGFloat(16)) {
                
                AsyncImage(url: URL(string: self.viewModel.league.league?.logo ?? ""), scale: 2 ) { image in
                    
                    image.resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 30, maxHeight: 45)
                    
                }
            placeholder: {
                ProgressView()
            }
                
                
                VStack(alignment: .leading) {
                    
                    Text(viewModel.league.league?.name ?? "")
                        .font(.headline)
                    
                    
                    Text(viewModel.league.country?.name ?? "")
                        .font(.subheadline)
                    
                    
                }
                
                Spacer()
                
            }
        }
        .padding(.horizontal, 8)
        .frame(width: .none, height: CGFloat(60))
        
    }
}

struct LeagueRow_Previews: PreviewProvider {
    static var previews: some View {
        LeagueRow(viewModel: LeagueRowModel(league: Leagues(league: League(id: 39, name: "Premier League", type: "League", logo: "https://media.api-sports.io/football/leagues/39.png"), country: Country(name: "England", code: "GB", flag: "https://media.api-sports.io/flags/gb.svg"), seasons: nil)))
    }
}
