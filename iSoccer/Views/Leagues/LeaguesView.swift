//
//  LeaguesView.swift
//  Soccer
//
//  Created by Salvatore Tumminia on 08/11/21.
//

import SwiftUI

struct LeaguesView: View {
    
    @ObservedObject var viewModel: LeaguesViewModel
    @State var isActiveNav: Bool = false
    @State var selectedLeague: Leagues = Leagues()
    @EnvironmentObject var firestoreManager: FirestoreManager
    
    
    init(_ viewModel: LeaguesViewModel){
        UITableView.appearance().backgroundColor  = UIColor(named: "HomeBackgroundColor")
        UITableView.appearance().showsVerticalScrollIndicator = false
        self.viewModel = viewModel
    }
    
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("HomeBackgroundColor")
                    .edgesIgnoringSafeArea(.all)
                List (viewModel.sectionPublisher) { section in
                    Section(header: LeagueHeader(section: section)) {
                        ForEach(section.leagues!) { league in
                            LeagueRow(viewModel: LeagueRowModel(league: league))
                                .onTapGesture{
                                    self.selectedLeague = league
                                    self.isActiveNav.toggle()
                                }
                                .swipeActions(edge: .leading, allowsFullSwipe: true, content: {
                                    Button {
                                        self.firestoreManager.changePreferredState(leagueID: league.league?.id)
                                        self.viewModel.reloadAfterPreferred(leagueID: league.league?.id)
                                    } label: {
                                        if league.preferred {
                                            Label("Preferite", systemImage: "star.fill")
                                        }
                                        else {
                                            Label("NotPreferite", systemImage: "star.slash.fill")
                                        }
                                    }
                                    .tint(.yellow)
                                })
                        }
                    }
                    .listRowBackground(Color("League_row_BG"))
                }
                .listStyle(SidebarListStyle())
                .listRowInsets(EdgeInsets())
                
                NavigationLink(destination: LeagueDetail(LeagueDetailModel(currentLeague: self.selectedLeague, firestoreManager: firestoreManager)).environmentObject(firestoreManager), isActive: $isActiveNav) {
                    
                }.hidden()
            }
            .navigationTitle(Text("LEAGUES_NAME".mainLocalized()))
        }
        .padding(.bottom, 36)
    }
}

struct LeaguesView_Previews: PreviewProvider {
    static var previews: some View {
        LeaguesView(LeaguesViewModel(firestoreManager: FirestoreManager()))
            .preferredColorScheme(.dark)
    }
}
